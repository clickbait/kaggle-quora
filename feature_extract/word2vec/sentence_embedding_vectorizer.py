import numpy as np


class SentenceEmbeddingVectorizer(object):
    def __init__(self, word2vec):
        self.word2vec = word2vec

    def fit(self, X, y):
        return self

    def transform(self, X):
        M = np.array([
            np.sum([self.word2vec[w] for w in words if w in self.word2vec]
                    or [np.zeros(self.word2vec.vector_size)], axis=0)
            for words in X
        ])
        v = M.sum(axis=0)
        return v / np.sqrt((v ** 2).sum())