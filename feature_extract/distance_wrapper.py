import scipy.spatial.distance as distance


def measure(x, f):
    v1, v2 = x
    if v1.sum() != 0 and v2.sum() != 0:
        return f(v1, v2)
    else:
        return 0


def braycurtis_wrapper(x):
    return measure(x, distance.braycurtis)


def canberra_wrapper(x):
    return measure(x, distance.canberra)


def chebyshev_wrapper(x):
    return measure(x, distance.chebyshev)


def cityblock_wrapper(x):
    return measure(x, distance.cityblock)


def correlation_wrapper(x):
    return measure(x, distance.correlation)


def cosine_wrapper(x):
    return measure(x, distance.cosine)


def dice_wrapper(x):
    return measure(x, distance.dice)


def euclidean_wrapper(x):
    return measure(x, distance.euclidean)


def hamming_wrapper(x):
    return measure(x, distance.hamming)


def jaccard_wrapper(x):
    return measure(x, distance.jaccard)


def kulsinski_wrapper(x):
    return measure(x, distance.kulsinski)


def matching_wrapper(x):
    return measure(x, distance.matching)


def minkowski_wrapper(x):
    return measure(x, distance.minkowski)


def rogerstanimoto_wrapper(x):
    return measure(x, distance.rogerstanimoto)


def russellrao_wrapper(x):
    return measure(x, distance.russellrao)


def sokalmichener_wrapper(x):
    return measure(x, distance.sokalmichener)


def sokalsneath_wrapper(x):
    return measure(x, distance.sokalsneath)


def yule_wrapper(x):
    return measure(x, distance.yule)


def decode_from_csv(x):
    return x.strip("[]").split(", ")