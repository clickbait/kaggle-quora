import pandas as pd
import numpy as np
from fuzzywuzzy import fuzz
from nltk.corpus import stopwords
from multiprocessing import Pool
from itertools import izip

import logging
logger = logging.getLogger(__name__)
eng_stopwords = set(stopwords.words('english'))


# FEATURES
# Single, int
def count_character(x):
    return len(unicode(x))


def count_lower(x):
    return sum(1 for i in unicode(x) if i.islower())


def count_upper(x):
    return sum(1 for i in unicode(x) if i.isupper())


def count_number(x):
    return sum(1 for i in unicode(x) if i.isdigit())


def count_ascii(x):
    return sum(1 for i in unicode(x) if ord(i) < 128)


def count_non_ascii(x):
    return sum(1 for i in unicode(x) if ord(i) >= 128)


def count_stopwords(x):
    return len(list_stopwords(x))


def count_alpha(x):
    return sum(1 for i in unicode(x) if i.isalpha())


def count_non_alpha(x):
    return sum(1 for i in unicode(x) if not i.isalpha())

# money, math

# Single, list
def list_stopwords(x):
    return set(unicode(x)).intersection(eng_stopwords)


def exact_match(x):
    return int(bool(x[0] == x[1]))


def fuzz_qratio(x):
    return fuzz.QRatio(unicode(x[0]), unicode(x[1]))


def fuzz_wratio(x):
    field = 'question'
    return fuzz.WRatio(unicode(x[0]), unicode(x[1]))


def fuzz_partial_ratio(x):
    field = 'question'
    return fuzz.partial_ratio(unicode(x[0]), unicode(x[1]))


def fuzz_partial_token_set_ratio(x):
    field = 'question'
    return fuzz.partial_token_set_ratio(unicode(x[0]), unicode(x[1]))


def fuzz_partial_token_sort_ratio(x):
    field = 'question'
    return fuzz.partial_token_sort_ratio(unicode(x[0]), unicode(x[1]))


def fuzz_token_set_ratio(x):
    field = 'question'
    return fuzz.token_set_ratio(unicode(x[0]), unicode(x[1]))


def fuzz_token_sort_ratio(x):
    field = 'question'
    return fuzz.token_sort_ratio(unicode(x[0]), unicode(x[1]))


# METAFEATURES
def diff(x):
    return abs(x[0] - x[1])


def intersect(x):
    return len(set(x[0]).intersection(set(x[1])))


def mean(x):
    if len(x) != 0:
        return np.mean(x)
    else:
        return 0

def Min(x):
    if len(x) != 0:
        return min(x)
    else:
        return 0

def Max(x):
    if len(x) != 0:
        return max(x)
    else:
        return 0

    #Std, Ratio


def decode_features(feature):
    lst = feature[1:-1].split(',')
    lst = map(unicode, lst)
    lst = map(unicode.strip, lst)
    return lst


def extract_features(config, config_section, data_frame):
    # Feature extraction
    pool = Pool(processes=config.getint('multiprocessing', 'cpu_cores'))

    data_frame['unigram1'] = pool.map(decode_features, data_frame['unigram1'])
    data_frame['unigram2'] = pool.map(decode_features, data_frame['unigram2'])

    # Bigrams
    #logger.info("Generating bigrams")
    #data_frame['bigram1'] = data_frame['unigram1'].apply(lambda x: zip(x, x[1:])).apply(set)
    #data_frame['bigram2'] = data_frame['unigram2'].apply(lambda x: zip(x, x[1:])).apply(set)

    # Create DataFrame
    feature_frame = pd.DataFrame({'id': data_frame.iloc[:, 0]})

    # Direct Features
    ## Single input features
    logger.info("Generating direct single features")

    direct_single_features = [
        # TODO: unicode block / unicode category [i.e. korean]
        ('char_count', 'question', count_character),
        ('lower_count', 'question', count_lower),
        ('upper_count', 'question', count_upper),
        ('nonascii_count', 'question', count_non_ascii),
        ('number_count', 'question', count_number),
        ('stopword_count', 'unigram', count_stopwords),

        ('stopwords', 'unigram', list_stopwords),
    ]

    for output_field, input_field, f in direct_single_features:
        feature_frame[output_field + '1'] = pool.map(f, data_frame[input_field + '1'])
        feature_frame[output_field + '2'] = pool.map(f, data_frame[input_field + '2'])

    ## Double input features
    logger.info("Generating direct double features")

    direct_double_features = [
        ('exact_match_question', exact_match),
        ('fuzz_qratio', fuzz_qratio),
        ('fuzz_wratio', fuzz_wratio),
        ('fuzz_partial_ratio', fuzz_partial_ratio),
        ('fuzz_partial_token_set_ratio', fuzz_partial_token_set_ratio),
        ('fuzz_partial_token_sort_ratio', fuzz_partial_token_sort_ratio),
        ('fuzz_token_set_ratio', fuzz_token_set_ratio),
        ('fuzz_token_sort_ratio', fuzz_token_sort_ratio),
    ]

    for output_field, f in direct_double_features:
        feature_frame[output_field] = pool.map(f, izip(data_frame['question1'], data_frame['question2']))

    # Meta Features
    ## Double input meta features
    logger.info("Generating meta double features")

    feature_frame['lower_count_difference'] = pool.map(diff, izip(feature_frame['lower_count1'], feature_frame['lower_count2']))
    feature_frame['upper_count_difference'] = pool.map(diff, izip(feature_frame['upper_count1'], feature_frame['upper_count2']))
    feature_frame['char_count_difference'] = pool.map(diff, izip(feature_frame['char_count1'], feature_frame['char_count2']))
    feature_frame['nonascii_count_difference'] = pool.map(diff, izip(feature_frame['nonascii_count1'], feature_frame['nonascii_count2']))
    feature_frame['number_count_difference'] = pool.map(diff, izip(feature_frame['number_count1'], feature_frame['number_count2']))
    feature_frame['stopword_intersect'] = pool.map(intersect, izip(feature_frame['stopwords1'], feature_frame['stopwords2']))

    # Clean up
    logger.info("Cleaning up and saving")

    feature_frame = feature_frame.drop('stopwords1', axis=1)
    feature_frame = feature_frame.drop('stopwords2', axis=1)

    pool.close()

    return feature_frame