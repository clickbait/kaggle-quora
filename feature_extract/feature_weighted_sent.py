from sklearn.feature_extraction.text import TfidfVectorizer
import re
import gensim
import pandas as pd
import numpy as np
import os

from nltk.corpus import stopwords

import logging

logging.getLogger('gensim').setLevel(logging.ERROR)
logger = logging.getLogger(__name__)

stop_words = stopwords.words('english')

logger = logging.getLogger(__name__)

def sent2weightedvec(s, model, tff_voc, tff_idx):
    words = [w for w in s if not w in stop_words]
    words = [w for w in words if w.isalpha()]
    wordcount = len(words)
    M = []
    Unknown = 0
    for w in words:
        try:
            try:
                M.append(tff_idx[tff_voc[w]] * model[w])
            except:
                M.append(model[w])
                continue
        # number of words unkown to word2vec
        except:
            Unknown += 1
            continue
    M = np.array(M, dtype=np.float64)
    v = M.sum(axis=0)

    if len(M) != 0:
        return v / np.sqrt((v ** 2).sum()), Unknown

    else:
        return np.zeros((300)), Unknown


def questionStart(first_word):
    wcounts = {'how': 0, 'should': 0, 'what': 0, 'which': 0, 'can': 0, 'when': 0, 'after': 0, 'is': 0, 'why': 0,
               'math': 0, 'if': 0, 'other': 0}

    if first_word in wcounts:
        wcounts[first_word] += 1

    else:
        wcounts['other'] += 1

    return wcounts.values()

def decode_unigram(feature):
    """
    For a given unigram that is a list, but interpreted as a string, this makes it into a string

    :param feature: the string
    :return: the unigram
    """
    lst = feature[1:-1].split(',')
    lst = map(unicode, lst)
    lst = map(unicode.strip, lst)
    return lst

def extract_features(config, config_section, data_frame):

    data_path = config.get('data', 'data_path')
    model_base_path = config.get('data', 'model_path')
    model_file = config.get(config_section, 'model_file')
    model_path = os.path.join(data_path, model_base_path, model_file)

    num_qs = data_frame.unigram1.values.shape[0]

    logger.info("Retrieve unigrams")

    model = gensim.models.KeyedVectors.load_word2vec_format(model_path, binary=True)

    question1_vectors = np.zeros((num_qs, 300))
    question2_vectors = np.zeros((num_qs, 300))
    cv_char = TfidfVectorizer(analyzer='word', min_df=2)

    logger.info("Creating TFIDF vocabulary")

    cv_char.fit_transform(data_frame['unigram1'] + data_frame['unigram2'])
    cv_char.transform(data_frame['unigram1']+data_frame['unigram2'])

    tff_voc = cv_char.vocabulary_
    tff_idx = cv_char.idf_

    # number of words not known to word2vec
    Unknown_q1 = np.zeros((num_qs))
    Unknown_q2 = np.zeros((num_qs))
    # feature vector for the starting word of a vector
    QS_q1 = np.zeros((num_qs,12))
    QS_q2 = np.zeros((num_qs,12))
    logger.info("Retrieving weighted sentences from word2vec with TFIDF scaling")

    for i, q in enumerate(data_frame.unigram1.values):
        # get sentence in word2vec space, number of unknown words
        # use regex to strip sentences
        question1_vectors[i], un = sent2weightedvec(re.sub('[\[\]\?\,]', ' ', q).split(), model,tff_voc, tff_idx)
        try:
            QS_q1[i] = questionStart(re.sub('[\[\]\?\,]', ' ', q).split()[0])

        # assign 'other' if question is empty
        except IndexError:
            QS_q1[i][8] = 1

        Unknown_q1[i] = un

    for i, q in enumerate(data_frame.unigram2.values):
        # get sentence in word2vec space, number of unknown words, number of words
        question2_vectors[i], unb = sent2weightedvec(re.sub('[\[\]\?\,]', ' ', q).split(), model ,tff_voc, tff_idx)
        try:
            QS_q2[i] = questionStart(re.sub('[\[\]\?\,]', ' ', q).split()[0])

        # assign 'other' if question is empty
        except IndexError:
            QS_q2[i][8] = 1

        Unknown_q2[i] = un

    logger.info("Freeing memory")
    del model, cv_char

    feature_frame = pd.DataFrame(question1_vectors-question2_vectors)

    return feature_frame
