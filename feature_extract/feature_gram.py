import pandas as pd
import numpy as np
import time
import logging

from scipy import sparse
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from multiprocessing import Pool


logger = logging.getLogger(__name__)

def decode_unigram(feature):
    """
    For a given unigram that is a list, but interpreted as a string, this makes it into a string
    
    :param feature: the string
    :return: the unigram
    """
    lst = feature[1:-1].split(',')
    lst = map(unicode, lst)
    lst = map(unicode.strip, lst)
    return lst


def jaccard(gram_q1, gram_q2, gram_list):
    """
    Basically a intersection over union
    
    :param gram_q1: array of gram of question 1
    :param gram_q2: array of gram of question 2
    :param gram_list: list containing the indices of the grams
    :return: the jaccard measure of q1, q2
    """

    v_num = (gram_q1[:, gram_list] > 0).minimum((gram_q2[:, gram_list] > 0)).sum(axis=1)
    v_den = (gram_q1[:, gram_list] > 0).maximum((gram_q2[:, gram_list] > 0)).sum(axis=1)
    v_den[np.where(v_den == 0)] = 1
    j_score = np.array(v_num.flatten()).astype(np.float32)[0, :] / np.array(v_den.flatten())[0, :]
    return j_score


def jaccard_all(gram_q1, gram_q2, gram_list):
    """
    Basically a intersection over union, but now we take everything into account 

    :param gram_q1: array of gram of question 1
    :param gram_q2: array of gram of question 2
    :param gram_list: list containing the indices of the grams
    :return: the jaccard measure of q1, q2
    """

    v_num = gram_q1[:, gram_list].minimum(gram_q2[:, gram_list]).sum(axis=1)
    v_den = gram_q1[:, gram_list].sum(axis=1) + gram_q2[:, gram_list].sum(axis=1)
    v_den[np.where(v_den == 0)] = 1
    j_score = np.array(v_num.flatten()).astype(np.float32)[0, :] / np.array(v_den.flatten())[0, :]
    return j_score


def jaccard_max(gram_q1, gram_q2, gram_list):
    """
    Basically a intersection over union, but now we take everything into account, also normalization no longer sum

    :param gram_q1: array of gram of question 1
    :param gram_q2: array of gram of question 2
    :param gram_list: list containing the indices of the grams
    :return: the jaccard measure of q1, q2
    """

    v_num = gram_q1[:, gram_list].minimum(gram_q2[:, gram_list]).sum(axis=1)
    v_den = gram_q1[:, gram_list].maximum(gram_q2[:, gram_list]).sum(axis=1)
    v_den[np.where(v_den == 0)] = 1
    j_score = np.array(v_num.flatten()).astype(np.float32)[0, :] / np.array(v_den.flatten())[0, :]
    return j_score


def tfidf_func(gram_q1, gram_q2, norm='l2', func='cosine'):
    """
    Uses the term frequency inverse document frequency to create a measure 
    
    :param gram_q1: the count grams for question 1
    :param gram_q2: the count grams for question 2
    :param norm: the norm used to normalize the counts/frequency, options are l1, l2, none
    :param func: the function to calculate the differences options are cosine, l1, l2
    :return: 
    """
    tft = TfidfTransformer(norm=norm)
    tft = tft.fit(sparse.vstack((gram_q1, gram_q2)))
    gram_q1_tf = tft.transform(gram_q1)
    gram_q2_tf = tft.transform(gram_q2)

    if func == 'cosine':
        v_num = np.array(gram_q1_tf.multiply(gram_q2_tf).sum(axis=1))[:, 0]
        v_den = np.array(np.sqrt(gram_q1_tf.multiply(gram_q1_tf).sum(axis=1)))[:, 0] * \
                np.array(np.sqrt(gram_q2_tf.multiply(gram_q2_tf).sum(axis=1)))[:, 0]

        v_num[np.where(v_den == 0)] = 1
        v_den[np.where(v_den == 0)] = 1

        cosine = 1 - v_num / v_den
        return cosine

    elif func == 'l1':
        dif = (gram_q1_tf - gram_q2_tf)
        L1 = np.array(np.abs(dif).sum(axis=1))[:, 0]
        return L1

    elif func == 'l2':
        dif = (gram_q1_tf - gram_q2_tf)
        L2 = np.sqrt(np.array(dif.multiply(dif).sum(axis=1))[:, 0])
        return L2

    else:
        print 'no correct function selected for the tfidf_func, pls change'

def extract_features(config, config_section, data_frame):
    pool = Pool(processes=config.getint('multiprocessing', 'cpu_cores'))

    if 'qid1' in data_frame.columns:
        data_frame = data_frame.drop(['qid1', 'qid2', 'question1', 'question2'], 1)
    else:
        data_frame = data_frame.drop(['question1', 'question2'], 1)
        data_frame = data_frame.rename(columns={'test_id': 'id'})

    # Load unigram
    logger.info("Retrieve unigrams")
    data_frame['unigram1'] = pool.map(decode_unigram, data_frame['unigram1'])
    data_frame['unigram2'] = pool.map(decode_unigram, data_frame['unigram2'])
    pool.close()

    # Create vocab over CHARACTERS
    logger.info("Create vocabolary for characters")
    cv_char = CountVectorizer(ngram_range=(1, 3), analyzer='char', min_df=2)
    char_freq = cv_char.fit_transform(map(lambda x: " ".join(x), data_frame['unigram1']) + map(lambda x: " ".join(x), data_frame['unigram2']))

    # Create character n-grams dictonaries (what is the gram, how many times), and a sorted list of indices
    logger.info("Create uni-, bi- and tri-gram dictonaries")
    unigrams = dict([(k, v) for (k, v) in cv_char.vocabulary_.items() if len(k) == 1])
    idx_unigrams = np.sort(unigrams.values())

    bigrams = dict([(k, v) for (k, v) in cv_char.vocabulary_.items() if len(k) == 2])
    idx_bigrams = np.sort(bigrams.values())

    trigrams = dict([(k, v) for (k, v) in cv_char.vocabulary_.items() if len(k) == 3])
    idx_trigrams = np.sort(trigrams.values())

    # Parse the questions
    logger.info("Parse questions using vocab")
    gram_q1 = cv_char.transform(map(lambda x: " ".join(x), data_frame['unigram1']))
    gram_q2 = cv_char.transform(map(lambda x: " ".join(x), data_frame['unigram2']))

    # Use the jaccard distance to obtain information on the questions
    # Unigram
    logger.info("Calculate the jaccard measures for unigrams")
    data_frame['char_unigram_jaccard'] = jaccard(gram_q1, gram_q2, idx_unigrams)
    data_frame['char_unigram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, idx_unigrams)
    data_frame['char_unigram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, idx_unigrams)

    # Bigram
    logger.info("Calculate the jaccard measures for bigrams")
    data_frame['char_bigram_jaccard'] = jaccard(gram_q1, gram_q2, idx_bigrams)
    data_frame['char_bigram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, idx_bigrams)
    data_frame['char_bigram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, idx_bigrams)

    # Trigram
    logger.info("Calculate the jaccard measures for trigrams")
    data_frame['char_trigram_jaccard'] = jaccard(gram_q1, gram_q2, idx_trigrams)
    data_frame['char_trigram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, idx_trigrams)
    data_frame['char_trigram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, idx_trigrams)

    # Unigram + Bigram + Trigram
    logger.info("Calculate the jaccard measures for uni + bi + tri grams")
    data_frame['char_allgram_jaccard'] = jaccard(gram_q1, gram_q2, range(0, len(idx_unigrams) + len(idx_bigrams) + len(idx_trigrams)))
    data_frame['char_allgram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, range(0, len(idx_unigrams) + len(idx_bigrams) + len(idx_trigrams)))
    data_frame['char_allgram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, range(0, len(idx_unigrams) + len(idx_bigrams) + len(idx_trigrams)))

    # Get the tf-idf features
    logger.info("Calculate the tf-idf features")
    data_frame['char_tfidf_l2_cosine'] = tfidf_func(gram_q1, gram_q2, norm='l2', func='cosine')
    data_frame['char_tfidf_l2_l1'] = tfidf_func(gram_q1, gram_q2, norm='l2', func='l1')
    data_frame['char_tfidf_l2_l2'] = tfidf_func(gram_q1, gram_q2, norm='l2', func='l2')


    # Create vocab over WORDS
    logger.info("Create the vocabulary for the words")
    cv_word = CountVectorizer(ngram_range=(1, 3), analyzer='word', min_df=2)
    char_word = cv_word.fit_transform(map(lambda x: " ".join(x), data_frame['unigram1']) + map(lambda x: " ".join(x), data_frame['unigram2']))

    # Create character n-grams dictonaries (what is the gram, how many times), and a sorted list of indices
    logger.info("Create uni-, bi- and tri-gram dictonaries")
    unigrams = dict([(k, v) for (k, v) in cv_word.vocabulary_.items() if len(k.split()) == 1])
    idx_unigrams = np.sort(unigrams.values())

    bigrams = dict([(k, v) for (k, v) in cv_word.vocabulary_.items() if len(k.split()) == 2])
    idx_bigrams = np.sort(bigrams.values())

    trigrams = dict([(k, v) for (k, v) in cv_word.vocabulary_.items() if len(k.split()) == 3])
    idx_trigrams = np.sort(trigrams.values())

    # Parse the questions
    logger.info("Parse questions using vocab")
    word_q1 = cv_word.transform(map(lambda x: " ".join(x), data_frame['unigram1']))
    word_q2 = cv_word.transform(map(lambda x: " ".join(x), data_frame['unigram2']))

    # Use the jaccard distance to obtain information on the questions
    # Unigram
    logger.info("Calculate the jaccard measures for unigrams")
    data_frame['word_unigram_jaccard'] = jaccard(word_q1, word_q2, idx_unigrams)
    data_frame['word_unigram_jaccard_all'] = jaccard_all(word_q1, word_q2, idx_unigrams)
    data_frame['word_unigram_jaccard_max'] = jaccard_max(word_q1, word_q2, idx_unigrams)

    # Bigram
    logger.info("Calculate the jaccard measures for bigrams")
    data_frame['word_bigram_jaccard'] = jaccard(word_q1, word_q2, idx_bigrams)
    data_frame['word_bigram_jaccard_all'] = jaccard_all(word_q1, word_q2, idx_bigrams)
    data_frame['word_bigram_jaccard_max'] = jaccard_max(word_q1, word_q2, idx_bigrams)

    # Trigram
    logger.info("Calculate the jaccard measures for trigrams")
    data_frame['word_trigram_jaccard'] = jaccard(word_q1, word_q2, idx_trigrams)
    data_frame['word_trigram_jaccard_all'] = jaccard_all(word_q1, word_q2, idx_trigrams)
    data_frame['word_trigram_jaccard_max'] = jaccard_max(word_q1, word_q2, idx_trigrams)

    # Unigram + Bigram + Trigram
    logger.info("Calculate the jaccard measures for uni + bi + tri grams")
    data_frame['word_allgram_jaccard'] = jaccard(word_q1, word_q2, range(0, len(idx_unigrams) + len(idx_bigrams) + len(idx_trigrams)))
    data_frame['word_allgram_jaccard_all'] = jaccard_all(word_q1, word_q2, range(0, len(idx_unigrams) + len(idx_bigrams) + len(idx_trigrams)))
    data_frame['word_allgram_jaccard_max'] = jaccard_max(word_q1, word_q2, range(0, len(idx_unigrams) + len(idx_bigrams) + len(idx_trigrams)))

    # Get the tf-idf features
    logger.info("Calculate the tf-idf features")
    data_frame['word_tfidf_l2_cosine'] = tfidf_func(word_q1, word_q2, norm='l2', func='cosine')
    data_frame['word_tfidf_l2_l1'] = tfidf_func(word_q1, word_q2, norm='l2', func='l1')
    data_frame['word_tfidf_l2_l2'] = tfidf_func(word_q1, word_q2, norm='l2', func='l2')

    # Remove the unigram
    data_frame = data_frame.drop(['unigram1', 'unigram2'], 1)

    return data_frame

# if __name__ == "__main__":
#     doc_path = "../../kaggle-quora-data/preprocessing/train_preprocessed.csv"
#     data_frame = pd.read_csv(doc_path, encoding='utf-8')
#
#     pool = Pool(processes=4)
#
#     # Load unigram
#     data_frame['unigram1'] = pool.map(decode_unigram, data_frame['unigram1'])
#     data_frame['unigram2'] = pool.map(decode_unigram, data_frame['unigram2'])
#     pool.close()
#
#     # Create vocab over CHARACTERS
#     cv_char = CountVectorizer(ngram_range=(1, 3), analyzer='char', min_df=2)
#     char_freq = cv_char.fit_transform(
#         map(lambda x: " ".join(x), data_frame['unigram1']) + map(lambda x: " ".join(x), data_frame['unigram2']))
#
#     # Create character n-grams dictonaries (what is the gram, how many times), and a sorted list of indices
#     print 'N-gram character classification over training set:'
#     unigrams = dict([(k, v) for (k, v) in cv_char.vocabulary_.items() if len(k) == 1])
#     idx_unigrams = np.sort(unigrams.values())
#     print 'Unigrams:', len(unigrams)
#     bigrams = dict([(k, v) for (k, v) in cv_char.vocabulary_.items() if len(k) == 2])
#     idx_bigrams = np.sort(bigrams.values())
#     print 'Bigrams: ', len(bigrams)
#     trigrams = dict([(k, v) for (k, v) in cv_char.vocabulary_.items() if len(k) == 3])
#     idx_trigrams = np.sort(trigrams.values())
#     print 'Trigrams:', len(trigrams)
#
#     # Parse the questions
#     gram_q1 = cv_char.transform(map(lambda x: " ".join(x), data_frame['unigram1']))
#     gram_q2 = cv_char.transform(map(lambda x: " ".join(x), data_frame['unigram2']))
#
#     # Use the jaccard distance to obtain information on the questions
#
#     t0 = time.time()
#     # Unigram
#     data_frame['char_unigram_jaccard'] = jaccard(gram_q1, gram_q2, idx_unigrams)
#     data_frame['char_unigram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, idx_unigrams)
#     data_frame['char_unigram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, idx_unigrams)
#     print time.time() - t0
#
#     t0 = time.time()
#     # Bigram
#     data_frame['char_bigram_jaccard'] = jaccard(gram_q1, gram_q2, idx_bigrams)
#     data_frame['char_bigram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, idx_bigrams)
#     data_frame['char_bigram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, idx_bigrams)
#     print time.time() - t0
#     t0 = time.time()
#     # Trigram
#     data_frame['char_trigram_jaccard'] = jaccard(gram_q1, gram_q2, idx_trigrams)
#     data_frame['char_trigram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, idx_trigrams)
#     data_frame['char_trigram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, idx_trigrams)
#     print time.time() - t0
#     t0 = time.time()
#     # Unigram + Bigram + Trigram
#     data_frame['char_allgram_jaccard'] = jaccard(gram_q1, gram_q2, range(0, len(idx_unigrams) + len(idx_bigrams) + len(idx_trigrams) ))
#     data_frame['char_allgram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, range(0, len(idx_unigrams) + len(idx_bigrams) + len(idx_trigrams)))
#     data_frame['char_allgram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, range(0, len(idx_unigrams) + len(idx_bigrams) + len(idx_trigrams)))
#     print time.time() - t0
#
#     # Get the tf-idf features
#     t0 = time.time()
#     data_frame['char_tfidf_l2_cosine'] = tfidf_func(gram_q1, gram_q2, norm='l2', func='cosine')
#     data_frame['char_tfidf_l2_l1'] = tfidf_func(gram_q1, gram_q2, norm='l2', func='l1')
#     data_frame['char_tfidf_l2_l2'] = tfidf_func(gram_q1, gram_q2, norm='l2', func='l2')
#     print time.time() - t0
#
#
#
#
#     # Create vocab over WORDS
#     cv_word = CountVectorizer(ngram_range=(1, 3), analyzer='word', min_df=2)
#     char_word = cv_word.fit_transform(
#         map(lambda x: " ".join(x), data_frame['unigram1']) + map(lambda x: " ".join(x), data_frame['unigram2']))
#
#     # Create character n-grams dictonaries (what is the gram, how many times), and a sorted list of indices
#     print 'N-gram character classification over training set:'
#     unigrams = dict([(k, v) for (k, v) in cv_word.vocabulary_.items() if len(k.split()) == 1])
#     idx_unigrams = np.sort(unigrams.values())
#     print 'Unigrams:', len(unigrams)
#     bigrams = dict([(k, v) for (k, v) in cv_word.vocabulary_.items() if len(k.split()) == 2])
#     idx_bigrams = np.sort(bigrams.values())
#     print 'Bigrams: ', len(bigrams)
#     trigrams = dict([(k, v) for (k, v) in cv_word.vocabulary_.items() if len(k.split()) == 3])
#     idx_trigrams = np.sort(trigrams.values())
#     print 'Trigrams:', len(trigrams)
#
#     # Parse the questions
#     word_q1 = cv_word.transform(map(lambda x: " ".join(x), data_frame['unigram1']))
#     word_q2 = cv_word.transform(map(lambda x: " ".join(x), data_frame['unigram2']))
#
#     # Use the jaccard distance to obtain information on the questions
#     t0 = time.time()
#     # Unigram
#     data_frame['word_unigram_jaccard'] = jaccard(word_q1, word_q2, idx_unigrams)
#     data_frame['word_unigram_jaccard_all'] = jaccard_all(word_q1, word_q2, idx_unigrams)
#     data_frame['word_unigram_jaccard_max'] = jaccard_max(word_q1, word_q2, idx_unigrams)
#     print time.time() - t0
#
#     t0 = time.time()
#     # Bigram
#     data_frame['word_bigram_jaccard'] = jaccard(word_q1, word_q2, idx_bigrams)
#     data_frame['word_bigram_jaccard_all'] = jaccard_all(word_q1, word_q2, idx_bigrams)
#     data_frame['word_bigram_jaccard_max'] = jaccard_max(word_q1, word_q2, idx_bigrams)
#     print time.time() - t0
#     t0 = time.time()
#     # Trigram
#     data_frame['word_trigram_jaccard'] = jaccard(word_q1, word_q2, idx_trigrams)
#     data_frame['word_trigram_jaccard_all'] = jaccard_all(word_q1, word_q2, idx_trigrams)
#     data_frame['word_trigram_jaccard_max'] = jaccard_max(word_q1, word_q2, idx_trigrams)
#     print time.time() - t0
#     t0 = time.time()
#     # Unigram + Bigram + Trigram
#     data_frame['word_allgram_jaccard'] = jaccard(word_q1, word_q2,
#                                                  range(0, len(idx_unigrams) + len(idx_bigrams) + len(idx_trigrams)))
#     data_frame['word_allgram_jaccard_all'] = jaccard_all(word_q1, word_q2, range(0, len(idx_unigrams) + len(
#         idx_bigrams) + len(idx_trigrams)))
#     data_frame['word_allgram_jaccard_max'] = jaccard_max(word_q1, word_q2, range(0, len(idx_unigrams) + len(
#         idx_bigrams) + len(idx_trigrams)))
#     print time.time() - t0
#
#     # Get the tf-idf features
#     t0 = time.time()
#     data_frame['word_tfidf_l2_cosine'] = tfidf_func(word_q1, word_q2, norm='l2', func='cosine')
#     data_frame['word_tfidf_l2_l1'] = tfidf_func(word_q1, word_q2, norm='l2', func='l1')
#     data_frame['word_tfidf_l2_l2'] = tfidf_func(word_q1, word_q2, norm='l2', func='l2')
#     print time.time() - t0
#
#     print data_frame.tail(10)