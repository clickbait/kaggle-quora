import pandas as pd
import numpy as np
import random
import time
import logging

from sklearn.feature_extraction.text import  TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer

import matplotlib.pyplot as plt
from matplotlib import mlab

from multiprocessing import Pool


def decode_unigram(feature):
    """
    For a given unigram that is a list, but interpreted as a string, this makes it into a string

    :param feature: the string
    :return: the unigram
    """
    lst = feature[1:-1].split(',')
    lst = map(unicode, lst)
    lst = map(unicode.strip, lst)
    return lst


def extract_features(config, config_section, data_frame):
    pool = Pool(processes=config.getint('multiprocessing', 'cpu_cores'))

    if 'qid1' in data_frame.columns:
        data_frame = data_frame.drop(['qid1', 'qid2', 'question1', 'question2'], 1)
    else:
        data_frame = data_frame.drop(['question1', 'question2'], 1)
        data_frame = data_frame.rename(columns={'test_id': 'id'})

    return 1

if __name__ == "__main__":
    doc_path = "../../kaggle-quora-data/preprocessing/train_preprocessed.csv"
    data_frame = pd.read_csv(doc_path, encoding='utf-8').fillna('')

    test_doc_path = "../../kaggle-quora-data/preprocessing/test_preprocessed.0.csv"
    test_data_frame = pd.read_csv(test_doc_path, encoding='utf-8').fillna('')

    pool = Pool(processes=4)

    # Load unigram
    data_frame['unigram1'] = pool.map(decode_unigram, data_frame['unigram1'])
    data_frame['unigram2'] = pool.map(decode_unigram, data_frame['unigram2'])
    test_data_frame['unigram1'] = pool.map(decode_unigram, test_data_frame['unigram1'])
    test_data_frame['unigram2'] = pool.map(decode_unigram, test_data_frame['unigram2'])
    pool.close()

    # Build
    vectorizer = TfidfVectorizer(max_df=0.05, max_features=1000, min_df=100, use_idf=True)
    X_train_tfidf = vectorizer.fit_transform(
        map(lambda x: " ".join(x), data_frame['unigram1']) + map(lambda x: " ".join(x), data_frame['unigram2'])
    )

    X_train_tfidf_q1 = vectorizer.transform(
        map(lambda x: " ".join(x), data_frame['unigram1'])
    )

    X_train_tfidf_q2 = vectorizer.transform(
        map(lambda x: " ".join(x), data_frame['unigram2'])
    )


    X_test_tfidf_q1 = vectorizer.transform(
        map(lambda x: " ".join(x), test_data_frame['unigram1'])
    )

    X_test_tfidf_q2 = vectorizer.transform(
        map(lambda x: " ".join(x), test_data_frame['unigram2'])
    )
    feat_names = vectorizer.get_feature_names()
    print feat_names
    print '\n', X_train_tfidf.shape[1]

    svd = TruncatedSVD(100)
    lsa = make_pipeline(svd, Normalizer(copy=False))
    X_train_lsa = lsa.fit_transform(X_train_tfidf)

    X_train_lsa_q1 = lsa.transform(X_train_tfidf_q1)
    X_train_lsa_q2 = lsa.transform(X_train_tfidf_q2)

    X_test_lsa_q1 = lsa.transform(X_test_tfidf_q1)
    X_test_lsa_q2 = lsa.transform(X_test_tfidf_q2)
    print np.sum(np.abs(X_train_lsa_q1 - X_train_lsa_q2), axis=1)[:14]
    print np.sum(np.abs(X_test_lsa_q1 - X_test_lsa_q2), axis=1)
    # is_duplicate: 5, 7, 11, 12, 13

    explained_variance = svd.explained_variance_ratio_.sum()
    print("  Explained variance of the SVD step: {}%".format(int(explained_variance * 100)))






