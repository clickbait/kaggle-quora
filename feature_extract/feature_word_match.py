# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from collections import Counter
from nltk.corpus import stopwords

#https://www.kaggle.com/dasolmar/xgb-with-whq-jaccard
def add_word_count(x, df, word):
    x['q1_' + word] = df['question1'].apply(lambda x: (word in unicode(x).lower()) * 1)
    x['q2_' + word] = df['question2'].apply(lambda x: (word in unicode(x).lower()) * 1)
    x[word + '_both'] = x['q1_' + word] * x['q2_' + word]


def extract_features(config, config_section, data_frame):
    # If a word appears only once, we ignore it completely (likely a typo)
    # Epsilon defines a smoothing constant, which makes the effect of extremely rare words smaller
    def get_weight(count, eps=10000, min_count=2):
        return 0 if count < min_count else 1 / (count + eps)

    # TODO: use own stuff
    train_qs = pd.Series(data_frame['question1'].tolist() + data_frame['question2'].tolist()).astype(unicode)
    words = (" ".join(train_qs)).lower().split()
    counts = Counter(words)
    weights = {word: get_weight(count) for word, count in counts.items()}

    stops = set(stopwords.words("english"))

    def word_shares(row):
        q1_list = unicode(row['question1']).lower().split()
        q1 = set(q1_list)
        q1words = q1.difference(stops)
        if len(q1words) == 0:
            return '0:0:0:0:0:0:0:0'

        q2_list = unicode(row['question2']).lower().split()
        q2 = set(q2_list)
        q2words = q2.difference(stops)
        if len(q2words) == 0:
            return '0:0:0:0:0:0:0:0'

        words_hamming = sum(1 for i in zip(q1_list, q2_list) if i[0] == i[1]) / max(len(q1_list), len(q2_list))

        q1stops = q1.intersection(stops)
        q2stops = q2.intersection(stops)

        q1_2gram = set([i for i in zip(q1_list, q1_list[1:])])
        q2_2gram = set([i for i in zip(q2_list, q2_list[1:])])

        shared_2gram = q1_2gram.intersection(q2_2gram)

        shared_words = q1words.intersection(q2words)
        shared_weights = [weights.get(w, 0) for w in shared_words]
        q1_weights = [weights.get(w, 0) for w in q1words]
        q2_weights = [weights.get(w, 0) for w in q2words]
        total_weights = q1_weights + q1_weights

        R1 = np.sum(shared_weights) / np.sum(total_weights)  # tfidf share
        R2 = len(shared_words) / (len(q1words) + len(q2words) - len(shared_words))  # count share
        R31 = len(q1stops) / len(q1words)  # stops in q1
        R32 = len(q2stops) / len(q2words)  # stops in q2
        Rcosine_denominator = (np.sqrt(np.dot(q1_weights, q1_weights)) * np.sqrt(np.dot(q2_weights, q2_weights)))
        Rcosine = np.dot(shared_weights, shared_weights) / Rcosine_denominator
        if len(q1_2gram) + len(q2_2gram) == 0:
            R2gram = 0
        else:
            R2gram = len(shared_2gram) / (len(q1_2gram) + len(q2_2gram))
        return '{}:{}:{}:{}:{}:{}:{}:{}'.format(R1, R2, len(shared_words), R31, R32, R2gram, Rcosine, words_hamming)

    df = data_frame
    df['word_shares'] = df.apply(word_shares, axis=1, raw=True)

    feature_frame = pd.DataFrame({'id': df.iloc[:, 0]})

    feature_frame['word_match'] = df['word_shares'].apply(lambda x: float(x.split(':')[0]))
    feature_frame['word_match_2root'] = np.sqrt(feature_frame['word_match'])
    feature_frame['tfidf_word_match'] = df['word_shares'].apply(lambda x: float(x.split(':')[1]))
    feature_frame['shared_count'] = df['word_shares'].apply(lambda x: float(x.split(':')[2]))

    feature_frame['stops1_ratio'] = df['word_shares'].apply(lambda x: float(x.split(':')[3]))
    feature_frame['stops2_ratio'] = df['word_shares'].apply(lambda x: float(x.split(':')[4]))
    feature_frame['shared_2gram'] = df['word_shares'].apply(lambda x: float(x.split(':')[5]))
    feature_frame['cosine'] = df['word_shares'].apply(lambda x: float(x.split(':')[6]))
    # feature_frame['words_hamming'] = df['word_shares'].apply(lambda x: float(x.split(':')[7]))
    feature_frame['diff_stops_r'] = feature_frame['stops1_ratio'] - feature_frame['stops2_ratio']

    feature_frame['len_q1'] = df['question1'].apply(lambda x: len(unicode(x)))
    feature_frame['len_q2'] = df['question2'].apply(lambda x: len(unicode(x)))
    feature_frame['diff_len'] = feature_frame['len_q1'] - feature_frame['len_q2']

    feature_frame['caps_count_q1'] = df['question1'].apply(lambda x: sum(1 for i in unicode(x) if i.isupper()))
    feature_frame['caps_count_q2'] = df['question2'].apply(lambda x: sum(1 for i in unicode(x) if i.isupper()))
    feature_frame['diff_caps'] = feature_frame['caps_count_q1'] - feature_frame['caps_count_q2']

    feature_frame['len_char_q1'] = df['question1'].apply(lambda x: len(unicode(x).replace(' ', '')))
    feature_frame['len_char_q2'] = df['question2'].apply(lambda x: len(unicode(x).replace(' ', '')))
    feature_frame['diff_len_char'] = feature_frame['len_char_q1'] - feature_frame['len_char_q2']

    feature_frame['len_word_q1'] = df['question1'].apply(lambda x: len(unicode(x).split()))
    feature_frame['len_word_q2'] = df['question2'].apply(lambda x: len(unicode(x).split()))
    feature_frame['diff_len_word'] = feature_frame['len_word_q1'] - feature_frame['len_word_q2']

    feature_frame['avg_world_len1'] = feature_frame['len_char_q1'] / feature_frame['len_word_q1']
    feature_frame['avg_world_len2'] = feature_frame['len_char_q2'] / feature_frame['len_word_q2']
    feature_frame['diff_avg_word'] = feature_frame['avg_world_len1'] - feature_frame['avg_world_len2']

    feature_frame['exactly_same'] = (df['question1'] == df['question2']).astype(int)
    feature_frame['duplicated'] = df.duplicated(['question1', 'question2']).astype(int)
    add_word_count(feature_frame, df, 'how')
    add_word_count(feature_frame, df, 'what')
    add_word_count(feature_frame, df, 'which')
    add_word_count(feature_frame, df, 'who')
    add_word_count(feature_frame, df, 'where')
    add_word_count(feature_frame, df, 'when')
    add_word_count(feature_frame, df, 'why')

    return feature_frame
