from multiprocessing import Pool
import nltk
import numpy as np
import pandas as pd
from collections import Counter
import operator
import logging
logger = logging.getLogger(__name__)


def count_wordtypes(tokenized_text):
    tag_dictionary = {'VBG': '29', 'FW': '5', 'VBN': '30', ',': '40', 'VBP': '31', 'WDT': '33', 'JJ': '7', 'WP': '35',
                      'VBZ': '32', 'DT': '3', 'PP': '19', '"': '48', 'RP': '23', '$': '38', 'NN': '12', ')': '43',
                      '(': '42', 'VBD': '28', 'POS': '17', '.': '39', 'TO': '25', 'PRP': '18', 'RB': '20', ':': '41',
                      'NNS': '13', 'NNP': '14', '#': '37', 'VB': '27', 'WRB': '36', '`': '45', 'CC': '1', 'LS': '10',
                      'PDT': '16', 'RBS': '22', 'RBR': '21', 'CD': '2', 'EX': '4', 'IN': '6', 'MD': '11', 'NNPS': '15',
                      "'": '47', 'JJS': '9', 'JJR': '8', 'SYM': '24', 'UH': '26', 'PRP$': '49'}

    tagged = nltk.pos_tag(tokenized_text)

    text_tags = [k[1] for k in tagged if k[1] in tag_dictionary.keys()]
    count_array = np.zeros(49)
    counts = Counter(text_tags)
    for wordtype in counts:
        count_array[int(tag_dictionary[wordtype])-1] = int(counts[wordtype])

    return count_array


def encode_wordtypes(tokenized_text):
    tag_dictionary = {'VBG': '29', 'FW': '5', 'VBN': '30', ',': '40', 'VBP': '31', 'WDT': '33', 'JJ': '7', 'WP': '35',
                      'VBZ': '32', 'DT': '3', 'PP': '19', '"': '48', 'RP': '23', '$': '38', 'NN': '12', ')': '43',
                      '(': '42', 'VBD': '28', 'POS': '17', '.': '39', 'TO': '25', 'PRP': '18', 'RB': '20', ':': '41',
                      'NNS': '13', 'NNP': '14', '#': '37', 'VB': '27', 'WRB': '36', '`': '45', 'CC': '1', 'LS': '10',
                      'PDT': '16', 'RBS': '22', 'RBR': '21', 'CD': '2', 'EX': '4', 'IN': '6', 'MD': '11', 'NNPS': '15',
                      "'": '47', 'JJS': '9', 'JJR': '8', 'SYM': '24', 'UH': '26', 'PRP$': '49'}

    text_tagged = nltk.pos_tag(tokenized_text)
    sen_length = len(text_tagged)
    encode_array = np.zeros((49,sen_length))
    for i in range(0,sen_length):
        print i
        print int(tag_dictionary[text_tagged[i][1]])
        encode_array[int(tag_dictionary[text_tagged[i][1]])-1][i] = 1
    return encode_array


def decode_features(feature):
    lst = feature[1:-1].split(',')
    lst = map(unicode, lst)
    lst = map(unicode.strip, lst)
    return lst


def extract_features(config, config_section, data_frame):
    # Feature extraction
    logger.info("Creating counts for pos")
    pool = Pool(processes=config.getint('multiprocessing', 'cpu_cores'))

    data_frame['unigram1'] = pool.map(decode_features, data_frame['unigram1'])
    data_frame['unigram2'] = pool.map(decode_features, data_frame['unigram2'])

    # OLD FILTER FOR REMOVING STUFF
    data_frame['unigram1'] = data_frame['unigram1'].apply(lambda x: filter(None, x))
    data_frame['unigram2'] = data_frame['unigram2'].apply(lambda x: filter(None, x))

    # Create DataFrame
    start = {'id': data_frame.iloc[:, 0]}
    #columns_q1 = []
    #columns_q2 = []
    columns = []

    for i in range(0, 49):
        #start['pos_q1_' + str(i)] = 0
        #start['pos_q2_' + str(i)] = 0
        start['pos_' + str(i)] = 0

        #columns_q1.append('pos_q1_' + str(i))
        #columns_q2.append('pos_q2_' + str(i))
        columns.append('pos_' + str(i))

    feature_frame = pd.DataFrame(start)
    logger.info("Counting pos tags for question 1")
    q1 = pool.map(count_wordtypes, data_frame['unigram1'])
    # print q1
    logger.info("Counting pos tags for question 2")
    q2 = pool.map(count_wordtypes, data_frame['unigram2'])

    q = map(operator.sub, q1, q2)
    q = pool.map(abs, q)

    feature_frame[columns] = q

    #feature_frame = feature_frame.drop(columns_q1)
    #feature_frame = feature_frame.drop(columns_q2)

    pool.close()

    return feature_frame
