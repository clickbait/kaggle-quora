from multiprocessing import Pool

import gensim
import pandas as pd
import numpy as np
import os

from scipy.stats import skew, kurtosis
from nltk.corpus import stopwords

import logging

from feature_extract.distance_wrapper import cityblock_wrapper, canberra_wrapper, chebyshev_wrapper, braycurtis_wrapper, \
    cosine_wrapper, dice_wrapper, euclidean_wrapper, hamming_wrapper, jaccard_wrapper, decode_from_csv, \
    correlation_wrapper, kulsinski_wrapper, matching_wrapper, minkowski_wrapper, rogerstanimoto_wrapper, \
    russellrao_wrapper, sokalmichener_wrapper, sokalsneath_wrapper, yule_wrapper

logging.getLogger('gensim').setLevel(logging.ERROR)
logger = logging.getLogger(__name__)

stop_words = stopwords.words('english')


def wmd(s1, s2, model):
    s1 = [w for w in s1 if w not in stop_words]
    s2 = [w for w in s2 if w not in stop_words]
    return model.wmdistance(s1, s2)


def sent2vec(s, model):
    words = [w for w in s if not w in stop_words]
    words = [w for w in words if w.isalpha()]
    M = []
    for w in words:
        try:
            M.append(model[w])
        except:
            continue
    M = np.array(M, dtype=np.float64)
    v = M.sum(axis=0)

    if len(M) != 0:
        return v / np.sqrt((v ** 2).sum())
    else:
        return np.zeros((300))


def extract_features(config, config_section, data_frame):
    data_path = config.get('data', 'data_path')
    model_base_path = config.get('data', 'model_path')
    model_file = config.get(config_section, 'model_file')
    model_path = os.path.join(data_path, model_base_path, model_file)

    pool = Pool(processes=8)

    data_frame['unigram1'] = pool.map(decode_from_csv, data_frame['unigram1'])
    data_frame['unigram2'] = pool.map(decode_from_csv, data_frame['unigram2'])

    # Tbh no idea what init_sims does, but we have too little time to focus on it
    # norm_model = gensim.models.KeyedVectors.load_word2vec_format(model_path,
    #                                                              binary=config.getboolean(config_section, 'is_binary'))
    # norm_model.init_sims(replace=True)
    # logger.info("Norm WMD")

    # data_frame['norm_wmd'] = data_frame.apply(lambda x: wmd(x['unigram1'], x['unigram2'], norm_model), axis=1)

    # Since we cant delete it, since it is referenced in a lambda function, we clear it ...
    # norm_model = []

    # Get the normal model to work
    model = gensim.models.KeyedVectors.load_word2vec_format(model_path, binary=config.getboolean(config_section, 'is_binary'))
    # logger.info("WMD")
    # data_frame['wmd'] = data_frame.apply(lambda x: wmd(x['unigram1'], x['unigram2'], model), axis=1)

    logger.info("Sent2vec")
    question1_vectors = np.zeros((data_frame.shape[0], 300))
    question2_vectors = np.zeros((data_frame.shape[0], 300))

    for i, q in enumerate(data_frame.unigram1.values):
        question1_vectors[i] = sent2vec(q, model)

    for i, q in enumerate(data_frame.unigram2.values):
        question2_vectors[i] = sent2vec(q, model)
    model = []

    zipped_vectors = zip(question1_vectors, question2_vectors)

    feature_frame = pd.DataFrame({'id': data_frame.iloc[:, 0]})

    logger.info("Braycurtis distance")
    feature_frame['braycurtis_distance'] = pool.map(braycurtis_wrapper, zipped_vectors)

    logger.info("Canberra distance")
    feature_frame['canberra_distance'] = pool.map(canberra_wrapper, zipped_vectors)

    logger.info("Chebyshev distance")
    feature_frame['chebyshev_distance'] = pool.map(chebyshev_wrapper, zipped_vectors)

    logger.info("Cityblock distance")
    feature_frame['cityblock_distance'] = pool.map(cityblock_wrapper, zipped_vectors)

    logger.info("Correlation distance")
    feature_frame['correlation_distance'] = pool.map(correlation_wrapper, zipped_vectors)

    logger.info("Cosine distance")
    feature_frame['cosine_distance'] = pool.map(cosine_wrapper, zipped_vectors)

    logger.info("Dice distance")
    feature_frame['dice_distance'] = pool.map(dice_wrapper, zipped_vectors)

    logger.info("Euclidean distance")
    feature_frame['euclidean_distance'] = pool.map(euclidean_wrapper, zipped_vectors)

    logger.info("Hamming distance")
    feature_frame['hamming_distance'] = pool.map(hamming_wrapper, zipped_vectors)

    logger.info("Jaccard distance")
    feature_frame['jaccard_distance'] = pool.map(jaccard_wrapper, zipped_vectors)

    logger.info("Kulsinski distance")
    feature_frame['kulsinski_distance'] = pool.map(kulsinski_wrapper, zipped_vectors)

    logger.info("Matching distance")
    feature_frame['matching_distance'] = pool.map(matching_wrapper, zipped_vectors)

    #logger.info("Minkowski distance")
    #feature_frame['minkowski_distance'] = pool.map(minkowski_wrapper, zipped_vectors)

    logger.info("Rogerstanimoto distance")
    feature_frame['rogerstanimoto_distance'] = pool.map(rogerstanimoto_wrapper, zipped_vectors)

    logger.info("Russellrao distance")
    feature_frame['russellrao_distance'] = pool.map(russellrao_wrapper, zipped_vectors)

    logger.info("Sokalmichener distance")
    feature_frame['sokalmichener_distance'] = pool.map(sokalmichener_wrapper, zipped_vectors)

    logger.info("Sokalsneath distance")
    feature_frame['sokalsneath_distance'] = pool.map(sokalsneath_wrapper, zipped_vectors)

    logger.info("Yule distance")
    feature_frame['yule_distance'] = pool.map(yule_wrapper, zipped_vectors)

    logger.info("Skew")
    feature_frame['skew_q1vec'] = pool.map(skew, question1_vectors)
    feature_frame['skew_q2vec'] = pool.map(skew, question2_vectors)

    logger.info("Kurtosis")
    feature_frame['kur_q1vec'] = pool.map(kurtosis, question1_vectors)
    feature_frame['kur_q2vec'] = pool.map(kurtosis, question2_vectors)

    pool.close()

    return feature_frame
