# From: http://mccormickml.com/2016/04/12/googles-pretrained-word2vec-model-in-python/
# Updated with https://rare-technologies.com/word2vec-tutorial/
import os

import pandas as pd
from gensim.models import KeyedVectors

from word2vec.mean_embedding_vectorizer import MeanEmbeddingVectorizer


def extract_features(config, config_section, data_frame):
    data_path = config.get('data', 'data_path')
    model_base_path = config.get('data', 'model_path')
    model_file = config.get(config_section, 'model_file')
    model_path = os.path.join(data_path, model_base_path, model_file)

    model = KeyedVectors.load_word2vec_format(model_path, binary=config.getboolean(config_section, 'is_binary'))

    ev = MeanEmbeddingVectorizer(model)
    embedding_matrix1 = ev.transform(data_frame['unigram1'])
    embedding_matrix2 = ev.transform(data_frame['unigram2'])

    feature_frame = pd.DataFrame(embedding_matrix1 - embedding_matrix2)

    return feature_frame