import pandas as pd
import numpy as np
import glob
import os
import random
import csv

from ConfigParser import SafeConfigParser
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
#from data.dataset_splitter import split_data
from classification.xgboost_simple import ClassifierXgboost

config = SafeConfigParser()
config.read('config/daniele.ini')

train_path = 'kaggle-quora-data/train.csv'
submission_path = 'results/stack/'

nfolds = 10

"""
:param train_files_pred 
    a list of file names, one for each classifier, containing the predictions for the training set
:param test_files_pred
    a list of file names, one for each classifier, containing the predictions for the test set
:param fold_no 
    the number of folds to use for k-fold
"""
def stacking(train_files_pred,test_files_pred, nfolds=3):
    print "Stacking..."

    train_lbls = pd.read_csv(train_path, encoding='utf-8')['is_duplicate'].values
    
    # 1. Read the predicted probabilities for the training set
    train_proba = []
    print train_files_pred    
    for f in train_files_pred:
        train_proba_df = pd.read_csv(f, encoding='utf-8')
  
        train_proba.append(np.asarray(train_proba_df['is_duplicate']))

        del train_proba_df
 
    # vstack add to the bottom, that's why we have to transpose (we're dealing with row arrays)
    train_pred_stack = np.vstack(train_proba)
    train_pred_stack = train_pred_stack.transpose()
    print(train_pred_stack)
    #train_feat_stack = np.hstack(train_feat)
    # Run with features
    #train_stack = np.hstack([train_pred_stack, train_feat_stack])
    # Run with only predictions
    train_stack = train_pred_stack
    del train_proba, train_pred_stack

    # Train a meta-classifier for a number of folds
    meta_clf = ClassifierXgboost(config)
    
    for fold in range(0, nfolds):
        fold_dir = os.path.join(submission_path, 'fold%d' % fold)
        if not os.path.isdir(fold_dir):
            os.mkdir(fold_dir)

        x_train, x_valid, y_train, y_valid = train_test_split(train_stack, train_lbls, test_size=0.1, random_state=4242 + fold)
        meta_clf.fit(x_train, y_train, x_valid, y_valid)

        # 2. Read the predicted probabilities for the test set. Take care of the split
        test_proba = []
          
        # Get predictions for each classifier and stack them together. Only retrieve rows in the feat set
        for clf in test_files_pred:
            test_proba_df = pd.read_csv(clf, encoding='utf-8')
            test_proba.append(np.asarray(test_proba_df['is_duplicate'][:]))
            test_ids = (test_proba_df['test_id']).astype(int)
            del test_proba_df
         
        test_pred_stack = np.vstack(test_proba)
        test_pred_stack = test_pred_stack.transpose()
        #test_feat_stack = np.hstack(test_feat)

        #test_stack = np.hstack([test_pred_stack, test_feat_stack])
        test_stack = test_pred_stack
        del test_pred_stack

        print "Done stacking. Creating submission file...[fold %d]" % (fold)
        
        test_predictions = meta_clf.predict(test_stack)
        final_submission = pd.DataFrame(columns=['test_id', 'is_duplicate'])
        final_submission['test_id'] = test_ids
        final_submission['is_duplicate'] = test_predictions

        final_submission.to_csv(os.path.join(fold_dir, 'final_submission_fold_%d.csv' % ( fold)), encoding='utf-8', index=False)

        test_proba = []
        test_feat = []

        del test_proba, test_feat

def blend_folds(nfolds):
    stacked_df = concat_predictions(os.path.join(submission_path, 'fold0'))
    for fold in range(1, nfolds):
        fold_dir = os.path.join(submission_path, 'fold%d' % fold)
        stacked_df['is_duplicate'] += concat_predictions(fold_dir)['is_duplicate']
    stacked_df['test_id'] = stacked_df['test_id'].astype(int)
    stacked_df['is_duplicate'] /= nfolds
    stacked_df.to_csv(os.path.join(submission_path, 'final_stack/stacked_submission.csv'), encoding='utf-8', index=False)

def concat_predictions(dir):
    concat_submission = pd.DataFrame(columns=['test_id', 'is_duplicate'])
    
    files = sorted(glob.glob(os.path.join(dir, 'final_submission*')))
    for f in files:
        concat_submission = concat_submission.append(pd.read_csv(f, encoding='utf-8'))

    # final_submission.to_csv(os.path.join(submission_path, 'concat_submission.csv'), encoding='utf-8', index=False)
    # return 'concat_submission.csv'
    return concat_submission

def get_train_files():
    
    # return train_files_pred, os.path.join(submission_path, 'train_feat_manual.csv')
    train_preds = map(lambda x: os.path.join(submission_path, x), ['1D_conv_0.1650_train_pred.csv', 'predictions_XGB_leaky.train_run_0.csv'])
    #train_feat = os.path.join(submission_path, 'train_merge_feat.csv')
    print train_preds
    return train_preds

def get_test_files():
    
    test_preds = map(lambda x: os.path.join(submission_path, x), ['1D_conv_0.1650_test.csv', 'predictions_XGB_leaky.test_run_0.csv'])
    
    return test_preds

def main():
    # Generate some random predictions for a bunch of classifiers
    train_preds = get_train_files()
    test_preds  = get_test_files()
    print(train_preds)
    stacking(train_preds, test_preds, nfolds)
    blend_folds(nfolds)

if __name__ == '__main__':
    main()
