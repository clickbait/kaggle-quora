### CLICKBAIT - KAGGLE - QUORA QUESTION PAIRS

In this repository, our current solution to the Quora Question Pairs challenge is uploaded.

In order to run the pipeline, make a config file with your name in the config folder. 

## Classifiers currently implemented

XGBoost

LSTM (simple)

Lasso

Ridge

## Classifiers to be implemented in pipeline

deepnet.py

skipthought vectors


## CONFIG FILE OPTIONS

# Multiprocessing

-gpu_cores
-use_gpu
-gpu_id

# Data

-data_path
-train_file
-test_file
-model_path
-feature_path
-submission_path

# Crossvalidation

-runs
-splits (K)

# Preprocessing

-active
-train_file
-train_labels_file
-train_split
-test_file
-test_split
-stemmer

# Features
# Manual Features
# Word2Vec Google News
