; Config file for the Kaggle Quora Duplicate Questions challenge

[multiprocessing]
; cpu_cores (integer)
; The number of cores in your machine. Default: 4
cpu_cores = 8
; use_gpu (boolean)
; Should we use the gpu? Default: False
use_gpu = True
; gpu_id (integer)
; Which gpu to use? Default: 0
gpu_id = 0

[data]
; data_path (string)
; path to the main data directory
data_path = ../kaggle-quora-data/
; train_file (string)
; the filename of the original train file
train_file = train.csv
; test_file (string)
; the filename of the original test file
test_file = test.csv
; model_path (string)
; where in the data_path are the models stored?
model_path = models/
; feature_path (string)
; where in data_path to store the features?
feature_path = features/
; submission_path (string)
; where in data_path to store the submissions?
submission_path = results/

[cross-validation]
; runs (int)
; the number of times that cross-validation should be ran
runs = 1
; splits (int)
; the number of folds for pseudo K-folding
splits = 3

[preprocessing]
; active (boolean)
; Run the preprocessing? Turn off after the first time to speed up the process
active = False
; train_file (string)
; How to name the preprocessed train file? Is placed at the data_path
train_file = train_preprocessed
; train_labels_file (string)
; How to name the preprocessed train labels file? Is placed at the data_path
train_labels_file = train_labels
; train_split (integer)
; Split the train file in N files, is useful for memory intensive classification methods (<= 1: no split)
train_split = 1
; test_file (string)
; how to name the preprocessed test file? It is placed at the data_path
test_file = test_preprocessed
; test_split (integer)
; Split the test file in N files, is useful for memory intensive classification methods (<= 1: no split)
test_split = 5
; stemmer (string)
; Which stemmer to use? Options are: snowball, porter, lancaster
stemmer = porter

[feature_manual]
; active (boolean)
; Run this featue? Turn off after the first time to speed up the process
active = False
; train_feature_file (string)
; where to store the features extracted from the train data
train_feature_file = train_feat_manual
; test_feature_file (string)
; where to store the features extracted from the test data
test_feature_file = test_feat_manual

[feature_word2vec_google]
; active (boolean)
; Run this featue? Turn off after the first time to speed up the process
active = False
; train_feature_file (string)
; where to store the features extracted from the train data
train_feature_file = train_feat_word2vec_google
; test_feature_file (string)
; where to store the features extracted from the test data
test_feature_file = test_feat_word2vec_google
; model_file (string)
; location of the model within the model path
model_file = word2vec/GoogleNews-vectors-negative300.bin
; is_binary (boolean)
; Is the model saved in binary format?
is_binary = True

[feature_pos]
; active (boolean)
; Run this featue? Turn off after the first time to speed up the process
active = False
; train_feature_file (string)
; where to store the features extracted from the train data
train_feature_file = train_feat_pos
; test_feature_file (string)
; where to store the features extracted from the test data
test_feature_file = test_feat_pos

[feature_distance]
; active (boolean)
; Run this featue? Turn off after the first time to speed up the process
active = False
; train_feature_file (string)
; where to store the features extracted from the train data
train_feature_file = train_feat_distance
; test_feature_file (string)
; where to store the features extracted from the test data
test_feature_file = test_feat_distance
; model_file (string)
; location of the model within the model path
model_file = word2vec/GoogleNews-vectors-negative300.bin
; is_binary (boolean)
; Is the model saved in binary format?
is_binary = True

[feature_stolen_from_david]
; active (boolean)
; Run this featue? Turn off after the first time to speed up the process
active = False
; train_feature_file (string)
; where to store the features extracted from the train data
train_feature_file = train_feat_stolen_from_david
; test_feature_file (string)
; where to store the features extracted from the test data
test_feature_file = test_feat_stolen_from_david

[feature_magic]
; active (boolean)
; Run this featue? Turn off after the first time to speed up the process
active = False
; train_feature_file (string)
; where to store the features extracted from the train data
train_feature_file = train_magic
; test_feature_file (string)
; where to store the features extracted from the test data
test_feature_file = test_magic

[feature_merge]
; active (boolean)
; Run this featue? Turn off after the first time to speed up the process
active = False
; train_feature_file (string)
; where to store the features extracted from the train data
train_feature_file = train_merge
; test_feature_file (string)
; where to store the features extracted from the test data
test_feature_file = test_merge

[merge]
; active (boolean)
; Run merge? Turn off after the first time to speed up the process
active = False

[classifier_xgboost_simple]
; active (boolean)
; Run this classifier? Turn off to exclude
active = True
; model_file (string)
; where to store the trained classifier model. Is placed at the models path.
model_file = xgboost/xgboost_simple.bin
; submission_file (string)
; where to store the submission file generated by this classifier? Is placed at the submissions path
submission_file = xgboost_simple.csv
; xgboosts params
num_boost_round = 300
objective = binary:logistic
eval_metric = logloss
eta = 0.03
max_depth = 500
silent = 0
verbose = 1
subsample = 0.8
n_estimators = 2500
# scale_pos_weight = ?
# max_delta_step = ?

[classifier_lstm_simple]
; active (boolean)
; Run this classifier? Turn off to exclude
active = False
; model_file (string)
; where to store the trained classifier model. Is placed at the models path.
model_file = lstm/lstm_simple.bin
; submission_file (string)
; where to store the submission file generated by this classifier? Is placed at the submissions path
submission_file = lstm_simple.csv

[classifier_ridge]
; active (boolean)
; Run this classifier? Turn off to exclude
active = True
; model_file (string)
; where to store the trained classifier model. Is placed at the models path.
model_file = ridge/ridge.bin
; submission_file (string)
; where to store the submission file generated by this classifier? Is placed at the submissions path
submission_file = ridge.csv
; alpha (float)
; Ridge alpha
alpha = 1.0

[classifier_lasso]
; active (boolean)
; Run this classifier? Turn off to exclude
active = False
; model_file (string)
; where to store the trained classifier model. Is placed at the models path.
model_file = lasso/lasso.bin
; submission_file (string)
; where to store the submission file generated by this classifier? Is placed at the submissions path
submission_file = lasso.csv
; alpha (float)
; Lasso alpha
alpha = 1.0

[classifier_svm]
; active (boolean)
; Run this classifier? Turn off to exclude
active = False
; model_file (string)
; where to store the trained classifier model. Is placed at the models path.
model_file = svm/svm.bin
; submission_file (string)
; where to store the submission file generated by this classifier? Is placed at the submissions path
submission_file = svm_linear.csv
; kernel (string)
; SVM kernel
kernel = linear