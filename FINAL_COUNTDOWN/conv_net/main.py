import os
import glob
import logging
import numpy as np

import features.featurizer as featurizer
import preprocessing.magic as santa
import preprocessing.data_loader as dfloader
import preprocessing.preprocess as dfpreprocess
import models.deep_nlp as NLP_model

from ConfigParser import SafeConfigParser


# Setup the logger
logging.basicConfig(level=logging.INFO)

def main(config):
    # Set the seed
    np.random.seed(config.getint('general', 'seed'))

    # Apply the magic features
    if config.getboolean('main', 'magic'):
        logging.info('Giving the dataset their magic features')
        santa.inject_magic(config)

    # Load the magic csv and split the test set
    if config.getboolean('main', 'load'):
        logging.info('Loading data')
        dfloader.load_train(config)
        dfloader.load_test(config)

    # Do we want to preprocess the data
    if config.getboolean('main', 'preprocess'):
        logging.info('preprocessing data')
        for file in sorted(glob.glob(config.get('preprocess', 'ld_dir') + "*.csv")):
            logging.info('preprocessing ' + os.path.basename(file))
            dfpreprocess.preprocess_questions(config, os.path.basename(file))

    # Do we want to add features
    if config.getboolean('main', 'feature'):
        logging.info('Obtaining features')
        for file in sorted(glob.glob(config.get('feature', 'ld_dir') + '*.csv')):
            logging.info('featurize ' + os.path.basename(file))
            featurizer.extract(config, os.path.basename(file))

        logging.info('rescaling the features for the network')
        featurizer.rescale_feat(config)

    # Train model
    if config.getboolean('main', 'train'):
        logging.info('running model')
        NLP_model.train_model(config)

    # Make predictions
    if config.getboolean('main', 'predict'):
        logging.info('predicting on test set')
        NLP_model.predict_model(config)

    return 1

if __name__ == '__main__':
    # Directory/File location settings
    config = SafeConfigParser()
    config.read('config.ini')

    main(config)