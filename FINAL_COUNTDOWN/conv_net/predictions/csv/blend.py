import pandas as pd
import numpy as np
import glob


blend_avg = np.zeros((2345796, 1))
blend_geo = np.ones((2345796, 1))
num = 0
for file in sorted(glob.glob('*.csv')):
    num += 1
    temp_df = pd.read_csv(file)
    blend_avg += temp_df[['is_duplicate']].values
    blend_geo *= temp_df[['is_duplicate']].values
blend_avg/float(num)

temp_df['is_duplicate'] = blend_avg/float(num)
temp_df.to_csv('../blend_avg.csv', index=False)

print np.power(blend_geo, 1/float(num))

temp_df['is_duplicate'] = np.power(blend_geo, 1/float(num))
temp_df.to_csv('../blend_geo.csv', index=False)