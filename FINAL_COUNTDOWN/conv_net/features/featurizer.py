import glob
import gensim
import logging
import wrapper_distance

import pandas as pd
import numpy as np


from sklearn.preprocessing import StandardScaler
from scipy.stats import skew, kurtosis
from multiprocessing import Pool
from nltk import word_tokenize
from functools import partial


# Setup logger
logging.getLogger('gensim').setLevel(logging.ERROR)
logger = logging.getLogger(__name__)


def wmd(model, zipped):
    q1, q2 = zipped
    q1_tokens = word_tokenize(q1)
    q2_tokens = word_tokenize(q2)
    return model.wmdistance(q1_tokens, q2_tokens)


def sent2vec(q, model, dict):
    q_tokens = word_tokenize(q)
    M = np.zeros((len(q_tokens), 300))
    for i, token in enumerate(q_tokens):
        try:
            M[i, :] = model[token]
        except:
            if not token in dict:
                dict[token] = np.random.rand(300)
            M[i, :] = dict[token]

    v = M.sum(axis=0)
    if len(q_tokens) > 0:
        return v / np.sqrt((v ** 2).sum()), dict
    else:
        return np.zeros((300)), dict


def extract(config, _name):
    # Setup pooling
    pool = Pool(processes=config.getint('general', 'cpu_cores'))

    # Load dir
    df = pd.read_csv(config.get('feature', 'ld_dir') + _name, encoding='utf-8').fillna('')


    # Load the embedding
    model = gensim.models.KeyedVectors.load_word2vec_format(config.get('feature', 'embedding'), binary=config.getboolean('feature', 'emb_binary'))

    # Doing sent2vec
    logger.info("Sent2vec")
    dict = {}
    question1_vectors = np.zeros((df.shape[0], 300))
    question2_vectors = np.zeros((df.shape[0], 300))

    for i, q in enumerate(df.question1.values):
        question1_vectors[i], dict = sent2vec(q, model, dict)

    for i, q in enumerate(df.question2.values):
        question2_vectors[i], dict = sent2vec(q, model, dict)

    # Delete the model, to make some space
    model = []


    logger.info('Zipping the questionvectors together')
    zipped_vectors = zip(question1_vectors, question2_vectors)

    logger.info("Braycurtis distance")
    df['braycurtis_distance'] = pool.map(wrapper_distance.braycurtis_wrapper, zipped_vectors)

    logger.info("Canberra distance")
    df['canberra_distance'] = pool.map(wrapper_distance.canberra_wrapper, zipped_vectors)

    logger.info("Chebyshev distance")
    df['chebyshev_distance'] = pool.map(wrapper_distance.chebyshev_wrapper, zipped_vectors)

    logger.info("Cityblock distance")
    df['cityblock_distance'] = pool.map(wrapper_distance.cityblock_wrapper, zipped_vectors)

    logger.info("Correlation distance")
    df['correlation_distance'] = pool.map(wrapper_distance.correlation_wrapper, zipped_vectors)

    logger.info("Cosine distance")
    df['cosine_distance'] = pool.map(wrapper_distance.cosine_wrapper, zipped_vectors)

    logger.info("Dice distance")
    df['dice_distance'] = pool.map(wrapper_distance.dice_wrapper, zipped_vectors)

    logger.info("Euclidean distance")
    df['euclidean_distance'] = pool.map(wrapper_distance.euclidean_wrapper, zipped_vectors)

    logger.info("Kulsinski distance")
    df['kulsinski_distance'] = pool.map(wrapper_distance.kulsinski_wrapper, zipped_vectors)

    logger.info("Minkowski distance")
    df['minkowski_distance'] = pool.map(wrapper_distance.minkowski_wrapper, zipped_vectors)

    logger.info("Rogerstanimoto distance")
    df['rogerstanimoto_distance'] = pool.map(wrapper_distance.rogerstanimoto_wrapper, zipped_vectors)

    logger.info("Russellrao distance")
    df['russellrao_distance'] = pool.map(wrapper_distance.russellrao_wrapper, zipped_vectors)

    logger.info("Sokalmichener distance")
    df['sokalmichener_distance'] = pool.map(wrapper_distance.sokalmichener_wrapper, zipped_vectors)

    logger.info("Sokalsneath distance")
    df['sokalsneath_distance'] = pool.map(wrapper_distance.sokalsneath_wrapper, zipped_vectors)

    logger.info("Yule distance")
    df['yule_distance'] = pool.map(wrapper_distance.yule_wrapper, zipped_vectors)

    logger.info("Skew")
    df['skew_q1vec'] = pool.map(skew, question1_vectors)
    df['skew_q2vec'] = pool.map(skew, question2_vectors)

    logger.info("Kurtosis")
    df['kur_q1vec'] = pool.map(kurtosis, question1_vectors)
    df['kur_q2vec'] = pool.map(kurtosis, question2_vectors)

    pool.close()

    # Save it
    df.to_csv(config.get('feature', 'sv_dir') + _name, encoding='utf-8', index=False)


def rescale_feat(config):

    store_df = pd.DataFrame(columns=[
        'q1_q2_intersect',
        'q1_freq',
        'q2_freq',
        'tfidf_wm_stops',
        'tfidf_wm',
        'char_tfidf_l2_cosine',
        'char_tfidf_l2_l1',
        'word_tfidf_l2_cosine',
        'word_tfidf_l2_l1',
        'qid1_max_kcore',
        'qid2_max_kcore',
        'len_q1',
        'len_q2',
        'common_words',
        'fuzz_qratio',
        'fuzz_partial_ratio',
        'fuzz_token_set_ratio',
        'fuzz_token_sort_ratio',
        'fuzz_partial_token_sort_ratio',
        'wmd',
        'norm_wmd',
        'braycurtis_distance',
        'canberra_distance',
        'chebyshev_distance',
        'cityblock_distance',
        'correlation_distance',
        'cosine_distance',
        'dice_distance',
        'euclidean_distance',
        'kulsinski_distance',
        'minkowski_distance',
        'rogerstanimoto_distance',
        'russellrao_distance',
        'sokalmichener_distance',
        'sokalsneath_distance',
        'yule_distance',
        'skew_q1vec',
        'skew_q2vec',
        'kur_q1vec',
        'kur_q2vec'
        ])

    for file in sorted(glob.glob(config.get('feature', 'sv_dir') + "*.csv")):
        temp_df = pd.read_csv(file, encoding='utf-8').fillna('')
        store_df = store_df.append(temp_df[[
            'q1_q2_intersect',
            'q1_freq',
            'q2_freq',
            'tfidf_wm_stops',
            'tfidf_wm',
            'char_tfidf_l2_cosine',
            'char_tfidf_l2_l1',
            'word_tfidf_l2_cosine',
            'word_tfidf_l2_l1',
            'qid1_max_kcore',
            'qid2_max_kcore',
            'len_q1',
            'len_q2',
            'common_words',
            'fuzz_qratio',
            'fuzz_partial_ratio',
            'fuzz_token_set_ratio',
            'fuzz_token_sort_ratio',
            'fuzz_partial_token_sort_ratio',
            'wmd',
            'norm_wmd',
            'braycurtis_distance',
            'canberra_distance',
            'chebyshev_distance',
            'cityblock_distance',
            'correlation_distance',
            'cosine_distance',
            'dice_distance',
            'euclidean_distance',
            'kulsinski_distance',
            'minkowski_distance',
            'rogerstanimoto_distance',
            'russellrao_distance',
            'sokalmichener_distance',
            'sokalsneath_distance',
            'yule_distance',
            'skew_q1vec',
            'skew_q2vec',
            'kur_q1vec',
            'kur_q2vec'
        ]], ignore_index=True)

    store_df['wmd'] = store_df['wmd'].replace(np.inf, 5)
    store_df['norm_wmd'] = store_df['norm_wmd'].replace(np.inf, 2)

    ss = StandardScaler()
    ss.fit(np.array(store_df))


    for file in sorted(glob.glob(config.get('feature', 'sv_dir') + '*.csv')):
        temp_df = pd.read_csv(file, encoding='utf-8').fillna('')
        temp_df['wmd'] = temp_df['wmd'].replace(np.inf, 5)
        temp_df['norm_wmd'] = temp_df['norm_wmd'].replace(np.inf, 2)

        temp_transform = ss.transform(np.array(temp_df[[
            'q1_q2_intersect',
            'q1_freq',
            'q2_freq',
            'tfidf_wm_stops',
            'tfidf_wm',
            'char_tfidf_l2_cosine',
            'char_tfidf_l2_l1',
            'word_tfidf_l2_cosine',
            'word_tfidf_l2_l1',
            'qid1_max_kcore',
            'qid2_max_kcore',
            'len_q1',
            'len_q2',
            'common_words',
            'fuzz_qratio',
            'fuzz_partial_ratio',
            'fuzz_token_set_ratio',
            'fuzz_token_sort_ratio',
            'fuzz_partial_token_sort_ratio',
            'wmd',
            'norm_wmd',
            'braycurtis_distance',
            'canberra_distance',
            'chebyshev_distance',
            'cityblock_distance',
            'correlation_distance',
            'cosine_distance',
            'dice_distance',
            'euclidean_distance',
            'kulsinski_distance',
            'minkowski_distance',
            'rogerstanimoto_distance',
            'russellrao_distance',
            'sokalmichener_distance',
            'sokalsneath_distance',
            'yule_distance',
            'skew_q1vec',
            'skew_q2vec',
            'kur_q1vec',
            'kur_q2vec'
        ]]))

        temp_df[[
            'q1_q2_intersect',
            'q1_freq',
            'q2_freq',
            'tfidf_wm_stops',
            'tfidf_wm',
            'char_tfidf_l2_cosine',
            'char_tfidf_l2_l1',
            'word_tfidf_l2_cosine',
            'word_tfidf_l2_l1',
            'qid1_max_kcore',
            'qid2_max_kcore',
            'len_q1',
            'len_q2',
            'common_words',
            'fuzz_qratio',
            'fuzz_partial_ratio',
            'fuzz_token_set_ratio',
            'fuzz_token_sort_ratio',
            'fuzz_partial_token_sort_ratio',
            'wmd',
            'norm_wmd',
            'braycurtis_distance',
            'canberra_distance',
            'chebyshev_distance',
            'cityblock_distance',
            'correlation_distance',
            'cosine_distance',
            'dice_distance',
            'euclidean_distance',
            'kulsinski_distance',
            'minkowski_distance',
            'rogerstanimoto_distance',
            'russellrao_distance',
            'sokalmichener_distance',
            'sokalsneath_distance',
            'yule_distance',
            'skew_q1vec',
            'skew_q2vec',
            'kur_q1vec',
            'kur_q2vec'
        ]] = temp_transform

        temp_df.to_csv(file, encoding='utf-8', index=False)