from keras import backend as K
from keras.models import Model, load_model
from keras.layers.merge import concatenate, multiply, dot
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM
from keras.layers import Dense, Dropout, Input, BatchNormalization, Conv1D, Lambda, GlobalAveragePooling1D


def model_lstm_(emb_matrix):

    emb_layer = Embedding(
        input_dim=emb_matrix.shape[0],
        output_dim=emb_matrix.shape[1],
        weights=[emb_matrix],
        input_length=60,
        trainable=False
    )

    lstm_layer = LSTM(
        units=200,
        dropout=0.2,
        recurrent_dropout=0.2
    )

    # Define inputs
    seq1 = Input(shape=(60,))
    seq2 = Input(shape=(60,))

    # Run inputs through embedding
    emb1 = emb_layer(seq1)
    emb2 = emb_layer(seq2)

    lstm1 = lstm_layer(emb1)
    lstm2 = lstm_layer(emb2)

    # Add the magic features
    magic_input = Input(shape=(3,))
    magic_dense = Dense(64, activation='relu')(magic_input)

    # Merge the Magic features with the lstm layers
    merge = concatenate([lstm1, lstm2, magic_dense])

    # The MLP that determines the outcome
    x = Dropout(0.2)(merge)
    x = BatchNormalization()(x)
    x = Dense(300, activation='relu')(x)

    x = Dropout(0.2)(x)
    x = BatchNormalization()(x)
    x = Dense(300, activation='relu')(x)

    x = Dropout(0.2)(x)
    x = BatchNormalization()(x)
    pred = Dense(1, activation='sigmoid')(x)

    model = Model(inputs=[seq1, seq2, magic_input], outputs=pred)
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['acc'])

    return model


def model_conv1D_(emb_matrix):

    emb_layer = Embedding(
        input_dim=emb_matrix.shape[0],
        output_dim=emb_matrix.shape[1],
        weights=[emb_matrix],
        input_length=60,
        trainable=False
    )

    conv1 = Conv1D(filters=128, kernel_size=1, padding='same', activation='relu')
    conv2 = Conv1D(filters=128, kernel_size=2, padding='same', activation='relu')
    conv3 = Conv1D(filters=128, kernel_size=3, dilation_rate=1, padding='same', activation='relu')
    conv4 = Conv1D(filters=128, kernel_size=4, dilation_rate=1, padding='same', activation='relu')
    conv5 = Conv1D(filters=128, kernel_size=5, dilation_rate=1, padding='same', activation='relu')
    conv6 = Conv1D(filters=128, kernel_size=6, dilation_rate=1, padding='same', activation='relu')
    conv10 = Conv1D(filters=128, kernel_size=10, dilation_rate=1, padding='same', activation='relu')
    conv16 = Conv1D(filters=128, kernel_size=16, dilation_rate=1, padding='same', activation='relu')


    # Define inputs
    seq1 = Input(shape=(60,))
    seq2 = Input(shape=(60,))

    # Run inputs through embedding
    emb1 = emb_layer(seq1)
    emb2 = emb_layer(seq2)

    # Run through CONV + GAP layers
    conv1a = conv1(emb1)
    glob1a = GlobalAveragePooling1D()(conv1a)
    conv1b = conv1(emb2)
    glob1b = GlobalAveragePooling1D()(conv1b)

    conv2a = conv2(emb1)
    glob2a = GlobalAveragePooling1D()(conv2a)
    conv2b = conv2(emb2)
    glob2b = GlobalAveragePooling1D()(conv2b)

    conv3a = conv3(emb1)
    glob3a = GlobalAveragePooling1D()(conv3a)
    conv3b = conv3(emb2)
    glob3b = GlobalAveragePooling1D()(conv3b)

    conv4a = conv4(emb1)
    glob4a = GlobalAveragePooling1D()(conv4a)
    conv4b = conv4(emb2)
    glob4b = GlobalAveragePooling1D()(conv4b)

    conv5a = conv5(emb1)
    glob5a = GlobalAveragePooling1D()(conv5a)
    conv5b = conv5(emb2)
    glob5b = GlobalAveragePooling1D()(conv5b)

    conv6a = conv6(emb1)
    glob6a = GlobalAveragePooling1D()(conv6a)
    conv6b = conv6(emb2)
    glob6b = GlobalAveragePooling1D()(conv6b)

    conv10a = conv10(emb1)
    glob10a = GlobalAveragePooling1D()(conv10a)
    conv10b = conv10(emb2)
    glob10b = GlobalAveragePooling1D()(conv10b)

    conv16a = conv16(emb1)
    glob16a = GlobalAveragePooling1D()(conv16a)
    conv16b = conv16(emb2)
    glob16b = GlobalAveragePooling1D()(conv16b)

    mergea = concatenate([glob1a, glob2a, glob3a, glob4a, glob5a, glob6a, glob10a, glob16a])
    mergeb = concatenate([glob1b, glob2b, glob3b, glob4b, glob5b, glob6b, glob10b, glob16b])

    # For now we take the explicit absolute difference between the two sentences
    diff = Lambda(lambda x: K.abs(x[0] - x[1]), output_shape=(8*128, ))([mergea, mergeb])

    # Add the magic features
    magic_input = Input(shape=(5,))
    magic_dense = BatchNormalization()(magic_input)
    magic_dense = Dense(64, activation='relu')(magic_dense)


    # Add the distance features
    distance_input = Input(shape=(20,))
    distance_dense = BatchNormalization()(distance_input)
    distance_dense = Dense(128, activation='relu')(distance_dense)

    # Merge the Magic and distance features with the difference layer
    merge = concatenate([diff, magic_dense, distance_dense])

    # The MLP that determines the outcome
    x = Dropout(0.2)(merge)
    x = BatchNormalization()(x)
    x = Dense(300, activation='relu')(x)

    # x = Dropout(0.2)(x)
    # x = BatchNormalization()(x)
    # x = Dense(300, activation='relu')(x)

    x = Dropout(0.2)(x)
    x = BatchNormalization()(x)
    pred = Dense(1, activation='sigmoid')(x)

    model = Model(inputs=[seq1, seq2, magic_input, distance_input], outputs=pred)
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['acc'])

    return model


def model_conv1D_lstm(emb_matrix):

    emb_layer = Embedding(
        input_dim=emb_matrix.shape[0],
        output_dim=emb_matrix.shape[1],
        weights=[emb_matrix],
        input_length=60,
        trainable=False
    )

    lstm_layer = LSTM(
        units=200,
        dropout=0.2,
        recurrent_dropout=0.2
    )

    conv1 = Conv1D(filters=128, kernel_size=1, padding='same', activation='relu')
    conv2 = Conv1D(filters=128, kernel_size=2, padding='same', activation='relu')
    conv3 = Conv1D(filters=128, kernel_size=3, dilation_rate=1, padding='same', activation='relu')
    conv4 = Conv1D(filters=128, kernel_size=4, dilation_rate=1, padding='same', activation='relu')
    conv5 = Conv1D(filters=128, kernel_size=5, dilation_rate=1, padding='same', activation='relu')
    conv6 = Conv1D(filters=128, kernel_size=6, dilation_rate=1, padding='same', activation='relu')
    conv10 = Conv1D(filters=128, kernel_size=10, dilation_rate=1, padding='same', activation='relu')
    conv16 = Conv1D(filters=128, kernel_size=16, dilation_rate=1, padding='same', activation='relu')

    # Define inputs
    seq1 = Input(shape=(60,))
    seq2 = Input(shape=(60,))

    # Run inputs through embedding
    emb1 = emb_layer(seq1)
    emb2 = emb_layer(seq2)

    # Run through CONV + GAP layers
    conv1a = conv1(emb1)
    glob1a = GlobalAveragePooling1D()(conv1a)
    conv1b = conv1(emb2)
    glob1b = GlobalAveragePooling1D()(conv1b)

    conv2a = conv2(emb1)
    glob2a = GlobalAveragePooling1D()(conv2a)
    conv2b = conv2(emb2)
    glob2b = GlobalAveragePooling1D()(conv2b)

    conv3a = conv3(emb1)
    glob3a = GlobalAveragePooling1D()(conv3a)
    conv3b = conv3(emb2)
    glob3b = GlobalAveragePooling1D()(conv3b)

    conv4a = conv4(emb1)
    glob4a = GlobalAveragePooling1D()(conv4a)
    conv4b = conv4(emb2)
    glob4b = GlobalAveragePooling1D()(conv4b)

    conv5a = conv5(emb1)
    glob5a = GlobalAveragePooling1D()(conv5a)
    conv5b = conv5(emb2)
    glob5b = GlobalAveragePooling1D()(conv5b)

    conv6a = conv6(emb1)
    glob6a = GlobalAveragePooling1D()(conv6a)
    conv6b = conv6(emb2)
    glob6b = GlobalAveragePooling1D()(conv6b)

    conv10a = conv10(emb1)
    glob10a = GlobalAveragePooling1D()(conv10a)
    conv10b = conv10(emb2)
    glob10b = GlobalAveragePooling1D()(conv10b)

    conv16a = conv16(emb1)
    glob16a = GlobalAveragePooling1D()(conv16a)
    conv16b = conv16(emb2)
    glob16b = GlobalAveragePooling1D()(conv16b)

    mergea = concatenate([glob1a, glob2a, glob3a, glob4a, glob5a, glob6a, glob10a, glob16a])
    mergeb = concatenate([glob1b, glob2b, glob3b, glob4b, glob5b, glob6b, glob10b, glob16b])

    # For now we take the explicit absolute difference between the two sentences
    diff = Lambda(lambda x: K.abs(x[0] - x[1]), output_shape=(8*128, ))([mergea, mergeb])

    # Use the LSTM
    lstma = lstm_layer(emb1)
    lstmb = lstm_layer(emb2)

    merge_lstm = concatenate([lstma, lstmb])

    # Add the magic features
    magic_input = Input(shape=(3,))
    magic_dense = Dense(64, activation='relu')(magic_input)

    distance_input = Input(shape=(24,))
    distance_dense = Dense(128, activation='relu')(distance_input)

    # Merge the Magic features with the difference layer
    merge = concatenate([diff, magic_dense, distance_dense, merge_lstm])

    # The MLP that determines the outcome
    x = Dropout(0.2)(merge)
    x = BatchNormalization()(x)
    x = Dense(300, activation='relu')(x)

    x = Dropout(0.2)(x)
    x = BatchNormalization()(x)
    x = Dense(300, activation='relu')(x)

    x = Dropout(0.2)(x)
    x = BatchNormalization()(x)
    pred = Dense(1, activation='sigmoid')(x)

    model = Model(inputs=[seq1, seq2, magic_input, distance_input], outputs=pred)
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['acc'])

    return model


def model_conv1D_simple(emb_matrix):
    emb_layer = Embedding(
        input_dim=emb_matrix.shape[0],
        output_dim=emb_matrix.shape[1],
        weights=[emb_matrix],
        input_length=60,
        trainable=False
    )

    conv1 = Conv1D(filters=128, kernel_size=1, padding='same', activation='relu')
    conv2 = Conv1D(filters=128, kernel_size=2, padding='same', activation='relu')
    conv3 = Conv1D(filters=128, kernel_size=3, dilation_rate=1, padding='same', activation='relu')
    conv4 = Conv1D(filters=128, kernel_size=4, dilation_rate=1, padding='same', activation='relu')

    # Define inputs
    seq1 = Input(shape=(60,))
    seq2 = Input(shape=(60,))

    # Run inputs through embedding
    emb1 = emb_layer(seq1)
    emb2 = emb_layer(seq2)

    # Run through CONV + GAP layers
    conv1a = conv1(emb1)
    glob1a = GlobalAveragePooling1D()(conv1a)
    conv1b = conv1(emb2)
    glob1b = GlobalAveragePooling1D()(conv1b)

    conv2a = conv2(emb1)
    glob2a = GlobalAveragePooling1D()(conv2a)
    conv2b = conv2(emb2)
    glob2b = GlobalAveragePooling1D()(conv2b)

    conv3a = conv3(emb1)
    glob3a = GlobalAveragePooling1D()(conv3a)
    conv3b = conv3(emb2)
    glob3b = GlobalAveragePooling1D()(conv3b)

    conv4a = conv4(emb1)
    glob4a = GlobalAveragePooling1D()(conv4a)
    conv4b = conv4(emb2)
    glob4b = GlobalAveragePooling1D()(conv4b)

    mergea = concatenate([glob1a, glob2a, glob3a, glob4a])
    mergeb = concatenate([glob1b, glob2b, glob3b, glob4b])

    # For now we take the explicit absolute difference between the two sentences
    diff = Lambda(lambda x: K.abs(x[0] - x[1]), output_shape=(4 * 128,))([mergea, mergeb])

    # Add the magic features
    magic_input = Input(shape=(5,))
    magic_dense = BatchNormalization()(magic_input)
    magic_dense = Dense(64, activation='relu')(magic_dense)

    # Add the distance features
    distance_input = Input(shape=(20,))
    distance_dense = BatchNormalization()(distance_input)
    distance_dense = Dense(128, activation='relu')(distance_dense)

    # Merge the Magic and distance features with the difference layer
    merge = concatenate([diff, magic_dense, distance_dense])


    # The MLP that determines the outcome
    x = Dropout(0.2)(merge)
    x = BatchNormalization()(x)
    x = Dense(300, activation='relu')(x)

    x = Dropout(0.2)(x)
    x = BatchNormalization()(x)
    pred = Dense(1, activation='sigmoid')(x)

    # model = Model(inputs=[seq1, seq2, magic_input, distance_input], outputs=pred)
    model = Model(inputs=[seq1, seq2, magic_input, distance_input], outputs=pred)
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['acc'])

    return model


def model_lstm_simple(emb_matrix):

    emb_layer = Embedding(
        input_dim=emb_matrix.shape[0],
        output_dim=emb_matrix.shape[1],
        weights=[emb_matrix],
        input_length=60,
        trainable=False
    )

    lstm_layer = LSTM(
        units=200,
        dropout=0.2,
        recurrent_dropout=0.2
    )

    # Define inputs
    seq1 = Input(shape=(60,))
    seq2 = Input(shape=(60,))

    # Run inputs through embedding
    emb1 = emb_layer(seq1)
    emb2 = emb_layer(seq2)

    lstm1 = lstm_layer(emb1)
    lstm2 = lstm_layer(emb2)

    diff = multiply([lstm1, lstm2])

    # Add the magic features
    magic_input = Input(shape=(5,))
    magic_dense = BatchNormalization()(magic_input)
    magic_dense = Dense(64, activation='relu')(magic_dense)

    # Add the distance features
    distance_input = Input(shape=(20,))
    distance_dense = BatchNormalization()(distance_input)
    distance_dense = Dense(128, activation='relu')(distance_dense)

    # Merge the Magic and distance features with the difference layer
    merge = concatenate([diff, magic_dense, distance_dense])

    # The MLP that determines the outcome
    x = Dropout(0.2)(merge)
    x = BatchNormalization()(x)
    x = Dense(300, activation='relu')(x)

    x = Dropout(0.2)(x)
    x = BatchNormalization()(x)
    pred = Dense(1, activation='sigmoid')(x)

    model = Model(inputs=[seq1, seq2, magic_input, distance_input], outputs=pred)
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['acc'])

    return model

