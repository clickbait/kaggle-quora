import glob
import pandas as pd
import numpy as np

from unidecode import unidecode

from keras import backend as K
from keras.models import Model, load_model
from keras.layers.merge import concatenate
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM
from keras.layers import Dense, Dropout, Input, BatchNormalization, Conv1D, Lambda, GlobalAveragePooling1D
from keras.preprocessing import sequence, text
from keras.callbacks import ModelCheckpoint

from ConfigParser import SafeConfigParser


def model_conv_(emb_matrix):

    emb_layer = Embedding(
        input_dim=emb_matrix.shape[0],
        output_dim=emb_matrix.shape[1],
        weights=[emb_matrix],
        input_length=500,
        trainable=True
    )


    conv1 = Conv1D(filters=128, kernel_size=1, padding='same', activation='relu')
    conv2 = Conv1D(filters=128, kernel_size=2, padding='same', activation='relu')
    conv3 = Conv1D(filters=128, kernel_size=3, dilation_rate=1, padding='same', activation='relu')
    conv4 = Conv1D(filters=128, kernel_size=4, dilation_rate=1, padding='same', activation='relu')
    conv5 = Conv1D(filters=128, kernel_size=5, dilation_rate=1, padding='same', activation='relu')
    conv6 = Conv1D(filters=128, kernel_size=6, dilation_rate=1, padding='same', activation='relu')
    conv7 = Conv1D(filters=128, kernel_size=7, dilation_rate=1, padding='same', activation='relu')
    conv8 = Conv1D(filters=128, kernel_size=8, dilation_rate=1, padding='same', activation='relu')
    conv9 = Conv1D(filters=128, kernel_size=9, dilation_rate=1, padding='same', activation='relu')
    conv10 = Conv1D(filters=128, kernel_size=10, dilation_rate=1, padding='same', activation='relu')
    conv14 = Conv1D(filters=128, kernel_size=14, dilation_rate=1, padding='same', activation='relu')
    conv18 = Conv1D(filters=128, kernel_size=18, dilation_rate=1, padding='same', activation='relu')
    conv24 = Conv1D(filters=128, kernel_size=24, dilation_rate=1, padding='same', activation='relu')
    conv30 = Conv1D(filters=128, kernel_size=30, dilation_rate=1, padding='same', activation='relu')

    # Define inputs
    seq1 = Input(shape=(500,))
    seq2 = Input(shape=(500,))

    # Run inputs through embedding
    emb1 = emb_layer(seq1)
    emb2 = emb_layer(seq2)

    # Run through CONV + GAP layers
    conv1a = conv1(emb1)
    glob1a = GlobalAveragePooling1D()(conv1a)
    conv1b = conv1(emb2)
    glob1b = GlobalAveragePooling1D()(conv1b)

    conv2a = conv2(emb1)
    glob2a = GlobalAveragePooling1D()(conv2a)
    conv2b = conv2(emb2)
    glob2b = GlobalAveragePooling1D()(conv2b)

    conv3a = conv3(emb1)
    glob3a = GlobalAveragePooling1D()(conv3a)
    conv3b = conv3(emb2)
    glob3b = GlobalAveragePooling1D()(conv3b)

    conv4a = conv4(emb1)
    glob4a = GlobalAveragePooling1D()(conv4a)
    conv4b = conv4(emb2)
    glob4b = GlobalAveragePooling1D()(conv4b)

    conv5a = conv5(emb1)
    glob5a = GlobalAveragePooling1D()(conv5a)
    conv5b = conv5(emb2)
    glob5b = GlobalAveragePooling1D()(conv5b)

    conv6a = conv6(emb1)
    glob6a = GlobalAveragePooling1D()(conv6a)
    conv6b = conv6(emb2)
    glob6b = GlobalAveragePooling1D()(conv6b)

    conv7a = conv7(emb1)
    glob7a = GlobalAveragePooling1D()(conv7a)
    conv7b = conv7(emb2)
    glob7b = GlobalAveragePooling1D()(conv7b)

    conv8a = conv8(emb1)
    glob8a = GlobalAveragePooling1D()(conv8a)
    conv8b = conv8(emb2)
    glob8b = GlobalAveragePooling1D()(conv8b)

    conv9a = conv9(emb1)
    glob9a = GlobalAveragePooling1D()(conv9a)
    conv9b = conv9(emb2)
    glob9b = GlobalAveragePooling1D()(conv9b)

    conv10a = conv10(emb1)
    glob10a = GlobalAveragePooling1D()(conv10a)
    conv10b = conv10(emb2)
    glob10b = GlobalAveragePooling1D()(conv10b)

    conv14a = conv14(emb1)
    glob14a = GlobalAveragePooling1D()(conv14a)
    conv14b = conv14(emb2)
    glob14b = GlobalAveragePooling1D()(conv14b)

    conv18a = conv18(emb1)
    glob18a = GlobalAveragePooling1D()(conv18a)
    conv18b = conv18(emb2)
    glob18b = GlobalAveragePooling1D()(conv18b)

    conv24a = conv24(emb1)
    glob24a = GlobalAveragePooling1D()(conv24a)
    conv24b = conv24(emb2)
    glob24b = GlobalAveragePooling1D()(conv24b)

    conv30a = conv30(emb1)
    glob30a = GlobalAveragePooling1D()(conv30a)
    conv30b = conv30(emb2)
    glob30b = GlobalAveragePooling1D()(conv30b)

    mergea = concatenate([glob1a, glob2a, glob3a, glob4a, glob5a, glob6a, glob7a, glob8a, glob9a, glob10a, glob14a, glob18a, glob24a, glob30a])
    mergeb = concatenate([glob1b, glob2b, glob3b, glob4b, glob5b, glob6b, glob7b, glob8b, glob9b, glob10b, glob14b, glob18b, glob24b, glob30b])

    # For now we take the explicit absolute difference between the two sentences
    diff = Lambda(lambda x: K.abs(x[0] - x[1]), output_shape=(14 * 128,))([mergea, mergeb])

    # Add the magic features
    magic_input = Input(shape=(3,))
    magic_dense = Dense(64, activation='relu')(magic_input)

    # Merge the Magic features with the difference layer
    merge = concatenate([diff, magic_dense])

    # The MLP that determines the outcome
    x = Dropout(0.2)(merge)
    x = BatchNormalization()(x)
    x = Dense(300, activation='relu')(x)

    x = Dropout(0.2)(x)
    x = BatchNormalization()(x)
    pred = Dense(1, activation='sigmoid')(x)

    model = Model(inputs=[seq1, seq2, magic_input], outputs=pred)
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['acc'])

    return model


def train_model(config):
    # Load the training data
    df = pd.read_csv("../" + config.get('preprocess', 'sv_dir') + 'train.csv', encoding='utf-8')

    # initialize the text tokenizer from keras (only using it since it allows for easy integration with the embedding)
    tk = text.Tokenizer(num_words=config.getint('train', 'max_nb_words'), char_level=True)

    # the tokenizer gets vocabulary
    for file in sorted(glob.glob("../" + config.get('predict', 'ld_dir') + '*.csv')):
        # Obtain dataframe
        df = pd.read_csv(file, encoding='utf-8').fillna('')

        # Increase the vocab of the tokenizer
        tk.fit_on_texts(map(unidecode, df.question1.values.tolist() + df.question2.values.tolist()))

    char_index = tk.word_index
    print 'number of characters in text:', len(char_index)

    emb_matrix = np.zeros((len(char_index) + 1, 20))

    for char, i in char_index.items():
        # For now we let it be simple
        emb_matrix[i] = 10 ** -5 * np.random.rand((20))

    q1 = tk.texts_to_sequences(map(unidecode, df.question1.values.tolist()))
    q1 = sequence.pad_sequences(q1, maxlen=500)

    q2 = tk.texts_to_sequences(map(unidecode, df.question2.values.tolist()))
    q2 = sequence.pad_sequences(q2, maxlen=500)

    is_duplicate = df.is_duplicate.values
    magic_feat = np.array(df[['q1_q2_intersect', 'q1_freq', 'q2_freq']])

    model = model_conv_(emb_matrix)
    model.summary()

    # Create Checkpoint
    checkpoint = ModelCheckpoint("char_weight.hdf5", monitor='val_loss', save_best_only=True,
                                 verbose=1)

    # Finally train the fucking model
    model.fit(
        x=[q1, q2, magic_feat],
        y=is_duplicate,
        batch_size=128,
        epochs=5,
        verbose=1,
        callbacks=[checkpoint],
        validation_split=0.05,
        validation_data=None,
        shuffle=True,
        class_weight={0: 1.309028344, 1: 0.472001959},
        sample_weight=None,
        initial_epoch=0
    )


if __name__ == '__main__':
    config = SafeConfigParser()
    config.read('../config.ini')
    train_model(config)