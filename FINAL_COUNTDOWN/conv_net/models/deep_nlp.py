import numpy as np
import pandas as pd
import wrapper_models
import logging
import gensim
import glob

from unidecode import unidecode
from keras.models import load_model
from keras.preprocessing import sequence, text
from keras.callbacks import ModelCheckpoint


# Setup logger
logger = logging.getLogger(__name__)


def train_model(config):
    # Load the training data
    df = pd.read_csv(config.get('train', 'ld_dir') + 'train.csv', encoding='utf-8')

    # initialize the text tokenizer from keras (only using it since it allows for easy integration with the embedding)
    tk = text.Tokenizer(num_words=config.getint('train', 'max_nb_words'))

    # the tokenizer gets vocabulary
    for file in sorted(glob.glob(config.get('train', 'ld_dir') + '*.csv')):
        # Obtain dataframe
        df = pd.read_csv(file, encoding='utf-8').fillna('')

        # Increase the vocab of the tokenizer
        tk.fit_on_texts(map(unidecode, df.question1.values.tolist() + df.question2.values.tolist()))

    word_index = tk.word_index

    # Get the questions fitted
    if config.getboolean('train', 'q_switch'):
        q1 = tk.texts_to_sequences(map(unidecode, df.question1.values.tolist() + df.question2.values.tolist()))
        q1 = sequence.pad_sequences(q1, maxlen=config.getint('train', 'max_sent_len'))

        q2 = tk.texts_to_sequences(map(unidecode, df.question2.values.tolist() + df.question1.values.tolist()))
        q2 = sequence.pad_sequences(q2, maxlen=config.getint('train', 'max_sent_len'))

        is_duplicate = df.is_duplicate.values
        is_duplicate = np.concatenate((is_duplicate, is_duplicate))
        magic_feat = np.array(df[['q1_q2_intersect', 'q1_freq', 'q2_freq', 'qid1_max_kcore', 'qid2_max_kcore']])
        magic_feat = np.concatenate((magic_feat, magic_feat))
        dist_feat = np.array(df[[
            'tfidf_wm_stops',
            'char_tfidf_l2_cosine',
            'char_tfidf_l2_l1',
            'word_tfidf_l2_cosine',
            'word_tfidf_l2_l1',
            'skew_q1vec',
            'skew_q2vec',
            'kur_q1vec',
            'kur_q2vec'
        ]])
        dist_feat = np.concatenate((dist_feat, dist_feat))

    else:
        q1 = tk.texts_to_sequences(map(unidecode, df.question1.values.tolist()))
        q1 = sequence.pad_sequences(q1, maxlen=config.getint('train', 'max_sent_len'))

        q2 = tk.texts_to_sequences(map(unidecode, df.question2.values.tolist()))
        q2 = sequence.pad_sequences(q2, maxlen=config.getint('train', 'max_sent_len'))

        is_duplicate = df.is_duplicate.values
        magic_feat = np.array(df[['q1_q2_intersect', 'q1_freq', 'q2_freq', 'qid1_max_kcore', 'qid2_max_kcore']])
        dist_feat = np.array(df[[
            'tfidf_wm_stops',
            'tfidf_wm',
            'char_tfidf_l2_cosine',
            'char_tfidf_l2_l1',
            'word_tfidf_l2_cosine',
            'word_tfidf_l2_l1',
            'len_q1',
            'len_q2',
            'common_words',
            'fuzz_qratio',
            'fuzz_partial_ratio',
            'fuzz_token_set_ratio',
            'fuzz_token_sort_ratio',
            'fuzz_partial_token_sort_ratio',
            'wmd',
            'norm_wmd',
            'skew_q1vec',
            'skew_q2vec',
            'kur_q1vec',
            'kur_q2vec'
        ]])

    # Remove the dataframe
    df = []

    if config.getboolean('train', 'load_emb_matrix'):
        # Load the matrix
        emb_matrix = np.load('data/' + config.get('train', 'embedding') + '.npy')
    else:
        # The matrix that contains the vectors for the words
        emb_matrix = np.zeros((len(word_index) + 1, 300), dtype= np.float32)

        # Load the embedding
        emb_model = gensim.models.KeyedVectors.load_word2vec_format(config.get('train', 'embedding'), binary=config.getboolean('train', 'emb_binary'))

        # Fill the embedding matrix
        for word, i in word_index.items():
            # For now we let it be simple
            try:
                emb_matrix[i] = emb_model[word]
            except:
                if config.get('train', 'unrecog_method') == 'random':
                    emb_matrix[i] = 10 ** -5 * np.random.rand((300))
                elif config.get('train', 'unrecog_method') == 'zero':
                    print word

        # Save the matrix
        np.save('data/' + config.get('train', 'embedding'), emb_matrix)

        # delete stuff
        emb_model = []

    if config.get('train', 'model') == 'conv1D_lstm':
        model = wrapper_models.model_conv1D_lstm(emb_matrix)
    elif config.get('train', 'model') == 'conv1D_':
        model = wrapper_models.model_conv1D_(emb_matrix)
    elif config.get('train', 'model') == 'conv1D_simple':
        model = wrapper_models.model_conv1D_simple(emb_matrix)
    elif config.get('train', 'model') == 'lstm_':
        model = wrapper_models.model_lstm_(emb_matrix)
    elif config.get('train', 'model') == 'lstm_simple':
        model = wrapper_models.model_lstm_simple(emb_matrix)

    model.summary()

    # Initialize the stratified Kfold
    nb_split = 5

    for i in range(nb_split):
        print '\n', 'this is run: ', i + 1, '\n'

        # Get your model
        if config.get('train', 'model') == 'conv1D_lstm':
            model = wrapper_models.model_conv1D_lstm(emb_matrix)
        elif config.get('train', 'model') == 'conv1D_':
            model = wrapper_models.model_conv1D_(emb_matrix)
        elif config.get('train', 'model') == 'conv1D_simple':
            model = wrapper_models.model_conv1D_simple(emb_matrix)
        elif config.get('train', 'model') == 'lstm_':
            model = wrapper_models.model_lstm_(emb_matrix)
        elif config.get('train', 'model') == 'lstm_simple':
            model = wrapper_models.model_lstm_simple(emb_matrix)

        # Create Checkpoint
        checkpoint = ModelCheckpoint(config.get('train', 'checkpoint_loc') + 'weight' + str(i) + '.hdf5', monitor='val_loss', save_best_only=True, verbose=1)

        # Finally train the fucking model
        model.fit(
            x=[q1, q2, magic_feat, dist_feat],
            y=is_duplicate,
            batch_size=128,
            epochs=5,
            verbose=1,
            callbacks=[checkpoint],
            validation_split=0.1,
            shuffle=True,
            class_weight={0: 1.309028344, 1: 0.472001959},
            sample_weight=None,
            initial_epoch=0
        )



def predict_model(config):
    # Make train frame
    train_f = pd.DataFrame()

    # Make test frame
    test_f = pd.DataFrame()

    # initialize the text tokenizer from keras (only using it since it allows for easy integration with the embedding)
    tk = text.Tokenizer(num_words=config.getint('train', 'max_nb_words'))

    # Load the test data
    for file in sorted(glob.glob(config.get('predict', 'ld_dir') + '*.csv')):
        # Obtain dataframe
        df = pd.read_csv(file, encoding='utf-8').fillna('')

        # Increase the vocab of the tokenizer
        tk.fit_on_texts(map(unidecode, df.question1.values.tolist() + df.question2.values.tolist()))
        word_index = tk.word_index

        if 'test' in file:
            test_f = pd.concat([test_f, df[df.columns]])
        else:
            train_f = df

    # Transform the test questions, such that they are acceptable to

    q1 = tk.texts_to_sequences(map(unidecode, test_f.question1.values.tolist()))
    q1 = sequence.pad_sequences(q1, maxlen=config.getint('train', 'max_sent_len'))

    q2 = tk.texts_to_sequences(map(unidecode, test_f.question2.values.tolist()))
    q2 = sequence.pad_sequences(q2, maxlen=config.getint('train', 'max_sent_len'))

    # Get the magic features
    magic_feat = np.array(test_f[['q1_q2_intersect', 'q1_freq', 'q2_freq', 'qid1_max_kcore', 'qid2_max_kcore']])

    # Get distance features
    dist_feat = np.array(test_f[[
        'tfidf_wm_stops',
        'tfidf_wm',
        'char_tfidf_l2_cosine',
        'char_tfidf_l2_l1',
        'word_tfidf_l2_cosine',
        'word_tfidf_l2_l1',
        'len_q1',
        'len_q2',
        'common_words',
        'fuzz_qratio',
        'fuzz_partial_ratio',
        'fuzz_token_set_ratio',
        'fuzz_token_sort_ratio',
        'fuzz_partial_token_sort_ratio',
        'wmd',
        'norm_wmd',
        'skew_q1vec',
        'skew_q2vec',
        'kur_q1vec',
        'kur_q2vec'
    ]])

    test_f = test_f.drop(['question1',
                          'question2',
                          'q1_q2_intersect',
                          'q1_freq',
                          'q2_freq',
                          'qid1_max_kcore',
                          'qid2_max_kcore',
                          'tfidf_wm_stops',
                          'tfidf_wm',
                          'char_tfidf_l2_cosine',
                          'char_tfidf_l2_l1',
                          'word_tfidf_l2_cosine',
                          'word_tfidf_l2_l1',
                          'len_q1',
                          'len_q2',
                          'common_words',
                          'fuzz_qratio',
                          'fuzz_partial_ratio',
                          'fuzz_token_set_ratio',
                          'fuzz_token_sort_ratio',
                          'fuzz_partial_token_sort_ratio',
                          'wmd',
                          'norm_wmd',
                          'skew_q1vec',
                          'skew_q2vec',
                          'kur_q1vec',
                          'kur_q2vec'], axis=1)
    test_f = test_f.rename(columns={'id': 'test_id'})

    num = 0
    for file in sorted(glob.glob(config.get('predict', 'checkpoint_loc') + '*.hdf5')):
        # Make the model from the saved weights
        model = load_model(file)

        # Predict on test questions
        test_is_duplicate = model.predict([q1, q2, magic_feat, dist_feat], batch_size=128, verbose=1)
        test_f['is_duplicate'] = test_is_duplicate
        test_for_csv = test_f[['test_id', 'is_duplicate']]
        for i, row in test_for_csv.iterrows():
            row['test_id'] = int(i)
        test_for_csv.to_csv(config.get('predict', 'sv_dir') + 'pred' + str(num) + '.csv', index=False, encoding='utf-8')

        # NEXT
        num += 1

    # Blending of the files
    test_blend = pd.DataFrame()
    num = 0
    for file in sorted(glob.glob(config.get('predict', 'sv_dir') + '*.csv')):
        temp_df = pd.read_csv(file)
        test_blend[str(num)] = temp_df[['is_duplicate']]

    np_pred = test_blend.as_matrix()
    print np_pred


    return 1