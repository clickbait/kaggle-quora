import pandas as pd
import numpy as np
import logging

from scipy import sparse
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from collections import defaultdict
from functools import partial
from collections import Counter
from nltk.corpus import stopwords

logger = logging.getLogger(__name__)

def get_weight(count, eps=10000, min_count=2):
    if count < min_count:
        return 0
    else:
        return 1 / float(count + eps)


def common_characters(row):
    return len(set(row['question1']).intersection(set(row['question2'])))


def tfidf_word_match_share_stops(row, stops=None, weights=None):
    q1words = {}
    q2words = {}
    for word in row['question1'].lower().split():
        if word not in stops:
            q1words[word] = 1
    for word in row['question2'].lower().split():
        if word not in stops:
            q2words[word] = 1
    if len(q1words) == 0 or len(q2words) == 0:
        # The computer-generated chaff includes a few questions that are nothing but stopwords
        return 0

    shared_weights = [weights.get(w, 0) for w in q1words.keys() if w in q2words] + [weights.get(w, 0) for w in q2words.keys() if w in q1words]
    total_weights = [weights.get(w, 0) for w in q1words] + [weights.get(w, 0) for w in q2words]

    if np.sum(total_weights) == 0:
        R = 0
    else:
        R = np.sum(shared_weights) /float(np.sum(total_weights))
    return R


def tfidf_func(gram_q1, gram_q2, norm='l2', func='cosine'):
    """
    Uses the term frequency inverse document frequency to create a measure 

    :param gram_q1: the count grams for question 1
    :param gram_q2: the count grams for question 2
    :param norm: the norm used to normalize the counts/frequency, options are l1, l2, none
    :param func: the function to calculate the differences options are cosine, l1, l2
    :return: 
    """
    tft = TfidfTransformer(norm=norm)
    tft = tft.fit(sparse.vstack((gram_q1, gram_q2)))
    gram_q1_tf = tft.transform(gram_q1)
    gram_q2_tf = tft.transform(gram_q2)

    if func == 'cosine':
        v_num = np.array(gram_q1_tf.multiply(gram_q2_tf).sum(axis=1))[:, 0]
        v_den = np.array(np.sqrt(gram_q1_tf.multiply(gram_q1_tf).sum(axis=1)))[:, 0] * \
                np.array(np.sqrt(gram_q2_tf.multiply(gram_q2_tf).sum(axis=1)))[:, 0]

        v_num[np.where(v_den == 0)] = 1
        v_den[np.where(v_den == 0)] = 1

        cosine = 1 - v_num / v_den
        return cosine

    elif func == 'l1':
        dif = (gram_q1_tf - gram_q2_tf)
        L1 = np.array(np.abs(dif).sum(axis=1))[:, 0]
        return L1

    elif func == 'l2':
        dif = (gram_q1_tf - gram_q2_tf)
        L2 = np.sqrt(np.array(dif.multiply(dif).sum(axis=1))[:, 0])
        return L2

    else:
        print 'no correct function selected for the tfidf_func, pls change'


def inject_magic(config):
    train_df = pd.read_csv(config.get('magic', 'ld_train_path')).fillna('')
    test_df = pd.read_csv(config.get('magic', 'ld_test_path')).fillna('')

    # Calculating the magic features
    logger.info('Calculating the magic features')
    ques = pd.concat([train_df[['question1', 'question2']], test_df[['question1', 'question2']]], axis=0).reset_index(drop='index')
    q_dict = defaultdict(set)
    for i in range(ques.shape[0]):
            q_dict[ques.question1[i]].add(ques.question2[i])
            q_dict[ques.question2[i]].add(ques.question1[i])

    def q1_freq(row):
        return(len(q_dict[row['question1']]))

    def q2_freq(row):
        return(len(q_dict[row['question2']]))

    def q1_q2_intersect(row):
        return(len(set(q_dict[row['question1']]).intersection(set(q_dict[row['question2']]))))

    train_df['q1_q2_intersect'] = train_df.apply(q1_q2_intersect, axis=1, raw=True)
    train_df['q1_freq'] = train_df.apply(q1_freq, axis=1, raw=True)
    train_df['q2_freq'] = train_df.apply(q2_freq, axis=1, raw=True)

    test_df['q1_q2_intersect'] = test_df.apply(q1_q2_intersect, axis=1, raw=True)
    test_df['q1_freq'] = test_df.apply(q1_freq, axis=1, raw=True)
    test_df['q2_freq'] = test_df.apply(q2_freq, axis=1, raw=True)

    # Delete
    q_dict = []
    ques = []

    # Create a lower case stuff for the training set and test_set
    logger.info('Calculating the word_match stuff')
    train_df_lower = pd.DataFrame()
    test_df_lower = pd.DataFrame()

    train_df_lower['question1'] = train_df['question1'].map(lambda x: str(x).lower().split())
    train_df_lower['question2'] = train_df['question2'].map(lambda x: str(x).lower().split())
    test_df_lower['question1'] = test_df['question1'].map(lambda x: str(x).lower().split())
    test_df_lower['question2'] = test_df['question2'].map(lambda x: str(x).lower().split())

    # Calculate weights from VIEZE_XGBOOST and initialize the stopwords
    train_qs = pd.Series(train_df_lower['question1'].tolist() + train_df_lower['question2'].tolist())

    words = [x for y in train_qs for x in y]
    counts = Counter(words)
    weights = {word: get_weight(count) for word, count in counts.items()}
    stops = set(stopwords.words("english"))

    # Calculating VIEZE_XGBOOST features
    f = partial(tfidf_word_match_share_stops, stops=stops, weights=weights)
    train_df['tfidf_wm_stops'] = train_df.apply(f, axis=1, raw=True)
    test_df['tfidf_wm_stops'] = test_df.apply(f, axis=1, raw=True)
    f = partial(tfidf_word_match_share_stops, stops=[], weights=weights)
    train_df['tfidf_wm'] = train_df.apply(f, axis=1, raw=True)
    test_df['tfidf_wm'] = test_df.apply(f, axis=1, raw=True)

    del train_qs, train_df_lower, test_df_lower, words, counts, stops, f

    # Calculate the russian tfidf features
    logger.info('Doing the tfidf character stuff')
    cv_char = CountVectorizer(ngram_range=(1, 3), analyzer='char', min_df=2)
    cv_char.fit(train_df['question1'].tolist() +\
                          train_df['question2'].tolist() +\
                          test_df['question1'].tolist() +\
                          test_df['question2'].tolist())

    train_char_gram_q1 = cv_char.transform(train_df['question1'])
    train_char_gram_q2 = cv_char.transform(train_df['question2'])
    test_char_gram_q1 = cv_char.transform(test_df['question1'])
    test_char_gram_q2 = cv_char.transform(test_df['question2'])

    train_df['char_tfidf_l2_cosine'] = tfidf_func(train_char_gram_q1, train_char_gram_q2, norm='l2', func='cosine')
    train_df['char_tfidf_l2_l1'] = tfidf_func(train_char_gram_q1, train_char_gram_q2, norm='l2', func='l1')
    test_df['char_tfidf_l2_cosine'] = tfidf_func(test_char_gram_q1, test_char_gram_q2, norm='l2', func='cosine')
    test_df['char_tfidf_l2_l1'] = tfidf_func(test_char_gram_q1, test_char_gram_q2, norm='l2', func='l1')

    del cv_char, train_char_gram_q1, train_char_gram_q2, test_char_gram_q1, test_char_gram_q2

    logger.info('Doing the tfidf word stuff')
    cv_word = CountVectorizer(ngram_range=(1, 3), analyzer='word', min_df=2)
    cv_word.fit(train_df['question1'].tolist() + \
              train_df['question2'].tolist() + \
              test_df['question1'].tolist() + \
              test_df['question2'].tolist())

    train_word_gram_q1 = cv_word.transform(train_df['question1'])
    train_word_gram_q2 = cv_word.transform(train_df['question2'])
    test_word_gram_q1 = cv_word.transform(test_df['question1'])
    test_word_gram_q2 = cv_word.transform(test_df['question2'])

    train_df['word_tfidf_l2_cosine'] = tfidf_func(train_word_gram_q1, train_word_gram_q2, norm='l2', func='cosine')
    train_df['word_tfidf_l2_l1'] = tfidf_func(train_word_gram_q1, train_word_gram_q2, norm='l2', func='l1')
    test_df['word_tfidf_l2_cosine'] = tfidf_func(test_word_gram_q1, test_word_gram_q2, norm='l2', func='cosine')
    test_df['word_tfidf_l2_l1'] = tfidf_func(test_word_gram_q1, test_word_gram_q2, norm='l2', func='l1')

    del cv_word, train_word_gram_q1, train_word_gram_q2, test_word_gram_q1, test_word_gram_q2

    # Merge with the features generated by abhishek
    logger.info('Adding kcore, fuzz, manual and wmd features')
    train_features = pd.read_csv(config.get('magic', 'ld_feat_dir') + 'train_features.csv')
    test_features = pd.read_csv(config.get('magic', 'ld_feat_dir') + 'test_features.csv')
    train_kcore = pd.read_csv(config.get('magic', 'ld_feat_dir') + 'kcores_train.csv')
    test_kcore = pd.read_csv(config.get('magic', 'ld_feat_dir') + 'kcores_test.csv')

    # Add the kcore
    train_df[['qid1_max_kcore', 'qid2_max_kcore']] = train_kcore[['qid1_max_kcore', 'qid2_max_kcore']]
    test_df[['qid1_max_kcore', 'qid2_max_kcore']] = test_kcore[['qid1_max_kcore', 'qid2_max_kcore']]

    # Add a few (strong) manual features
    train_df[['len_q1', 'len_q2', 'common_words']] = train_features[['len_q1', 'len_q2', 'common_words']]
    test_df[['len_q1', 'len_q2', 'common_words']] = test_features[['len_q1', 'len_q2', 'common_words']]

    # Add fuzz features
    train_df[['fuzz_qratio', 'fuzz_partial_ratio', 'fuzz_token_set_ratio', 'fuzz_token_sort_ratio', 'fuzz_partial_token_sort_ratio']] = train_features[['fuzz_qratio', 'fuzz_partial_ratio', 'fuzz_token_set_ratio', 'fuzz_token_sort_ratio', 'fuzz_partial_token_sort_ratio']]
    test_df[['fuzz_qratio', 'fuzz_partial_ratio', 'fuzz_token_set_ratio', 'fuzz_token_sort_ratio', 'fuzz_partial_token_sort_ratio']] = test_features[['fuzz_qratio', 'fuzz_partial_ratio', 'fuzz_token_set_ratio', 'fuzz_token_sort_ratio', 'fuzz_partial_token_sort_ratio']]

    # Add word mover features
    train_df[['wmd', 'norm_wmd']] = train_features[['wmd', 'norm_wmd']]
    test_df[['wmd', 'norm_wmd']] = test_features[['wmd', 'norm_wmd']]

    # Save files
    train_df.to_csv(config.get('magic', 'sv_train_path') + '.csv', encoding='utf-8', index=False)
    test_df.to_csv(config.get('magic', 'sv_test_path') + '.csv', encoding='utf-8', index=False)