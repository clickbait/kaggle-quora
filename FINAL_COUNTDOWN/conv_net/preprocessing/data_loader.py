import pandas as pd
import numpy as np
import logging

# Setup logger
logger = logging.getLogger(__name__)

def load_train(config):
    """
    Load the training set, if specified, it will only have unique entries in the training set
    
    :param config: the settings 
    :return: -
    """
    # Obtain settings
    ld_path = config.get('loader', 'ld_train_path')
    sv_path = config.get('loader', 'sv_train_path')
    true_unique = config.getboolean('loader', 'true_unique')

    # Load dataset
    df = pd.read_csv(ld_path, encoding='utf-8').fillna('')

    # Do we only want unique questions?
    if true_unique:
        logger.info('Ensuring we have unique questions in the training set')

        # Count number of duplicates
        q1_count = df['qid1'].value_counts()
        q2_count = df['qid2'].value_counts()

        # Take question ids with count 1 (uniques)
        q1_unique = list(q1_count.loc[q1_count == 1].index)
        q2_unique = list(q2_count.loc[q2_count == 1].index)

        # Get the unique question ids from original data frame
        q1df = df.loc[df['qid1'].isin(q1_unique)]
        q2df = df.loc[df['qid2'].isin(q2_unique)]

        # Get the set of unique questions
        qidx_unique = list(set(q1df.index) & set(q2df.index))

        # Remove the duplicates
        df = df.loc[qidx_unique].reset_index(drop=True)

    df.to_csv(sv_path + '.csv', encoding='utf-8', index=False)


def load_test(config):
    """
    Load the test set en split it into multiple parts if needed
    
    :param config: the settings specified in config.ini
    :return: -
    """
    # Obtain settings
    ld_path = config.get('loader', 'ld_test_path')
    sv_path = config.get('loader', 'sv_test_path')
    nb_split = float(config.getint('loader', 'test_split'))


    df = pd.read_csv(ld_path, encoding='utf-8').fillna('')
    for g, split_df in df.groupby(np.arange(0, len(df)) / int(np.ceil(len(df)/nb_split))):
        split_df.to_csv(sv_path + '{}.csv'.format(g), encoding='utf-8', index=False)



