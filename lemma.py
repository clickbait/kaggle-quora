from nltk.stem.wordnet import WordNetLemmatizer as wnl
import re
import pandas as pd
import socket
import os
from nltk.corpus import stopwords
from nltk.stem.lancaster import LancasterStemmer
from ConfigParser import SafeConfigParser
import sys


stop_words = stopwords.words('english')


def lemstem(config_file):
    config = SafeConfigParser()
    config.read(config_file)
    data_path = config.get('data', 'data_path')
    target_path = data_path + 'train_lemstem.csv'
    data_path = data_path + 'train_preprocessed.csv'

    print 'Loading data...'

    data_frame = pd.read_csv(data_path, encoding='utf-8')

    new_data_frame = pd.DataFrame()

    lem_sen_q1, lem_sen_q2 = [], []
    stem_sen_q1, stem_sen_q2 = [], []
    print 'Performing stemming and lemmatization...'

    for i in range(0,data_frame['unigram1'].shape[0]):

        ls = LancasterStemmer()
        stem_sen_q1.append([ls.stem(word) for word in re.sub('[\[\]\?\,\']', '', data_frame['unigram1'][i]).split()])
        stem_sen_q2.append([ls.stem(word) for word in re.sub('[\[\]\?\,\']', '', data_frame['unigram2'][i]).split()])

        lmtzr = wnl()
        lem_sen_q1.append([lmtzr.lemmatize(word) for word in re.sub('[\[\]\?\,\']', '', data_frame['unigram1'][i]).split()])
        lem_sen_q2.append([lmtzr.lemmatize(word) for word in re.sub('[\[\]\?\,\']', '', data_frame['unigram2'][i]).split()])


    new_data_frame.insert(0,'id',data_frame['id'])
    new_data_frame.insert(1,'lancaster_stem_q1',stem_sen_q1)
    new_data_frame.insert(2,'wordnet_lemma_q1',lem_sen_q1)
    new_data_frame.insert(3,'lancaster_stem_q2',stem_sen_q2)
    new_data_frame.insert(3,'wordnet_lemma_q2',lem_sen_q2)

    print 'Saving to file...'
    new_data_frame.to_csv(target_path, index=False, encoding='utf-8')

if __name__ == "__main__":
    # Loading the config file. Add your hostname here to make it work. If you don't know your hostname, just run the
    # script once.
    config_path = "./config/"
    hostnames = {
        "int1.cartesius.surfsara.nl": 'jelle.ini',
        "mrplank-MS-B90111": 'simon.ini',
        "lumos-XPS-15-9560": 'alex.ini',
        "macco-linux": 'daniele.ini',
        "rooler": 'roeland.ini'
    }

    # Changing the config file automatically based on the hostname
    hostname = socket.gethostname()
    for candidate, config_file in hostnames.iteritems():
        if hostname == candidate:
            config_file = config_file
            break
    else:
        print "Your hostname is '%s', add it to the main.py config so your config is automagically loaded"
        sys.exit()
    # Starting the main function
    config = os.path.join(config_path, config_file)
    lemstem(config)
