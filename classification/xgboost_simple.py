import logging
import xgboost as xgb
import matplotlib.pyplot as plt

logger = logging.getLogger(__name__)


#https://github.com/dmlc/xgboost/blob/master/doc/parameter.md
class ClassifierXgboost(object):
    def __init__(self, config):
        self.config = config
        self.model = None

    def fit(self, trn_feat, trn_lbl, val_feat=None, val_lbl=None):
        logging.info("Fitting xgboost")
        num_boost_round = self.config.getint('classifier_xgboost_simple', 'num_boost_round')

        params = {
            'objective': self.config.get('classifier_xgboost_simple', 'objective'),
            'eval_metric': self.config.get('classifier_xgboost_simple', 'eval_metric'),
            'eta': self.config.getfloat('classifier_xgboost_simple', 'eta'),
            'subsample': self.config.getfloat('classifier_xgboost_simple', 'subsample'),
            'max_depth': self.config.getint('classifier_xgboost_simple', 'max_depth'),
            'verbose': self.config.getint('classifier_xgboost_simple', 'verbose'),
            'nthread': self.config.getint('multiprocessing', 'cpu_cores'),
            'silent': self.config.getint('classifier_xgboost_simple', 'silent'),
            'n_estimators':  self.config.getint('classifier_xgboost_simple', 'n_estimators'),
            'scale_pos_weight':  self.config.getfloat('classifier_xgboost_simple', 'scale_pos_weight'),
            'seed': 42,
        }

        d_train = xgb.DMatrix(trn_feat, label=trn_lbl)
        watchlist = [(d_train, 'train')]

        # Ignore validation set if not provided
        if val_feat is not None and val_lbl is not None:
            d_valid = xgb.DMatrix(val_feat, label=val_lbl)
            watchlist.append((d_valid, 'valid'))

        self.model = xgb.train(params, d_train, num_boost_round, watchlist, verbose_eval=10, early_stopping_rounds=50)

        ax = xgb.plot_importance(self.model)
        fig = ax.figure
        fig.set_size_inches(15, 24)
        plt.savefig('feature_importance.png')
        # plt.show()
        plt.close()

        xgb.plot_tree(self.model, num_trees=6)
        #fig = ax.figure
        #fig.set_size_inches(24, 15)
        #plt.savefig('tree.png')
        plt.show()
        # plt.close()

    def predict(self, tst_feat):
        d_test = xgb.DMatrix(tst_feat)
        p_test = self.model.predict(d_test)

        return p_test
