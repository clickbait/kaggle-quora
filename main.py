import os
import socket
from ConfigParser import SafeConfigParser
import logging
import sys
import glob
import math

import pandas as pd
from sklearn.metrics import log_loss
from sklearn.externals import joblib
from sklearn.linear_model import Ridge, Lasso
from sklearn.svm import SVC

import xgboost as xgb

import numpy as np

from classification.xgboost_simple import ClassifierXgboost
from data.dataset_splitter import split_data
from data.feature_merge import merge_feature_files
from data.preprocess import preprocessing
from feature_extract import feature_manual, feature_word2vec, feature_pos, feature_distance, feature_word_match, feature_gram
from common import parallel
import cPickle as pickle

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 2000)
logging.basicConfig(level=logging.INFO)


# important
# TODO: fix the splits / splits K-fold / k-folding
# TODO: merge the predictions for a certain configuration

# nice2have
# TODO: xgboost on GPU
# TODO: set feature extractors / classifiers in config
# TODO: include stacking
# TODO: simplify paths
# TODO: store splits to disk
# TODO: data augmentation
# TODO: add trigrams in preprocessing


class QuoraDuplicateEnd2End(object):
    def __init__(self, config_file):
        self.config = SafeConfigParser()
        self.config.read(config_file)

        # Create directories if they do not exist
        paths = [
            'model_path',
            'feature_path'
        ]
        for path in paths:
            full_path = os.path.join(self.config.get('data', 'data_path'), self.config.get('data', path))
            if not os.path.exists(full_path):
                os.makedirs(full_path)
            if path == 'model_path':
                for each_section in self.config.sections():
                    for (each_key, each_val) in self.config.items(each_section):
                        if each_key == 'model_file':
                            model_full_path = os.path.dirname(os.path.join(full_path, each_val))
                            if not os.path.exists(model_full_path):
                                os.makedirs(model_full_path)

        # Save the split size
        self.test_split = self.config.getint('preprocessing', 'test_split')

    def preprocess_train(self):
        logging.info("Preprocessing training set")
        # train_file = self.config.get('data', 'train_file')
        # train_file_labels = self.config.get('preprocessing', 'train_labels_file')
        # train_file_preprocessed = self.config.get('preprocessing', 'train_file')
        # preprocessing(self.config, train_file, train_file_preprocessed, train_file_labels)

        # Split the process into different file according to the config file
        train_split = self.config.getint('preprocessing', 'train_split')
        train_file = self.config.get('data', 'train_file')
        train_file_labels = self.config.get('preprocessing', 'train_labels_file')
        train_file_preprocessed = self.config.get('preprocessing', 'train_file')

        # Get input path
        data_path = self.config.get('data', 'data_path')
        full_path = os.path.join(data_path, train_file)

        # Get label path
        full_label_path = os.path.join(data_path, "%s.csv" % train_file_labels)
        full_path_preprocessed = os.path.join(data_path, "%s.csv" % train_file_preprocessed)

        # Process the data in chunks
        # if (train_split > 0):
        #     i = 0
        #     # Load the source file
        #     train_file_preprocessed_i = "%s.%d.csv" % (train_file_preprocessed, i)
        #     logging.info("Preprocessing %s and outputting to %s." % (train_file, train_file_preprocessed_i))
        #
        #     for data_frame_in in pd.read_csv(full_path, encoding='utf-8', chunksize=train_split):
        #         data_frame_out = preprocessing(self.config, data_frame_in, os.join(data_path, full_label_path))
        #
        #         # Save new frame
        #         full_path_preprocessed = os.path.join(data_path, train_file_preprocessed_i)
        #         data_frame_out.to_csv(full_path_preprocessed, index=False, encoding='utf-8')
        #
        #         i += 1
        # else:
        data_frame_in = pd.read_csv(full_path, encoding='utf-8')
        preprocessing(self.config, data_frame_in, full_path_preprocessed, full_label_path)


    def preprocess_test(self):
        logging.info("Preprocessing test set")
        # test_file = self.config.get('data', 'test_file')
        # test_file_preprocessed = self.config.get('preprocessing', 'test_file')
        # preprocessing(self.config, test_file, test_file_preprocessed)

        # Get input path
        data_path = self.config.get('data', 'data_path')

        test_file = self.config.get('data', 'test_file')
        test_file_preprocessed = self.config.get('preprocessing', 'test_file')
        test_full_path = os.path.join(data_path, test_file)

        if self.test_split > 1:
            logging.info("Splitting test set into %d chunks" % self.test_split)
            i = 0
            # Load the source file
            test_rows_count = float(2345796)
            chunck_size = math.ceil(test_rows_count / float(self.test_split))
            for data_frame_in in pd.read_csv(test_full_path, encoding='utf-8', chunksize=chunck_size):
                test_file_preprocessed_i = "%s.%d.csv" % (test_file_preprocessed, i)

                logging.info("Preprocessing %s and outputting to %s." % (test_file, test_file_preprocessed_i))
                # print(data_frame_in)
                full_path_preprocessed = os.path.join(data_path, test_file_preprocessed_i)

                preprocessing(self.config, data_frame_in, full_path_preprocessed)

                i += 1
        else:
            full_path_preprocessed = os.path.join(data_path, "%s.csv" % test_file_preprocessed)

            data_frame_in = pd.read_csv(test_full_path, encoding='utf-8')
            preprocessing(self.config, data_frame_in, full_path_preprocessed)

    def preprocess(self):
        """Preprocesses the train.csv and test.csv files and save this to a new DataFrame, where a column is added."""

        if self.config.getboolean('preprocessing', 'active'):
            self.preprocess_train()
            self.preprocess_test()
        else:
            logging.info("Not doing preprocessing today.")

    def split(self):
        """Creates train & validation splits from the train.csv and returns its keys."""
        for run_id in range(1, self.config.getint('cross-validation', 'runs') + 1):
            data_path = self.config.get('data', 'data_path')
            train_file_preprocessed = os.path.join(data_path, self.config.get('preprocessing', 'train_file'))
            traindf = pd.read_csv("%s.csv" % train_file_preprocessed, encoding='utf-8')

            fulltrain, fullval = split_data(self.config, self.config.getint('cross-validation','splits'))

            for split_id in range(0, self.config.getint('cross-validation', 'splits')):
                training_ids, validation_ids = fulltrain[split_id], fullval[split_id]
                yield run_id, split_id, training_ids, validation_ids

    def feature_extract_train(self, config_section, data_path, feature_path, f):
        # Extract the features from the preprocessed train file
        # train_file_in = self.config.get('preprocessing', 'train_file')
        # train_file_out = self.config.get(config_section, 'train_feature_file')
        # f(self.config, config_section, os.path.join(data_path, train_file_in),
        #   os.path.join(feature_path, train_file_out))

        data_path = self.config.get('data', 'data_path')
        feature_path = os.path.join(data_path, self.config.get('data', 'feature_path'))

        # Split the process into different file according to the config file
        train_split = self.config.getint('preprocessing', 'train_split')
        train_file_in = self.config.get('preprocessing', 'train_file')
        train_file_out = self.config.get(config_section, 'train_feature_file')

        # Feature extraction
        # if train_split > 0:
        #     i = 0
        #
        #     # Load the source file
        #     for data_frame in pd.read_csv("%s.%d.csv" % (train_file_in, i), chunksize=train_split, encoding='utf-8'):
        #         train_file_out_i = "%s.%d.csv" % (train_file_out, i)
        #
        #         feature_frame = f(self.config, config_section, data_frame)
        #         feature_frame.to_csv(os.path.join(feature_path, train_file_out_i), index=False,
        #                              encoding='utf-8')
        #
        #     i += 1
        # else:
        train_data_frame = pd.read_csv(os.path.join(data_path, "%s.csv" % train_file_in), encoding='utf-8')
        logging.info("Calculating features for %s", train_file_out)

        feature_frame = f(self.config, config_section, train_data_frame)
        feature_frame.to_csv(os.path.join(feature_path, "%s.csv" % train_file_out), index=False, encoding='utf-8')

    def feature_extract_test(self, config_section, data_path, feature_path, f):
        # Extract the features from the preprocessed test file
        # test_file_in = self.config.get('preprocessing', 'test_file')
        # test_file_out = self.config.get(config_section, 'test_feature_file')
        # f(self.config, config_section, os.path.join(data_path, test_file_in),
        #   os.path.join(feature_path, test_file_out))

        data_path = self.config.get('data', 'data_path')
        feature_path = os.path.join(data_path, self.config.get('data', 'feature_path'))

        # Split the process into different file according to the config file
        test_file_in = self.config.get('preprocessing', 'test_file')
        test_file_out = self.config.get(config_section, 'test_feature_file')

        # Feature extraction
        if self.test_split > 0:
            # Load the source file
            split_no = len(glob.glob(os.path.join(data_path, "%s*.csv" % test_file_in)))

            for i in range(0, self.config.getint('preprocessing', 'test_split')):
                test_file_out_i = "%s.%d.csv" % (test_file_out, i)
                test_file_in_i_path = os.path.join(data_path, "%s.%d.csv" % (test_file_in, i))

                data_frame = pd.read_csv(test_file_in_i_path, encoding='utf-8')

                logging.info("Calculating features for %s", test_file_out_i)
                feature_frame = f(self.config, config_section, data_frame)
                feature_frame.to_csv(os.path.join(feature_path, test_file_out_i), index=False, encoding='utf-8')
        else:
            test_data_frame = pd.read_csv(os.path.join(data_path, "%s.csv" % test_file_in), encoding='utf-8')
            logging.info("Calculating features for %s", test_file_out)

            feature_frame = f(self.config, config_section, test_data_frame)
            feature_frame.to_csv(os.path.join(feature_path, "%s.csv" % test_file_out), index=False, encoding='utf-8')

    def feature_extraction(self, feature_extractors):
        """Generates DataFrames with features based on the preprocessed data."""
        data_path = self.config.get('data', 'data_path')
        feature_path = os.path.join(data_path, self.config.get('data', 'feature_path'))

        # Extract all the features
        for config_section, f in feature_extractors.iteritems():
            if self.config.getboolean(config_section, 'active'):
                logging.info("Extracting features for '%s'" % config_section)

                self.feature_extract_train(config_section, data_path, feature_path, f)
                self.feature_extract_test(config_section, data_path, feature_path, f)

    def get_submission_filename(self, run_id, split_id, submission_path, classifier_config_section):
        ext = ".test.run_%d.split_%d." % (run_id, split_id)

        file_name = self.config.get(classifier_config_section, 'submission_file')
        file_base, file_ext = file_name.split(".", -1)
        file_name = file_base + ext + file_ext
        return os.path.join('./', submission_path, file_name)
        
    def get_val_submission_filename(self, run_id, split_id, submission_path, classifier_config_section):
        ext = ".val.run_%d.split_%d." % (run_id, split_id)

        file_name = self.config.get(classifier_config_section, 'submission_file')
        file_base, file_ext = file_name.split(".", -1)
        file_name = file_base + ext + file_ext
        return os.path.join('./', submission_path, file_name)

    def get_submission_path(self, feature_config_section):
        submission_path = self.config.get('data', 'submission_path')
        submission_path = os.path.join(submission_path, feature_config_section)

        return submission_path

    def get_model_filename(self, run_id, split_id, config_section):
        data_path = self.config.get('data', 'data_path')
        model_path = self.config.get('data', 'model_path')
        ext = ".run_%d.split_%d." % (run_id, split_id)

        file_name = self.config.get(config_section, 'model_file')
        file_base, file_ext = file_name.split(".", -1)
        file_name = file_base + ext + file_ext
        return os.path.join(data_path, model_path, file_name)

    def model_training(self, run_id, split_id, train_ids, validation_ids, feature_classifiers):
        """The first 'layer' of classification generates predictions from models."""

        data_path = self.config.get('data', 'data_path')
        model_path = os.path.join(data_path, self.config.get('data', 'model_path'))
        feature_path = os.path.join(data_path, self.config.get('data', 'feature_path'))

        for feature_config_section, classifiers in feature_classifiers.iteritems():
            # Load the precomputed features
            feature_file = os.path.join(feature_path, self.config.get(feature_config_section, 'train_feature_file'))
            X_base = pd.read_csv("%s.csv" % feature_file, index_col=0, encoding='utf-8')

            # Load the true labels of the data
            labels_file = os.path.join(data_path, self.config.get('preprocessing', 'train_labels_file'))
            Y_base = pd.read_csv("%s.csv" % labels_file, encoding='utf-8')


            # Split the features
            X_train_feat = X_base.ix[train_ids]
            with open('x_train_feat.pickle', 'w+') as f:
                pickle.dump(X_train_feat, f)
            #X_train_feat = X_train_feat.drop(['id'])

            X_val_feat = X_base.ix[validation_ids]
            with open('x_test_feat.pickle', 'w+') as f:
                pickle.dump(X_val_feat, f)
            #X_val_feat_id = X_val_feat['id']
            #X_val_feat = X_val_feat.drop(['id'])
            del X_base

            # Split the labels
            Y_train = Y_base['is_duplicate'].ix[train_ids]
            with open('y_train.pickle', 'w+') as f:
                pickle.dump(Y_train, f)
            Y_val = Y_base['is_duplicate'].ix[validation_ids]
            with open('y_test.pickle', 'w+') as f:
                pickle.dump(Y_val, f)
            del Y_base

            # Train all models
            for classifier_config_section, clf in classifiers.iteritems():
                if self.config.getboolean(classifier_config_section, 'active'):
                    logging.info("Training the '%s' model" % classifier_config_section)
                    if clf.__class__.__name__ == "ClassifierXgboost":
                        clf.fit(X_train_feat, Y_train, X_val_feat, Y_val)
                    else:
                        clf.fit(X_train_feat, Y_train)
                    logging.info("Done training the '%s' model" % classifier_config_section)

                    # Evaluates the performance of the models on the validation set.
                    Y_pred_val = clf.predict(X_val_feat)
                    
                    val_sub = pd.DataFrame(columns=['val_id', 'is_duplicate'])
                    val_sub['val_id'] = X_val_feat.index.values
                    val_sub['is_duplicate'] = Y_pred_val

                    val_sub_file = self.get_val_submission_filename(run_id, split_id, self.get_submission_path(feature_config_section), classifier_config_section)

                    submission_path = self.get_submission_path(feature_config_section)
                    if not os.path.exists(submission_path):
                        os.mkdir(submission_path)

                    val_sub.to_csv(val_sub_file, index=False, encoding='utf-8')

                    score = log_loss(Y_val, Y_pred_val)
                    logging.info("Log loss on the validation set for %s is %f" % (classifier_config_section, score))

                    # Save the model
                    model_file = self.get_model_filename(run_id, split_id, classifier_config_section)
                    joblib.dump(clf, model_file, compress=9)

    def make_submission(self, run_id, split_id, feature_classifiers):
        """Runs the models on the test set and outputs a csv that can be submitted to Kaggle."""
        data_path = self.config.get('data', 'data_path')
        feature_path = os.path.join(data_path, self.config.get('data', 'feature_path'))

        for feature_config_section, classifiers in feature_classifiers.iteritems():
            for classifier_config_section, clf in classifiers.iteritems():
                if self.config.getboolean(classifier_config_section, 'active'):
                    model_file = self.get_model_filename(run_id, split_id, classifier_config_section)

                    clf = joblib.load(model_file)

                    test_feature_file = os.path.join(feature_path,
                                                     self.config.get(feature_config_section, 'test_feature_file'))

                    sub = pd.DataFrame(columns=['test_id', 'is_duplicate'])
                    Y_test = np.array([])

                    if(self.test_split > 0):
                        split_no = len(glob.glob(os.path.join(data_path, "%s*.csv" % test_feature_file)))

                        for i in range(0, split_no):
                            sub_i = pd.DataFrame()

                            # Load the test features
                            test_feature_file_i = "%s.%d.csv" % (test_feature_file, i)
                            print test_feature_file_i

                            X_test_feat = pd.read_csv(test_feature_file_i, index_col=0, encoding='utf-8')
                            Y_test = clf.predict(X_test_feat)

                            sub_i['test_id'] = X_test_feat.index.values.astype(int)
                            sub_i['is_duplicate'] = Y_test

                            sub = sub.append(sub_i)
                    else:
                        # Load the test features
                        X_test_feat = pd.read_csv("%s.csv" % test_feature_file, index_col=0, encoding='utf-8')
                        Y_test = clf.predict(X_test_feat)

                        sub['test_id'] = X_test_feat.index.values.astype(int)
                        sub['is_duplicate'] = Y_test

                    logging.info("Create submission file")

                    submission_path = self.get_submission_path(feature_config_section)
                    if not os.path.exists(submission_path):
                        os.mkdir(submission_path)

                    submission_file = self.get_submission_filename(run_id, split_id, submission_path, classifier_config_section)
                    sub.to_csv(submission_file, index=False, encoding='utf-8')

    def blend_submissions(self, feature_classifiers):
        if not self.config.getboolean('blend', 'active'):
            logging.info("Not doing blend today")
            return

        # 1. Get the submissions for each classifier
        for feature_config_section, classifiers in feature_classifiers.iteritems():
            for classifier_config_section, clf in classifiers.iteritems():
                # if self.config.getboolean(classifier_config_section, 'active'):
                logging.info("Blending submissions for classifier %s", classifier_config_section)

                submission_path = self.get_submission_path(feature_config_section)

                # Get the submission file name from the classifier's config section
                clf_submission_filename = self.config.get(classifier_config_section, 'submission_file')
                clf_submission_filename = os.path.splitext(clf_submission_filename)[0]
                clf_submission_path = os.path.join(submission_path, clf_submission_filename)

                files = glob.glob("%s.test.*.csv" % clf_submission_path)

                if len(files) == 0:
                    print logging.warning("No submission found for this classifier, skipping it")
                else:
                    blend = pd.read_csv(files[0], encoding='utf-8')

                    for f in range(1, len(files)):
                        predictions = pd.read_csv(files[f], encoding='utf-8')
                        blend['is_duplicate'] += predictions['is_duplicate']

                    blend['is_duplicate'] /= len(files)
                    blend.to_csv("%s.test.csv" % clf_submission_path, encoding='utf-8', index=False)

    def blend_val_submissions(self, feature_classifiers):
        if not self.config.getboolean('blend', 'active'):
            logging.info("Not doing validation blend today")
            return

        # Get the submissions for each classifier
        for feature_config_section, classifiers in feature_classifiers.iteritems():
            for classifier_config_section, clf in classifiers.iteritems():
                # if self.config.getboolean(classifier_config_section, 'active'):
                logging.info("Blending validation predictions for classifier '%s'", classifier_config_section)

                submission_path = self.get_submission_path(feature_config_section)

                # Get the submission file name from the classifier's config section
                clf_submission_filename = self.config.get(classifier_config_section, 'submission_file')
                clf_submission_filename = os.path.splitext(clf_submission_filename)[0]
                clf_submission_path = os.path.join(submission_path, clf_submission_filename)

                val_files = glob.glob("%s.val*.csv" % clf_submission_path)

                if len(val_files) == 0:
                    print logging.warning("No submission found for this classifier, skipping it")
                else:
                    run_no = self.config.getint('cross-validation', 'runs')
                    split_no = self.config.getint('cross-validation', 'splits')

                    # Concat the validation submissions for splits, then average over the runs
                    for r in range(1, run_no + 1):
                        run_val_files = glob.glob("%s.val.run_%d*.csv" % (clf_submission_path, r))
                        run_val_predictions = pd.read_csv(run_val_files[0], encoding='utf-8')

                        for f in run_val_files[1:]:
                            run_val_predictions = pd.concat([run_val_predictions, pd.read_csv(f, encoding='utf-8')], ignore_index=True)

                        # Recover the original order for the questions.
                        run_val_predictions = run_val_predictions.sort_values('val_id')
                        print run_val_predictions.head(10)

                        if r == 1:
                            val_blend = pd.DataFrame(run_val_predictions)
                        else:
                            val_blend['is_duplicate'] += run_val_predictions['is_duplicate']

                    # Actually compute the average on the recovered data frame
                    val_blend['is_duplicate'] /= run_no
                    val_blend.to_csv("%s.val.csv" % clf_submission_path, encoding='utf-8', index=False)

    def get_feature_extractors(self):
        """ All the feature extractors we want to use. The key is used as config section. """
        feature_extractors = {
            # TODO: tfidf
            # TODO: doc2vec
            # TODO: glove
            # TODO: https://github.com/abhishekkrthakur/is_that_a_duplicate_quora_question
            'feature_manual': feature_manual.extract_features,
            #'feature_word2vec_google': feature_word2vec.extract_features,
            'feature_pos': feature_pos.extract_features,
            'feature_distance': feature_distance.extract_features,
            'feature_gram': feature_gram.extract_features,
            #'feature_stolen_from_david': feature_word_match.extract_features,
            #'feature_stolen_from_abhishekkrthakur': feature_word_match.extract_features,
        }
        return feature_extractors

    def get_classifiers_per_feature(self):
        """All the classifiers we want to use. The key is used as config section. The classifiers should be in the
        Scikit-Learn BaseEstimator format."""
        classifiers = {
            # TODO: LSTM
            # TODO: light GBM
            # TODO: deepnet
            # TODO: https://www.linkedin.com/pulse/duplicate-quora-question-abhishek-thakur
            # 'feature_manual': {
            #     'classifier_xgboost_simple': ClassifierXgboost(self.config),
            #     'classifier_ridge': Ridge(alpha=1.0, normalize=True),
            #     'classifier_lasso': Lasso(alpha=1.0, normalize=True),
            #     'classifier_svm': SVC(kernel='linear', max_iter=10000, verbose=True),
            # },
            'feature_merge': {
                'classifier_xgboost_simple': ClassifierXgboost(self.config),
                # 'classifier_ridge': Ridge(alpha=1.0, normalize=True),
                # 'classifier_lasso': Lasso(alpha=1.0, normalize=True),
                # 'classifier_svm': SVC(kernel='linear', max_iter=10000, verbose=True),
            }
            # 'feature_word2vec': {
            #
            # }
        }
        return classifiers

    def merge_features(self):
        if self.config.getboolean('merge', 'active'):
            logging.info("Merging features")
            test_split = self.config.getint('preprocessing', 'test_split')
            data_path = self.config.get('data', 'data_path')
            feature_path = os.path.join(data_path, self.config.get('data', 'feature_path'))

            # Test files
            bool_test_files = True
            list_feature_files = [feature_path + "test_feat_manual",
                                  feature_path + "test_feat_pos",
                                  feature_path + "test_feat_stolen_from_david",
                                  feature_path + "test_feat_magic",
                                  feature_path + "test_feat_magic_v2",
                                  feature_path + "test_feat_distance",
                                  feature_path + "test_feat_gram"]
            output_name = feature_path + "test_merge"
            merge_feature_files(bool_test_files, list_feature_files, test_split, output_name)

            # Training files
            bool_test_files = False
            list_feature_files = [feature_path + "train_feat_manual",
                                  feature_path + "train_feat_pos",
                                  feature_path + "train_feat_stolen_from_david",
                                  feature_path + "train_feat_magic",
                                  feature_path + "train_feat_magic_v2",
                                  feature_path + "train_feat_distance",
                                  feature_path + "train_feat_gram"]
            output_name = feature_path + "train_merge"
            merge_feature_files(bool_test_files, list_feature_files, test_split, output_name)
        else:
            logging.info("Not merging today")

    def stacking(self, feature_classifiers):
        if not self.config.getboolean('stacking', 'active'):
            logging.info("Not doing stacking today")
            return

        print "Stacking..."

        data_path = self.config.get('data', 'data_path')

        for feature_config_section, classifiers in feature_classifiers.iteritems():
            val_predictions = []
            test_predictions = []

            submissions_path = self.get_submission_path(feature_config_section)

            for classifier_config_section, clf in classifiers.iteritems():
                submission_file = self.config.get(classifier_config_section, 'submission_file')
                file_base, file_ext = submission_file.split(".", -1)

                val_submission_file = "%s.val.csv" % file_base
                val_submission_file = os.path.join(submissions_path, val_submission_file)

                test_submission_file = "%s.test.csv" % file_base
                test_submission_file = os.path.join(submissions_path, test_submission_file)

                dframe_val = pd.read_csv(val_submission_file, encoding='utf-8')
                val_predictions.append(dframe_val['is_duplicate'])

                dframe_test = pd.read_csv(test_submission_file, encoding='utf-8')
                test_predictions.append(dframe_test['is_duplicate'])

            # We're only interested in train ids in the validation set
            val_ids = dframe_val['val_id']

            # vstack add to the bottom, that's why we have to transpose
            val_stack = np.vstack(val_predictions)
            val_stack = val_stack.transpose()

            test_stack = np.vstack(test_predictions)
            test_stack = test_stack.transpose()

            # Add features to the pack. Maybe they help the model
            feature_path = os.path.join(data_path, self.config.get('data', 'feature_path'))

            val_feature_file = os.path.join(feature_path, self.config.get(feature_config_section, 'train_feature_file'))
            test_feature_file = os.path.join(feature_path, self.config.get(feature_config_section, 'test_feature_file'))

            # Only keep features for row in the validation set
            dframe_features_val = pd.read_csv("%s.csv" % val_feature_file, encoding='utf-8')
            dframe_features_val = dframe_features_val[dframe_features_val['id'].isin(val_ids)]

            # Handle the split test dataset
            if self.test_split > 1:
                files = glob.glob(os.path.join(data_path, "%s*.csv" % test_feature_file))

                dframe_features_test = pd.read_csv(files[0], encoding='utf-8')
                for i in range(1, len(files)):
                    # Load the test features
                    test_feature_file_i = files[i]
                    dframe_features_test = pd.concat([dframe_features_test, pd.read_csv(test_feature_file_i, encoding='utf-8')])
            else:
                # Load the test features
                dframe_features_test = pd.read_csv("%s.csv" % test_feature_file, encoding='utf-8')

            # We don't want the id and the header
            val_features = np.asarray(dframe_features_val.iloc[:, 1:])
            test_features = np.asarray(dframe_features_test.iloc[:, 1:])

            # hstack add to the right
            val_stack = np.hstack([val_features, val_stack])
            test_stack = np.hstack([test_features, test_stack])

            # Train XGBOOST. Only use labels for rows in the validation set
            path_train_lbls = self.config.get('preprocessing', 'train_labels_file')
            path_train_lbls = os.path.join(data_path, "%s.csv" % path_train_lbls)
            dframe_train_lbls = pd.read_csv(path_train_lbls, encoding='utf-8')
            dframe_train_lbls = dframe_train_lbls[dframe_train_lbls['id'].isin(val_ids)]

            lbls = np.asarray(list(dframe_train_lbls['is_duplicate']))

            # xgb.XGBClassifier(max_depth=300, learning_rate=0.11, silent=0, objective='binary:logistic', nthread=8)
            booster = ClassifierXgboost(self.config)
            booster.fit(val_stack, lbls)

            # Get predictions for the test stack. The second column is the 1-class (is_duplicate)
            #xgb_predictions= booster.predict_proba(test_stack)
            stack_predictions = booster.predict(test_stack)

            print "Done stacking. Creating submission file..."

            final_submission = pd.DataFrame(columns=['test_id', 'is_duplicate'])
            final_submission['test_id'] = dframe_test['test_id']
            final_submission['is_duplicate'] = stack_predictions

            final_submission.to_csv(os.path.join(submissions_path, 'final_submission.csv'), encoding='utf-8', index=False)

    def main(self):
        """The main function connects all the steps in this class, from preprocessing to submission, based on a 
        config in 'config/'. """
        logging.info("Preprocessing")
        self.preprocess()

        logging.info("Feature extraction")
        feature_extractors = self.get_feature_extractors()
        self.feature_extraction(feature_extractors)

        self.merge_features()

        feature_classifiers = self.get_classifiers_per_feature()
        if len(feature_classifiers.keys()) > 0:
            for run_id, split_id, train_ids, validation_ids in self.split():
                logging.info("Run: %d/%d, Split: %d/%d" % (run_id, self.config.getint('cross-validation', 'runs'), split_id,
                                                           self.config.getint('cross-validation', 'splits')))
                logging.info("train set length %d, validation set length %d" % (len(train_ids), len(validation_ids)))

                logging.info("Model training")
                self.model_training(run_id, split_id, train_ids, validation_ids, feature_classifiers)

                logging.info("Make submission (Prediction on test set)")
                self.make_submission(run_id, split_id, feature_classifiers)

        self.blend_submissions(feature_classifiers)
        self.blend_val_submissions(feature_classifiers)
        self.stacking(feature_classifiers)

if __name__ == "__main__":
    # Loading the config file. Add your hostname here to make it work. If you don't know your hostname, just run the
    # script once.
    config_path = "./config/"
    hostnames = {
        "int1.cartesius.surfsara.nl": 'jelle.ini',
        "mrplank-MS-B90111": 'simon.ini',
        "lumos-XPS-15-9560": 'alex.ini',
        "macco-linux": 'daniele.ini'
    }

    # Changing the config file automatically based on the hostname
    hostname = socket.gethostname()
    for candidate, config_file in hostnames.iteritems():
        if hostname == candidate:
            config_file = config_file
            break
    else:
        logging.info(
            "Your hostname is '%s', add it to the main.py config so your config is automagically loaded" % hostname)
        sys.exit()
    # Starting the main function
    quora_duplicate_end_2_end = QuoraDuplicateEnd2End(os.path.join(config_path, config_file))
    quora_duplicate_end_2_end.main()
