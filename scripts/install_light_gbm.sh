#!/usr/bin/env bash
cd ~
git clone --recursive https://github.com/Microsoft/LightGBM ; cd LightGBM
mkdir build ; cd build
cmake ..
make -j
cd ~
cd python-package
python setup.py install