import pandas as pd
import os
import logging
logger = logging.getLogger(__name__)


# TODO: move implementation form playground here
def split_data(config, run_id, split_id, random_seed=42):
    """ Quick hack to generate some data, needs replacement. """
    logger.info("Splitting data!")
    data_path = config.get('data', 'data_path')
    train_file_preprocessed = os.path.join(data_path, config.get('preprocessing', 'train_file'))

    splits = config.getint('cross-validation', 'splits')

    data_frame = pd.read_csv("%s.csv" % train_file_preprocessed, encoding='utf-8')
    indices = []
    for _ in range(splits):
        indices.append(data_frame.sample(frac=1.0/splits, random_state=random_seed).index)

    indexes = list(data_frame.index)
    val_ids = indices[split_id - 1]
    train_ids = set(indexes) - set(val_ids)
    return train_ids, val_ids