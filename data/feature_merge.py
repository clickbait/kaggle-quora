import pandas as pd
import glob
import logging
logger = logging.getLogger(__name__)


def merge_feature_files(bool_test_files, list_feature_files, test_split, output_name):
    """
    
    :param list_feature_files: 
    :param test_split:
    :param output_name: 
    :return: 
    """
    if bool_test_files:
        # Merge test files
        if test_split > 0:
            # Load the source file
            split_no = len(glob.glob("%s*.csv"% list_feature_files[0]))

            # For each split get a new file
            for i in range(0, split_no):
                new_data_frame = pd.DataFrame()

                logger.info("Merge into %s.%d.csv" % (output_name, i))
                for feature_file in list_feature_files:
                    old_data_frame = pd.read_csv("%s.%d.csv" % (feature_file, i))

                    if not len(new_data_frame) == 0:
                        old_data_frame = old_data_frame.drop(['id'], 1)

                    new_data_frame = pd.concat([new_data_frame, old_data_frame], axis=1)

                new_data_frame.to_csv("%s.%d.csv" % (output_name, i), index=False, encoding='utf-8')
        else:
            # Everything is in 1 file
            new_data_frame = pd.DataFrame()
            logger.info("Merge into %s.csv" % (output_name))

            for feature_file in list_feature_files:
                old_data_frame = pd.read_csv("%s.csv" % (feature_file))

                if not len(new_data_frame) == 0:
                    old_data_frame = old_data_frame.drop(['id'], 1)

                new_data_frame = pd.concat([new_data_frame, old_data_frame], axis=1)

            new_data_frame.to_csv("%s.csv" % (output_name), index=False, encoding='utf-8')
    else:
        # Merge training files
        new_data_frame = pd.DataFrame()
        logger.info("Merge into %s.csv" % (output_name))

        for feature_file in list_feature_files:
            old_data_frame = pd.read_csv("%s.csv" % (feature_file))

            if not len(new_data_frame) == 0:
                old_data_frame = old_data_frame.drop(['id'], 1)

            new_data_frame = pd.concat([new_data_frame, old_data_frame], axis=1)

        new_data_frame.to_csv("%s.csv" % (output_name), index=False, encoding='utf-8')
