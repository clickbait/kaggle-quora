#!/usr/bin/python
# -*- coding: utf-8 -*-
from nltk import word_tokenize
import pandas as pd
import nltk
import unicodedata
import os
import logging
from multiprocessing import Pool

logger = logging.getLogger(__name__)


def replace_in_string(question):
    # TODO: generalize (via regex)
    question = question.replace(u'₹20,000', u"20000 rupees")
    question = question.replace(u'₹1000', u"1000 rupees")
    question = question.replace(u'₹500', u"500 rupees")
    question = question.replace(u'$200', u'200 dollar')
    question = question.replace('2k17', '2017')
    question = question.replace('2k16', '2016')
    question = question.replace('2k15', '2015')

    # Parsing errors
    question = question.replace(u'\xe2\x80\x99', "'")
    question = question.replace(u'\u2019', "'")
    question = question.replace(u'\u2018', "'")
    question = question.replace(u'\xb4', "'")
    question = question.replace(u'\u201d', '"')
    question = question.replace(u'\u201c', '"')
    question = question.replace(u'\u20b9', ' rupees')
    question = question.replace(u'\xa3', ' pound ')
    question = question.replace(u'\u2026', '...')
    question = question.replace(u'\u2212', ' - ')
    question = question.replace(u'\u2013', ' - ')
    question = question.replace(u'\u26aa', ' degrees ')
    question = question.replace(u'\xd7', "*")
    question = question.replace(u'\u2229', ' intersection ')
    question = question.replace(u'\u2264', " less than or equal to ")
    question = question.replace(u'\xe2\x80\x9c', '"')
    question = question.replace(u'\xc2\xb4', "'")
    question = question.replace(u'\xe2\x80\x98', "'")
    question = question.replace(u'\xc3\xa9', "e")
    question = question.replace(u'\xc3\xa0', "a")
    question = question.replace(u'\xce\xbc', "u")  # micro
    question = question.replace(u'\x7f', "")  # delete control character
    question = question.replace(u'\xe2\x80\xa2', "")  # bullet point for enumeration
    question = question.replace(u'\xe2\x80\x8b', " ")  # zero width space
    question = question.replace(u'\xe2\x80\x93', "-")  # dash
    question = question.replace(u'\xc2\xbf', "")  # question mark turned
    question = question.replace(u'\xe2\x88\x9a', "square root ")  # square root
    question = question.replace(u'\u2205', " empty set ")
    question = question.replace(u'\xe2\x80\xa6', "...")  # ...
    question = question.replace(u'\xef\xbf\xbc', "")  # object replacement
    question = question.replace(u'\uff1f', "?")
    question = question.replace(u'\u202a', '')  # pop directional formatting
    question = question.replace(u'\u202c', '')  # pop directional formatting
    question = question.replace(u'u200e', 'left arrow')
    question = question.replace(u'u200f', 'right arrow')

    # Replace common combo's
    question = replace_word("Don't", "Do not", question)
    question = replace_word("Doesn't", "Does not", question)
    question = replace_word("Didn't", "Did not", question)
    question = replace_word("Aren't", "Are not", question)
    question = replace_word("Haven't", "Have not", question)
    question = replace_word("Hasn't", "Has not", question)
    question = replace_word("Isn't", "Is not", question)
    question = replace_word("It's", "It is", question)
    question = replace_word("I'm", "I am", question)
    question = replace_word("I'll", "I will", question)
    question = replace_word("I've", "I have", question)
    question = replace_word("You've", "You have", question)
    question = replace_word("You're", "You are", question)
    question = replace_word(" ur ", " your ", question)

    question = replace_word("We're", "We are", question)
    question = replace_word("We've", "We have", question)
    question = replace_word("Where's", "Where is", question)
    question = replace_word("Why's", "Why is", question)
    question = replace_word("What`s", "What is", question)
    question = replace_word("What's", "What is", question)
    question = replace_word("What're", "What are", question)
    question = replace_word("Wasn't", "Was not", question)
    question = replace_word("Who'll", "Who will", question)
    question = replace_word("Who's", "Who is", question)
    question = replace_word("Can't", "cannot", question)
    question = replace_word("How's", "How is", question)
    question = replace_word("Shouldn't", "Should not", question)
    question = replace_word("Wouldn't", "Would not", question)
    question = replace_word("Couldn't", "Could not", question)
    question = replace_word("There's", "There is", question)
    question = replace_word("WhAt", "what", question)

    # Added by Simon later on
    question = replace_word("don't", "do not", question)
    question = replace_word("what's", "what is", question)
    question = replace_word("let's", "let us", question)
    question = replace_word("won't", "will not", question)
    question = replace_word("i`m", "i am", question)
    question = replace_word("i'm", "i am", question)
    question = replace_word("is'nt", "is not", question)

    # Split words
    question = replace_word("toothbrushing", "tooth brushing", question)
    question = replace_word("timetravel", "time travel", question)
    question = replace_word("howmany", "how many", question)
    question = replace_word("socialtrade", "social trade", question)
    question = replace_word("howdo", "how do", question)
    question = replace_word("howto", "how to", question)
    question = replace_word("anyguidance", "any guidance", question)
    question = replace_word("ihave", "i have", question)
    question = replace_word("how-to", "how to", question)
    question = replace_word("howdifficult", "how difficult", question)
    question = replace_word("currentpolitical", "current political", question)
    question = replace_word("whatdo", "what do", question)
    question = replace_word("whatis", "what is", question)
    question = replace_word("what_are", "what are", question)
    question = replace_word("stand-up", "stand up", question)
    question = replace_word("self-improvement", "self improvement", question)
    question = replace_word("self-help", "self help", question)
    question = replace_word("ride-sharing", "ride sharing", question)
    question = replace_word("non-mexican", "non mexican", question)
    question = replace_word("non-native", "non native", question)
    question = replace_word("2-step", "two step", question)
    question = replace_word("multicolour", "multicolor", question)
    question = replace_word("remote-controlled", "remote controlled", question)
    question = replace_word("t-shirts", "shirts", question)
    question = replace_word("brexit", "great britain europa exit", question)
    question = replace_word("-year-old", "year old", question)
    question = replace_word("wwii", "world war two", question)
    question = replace_word("wwi", "world war one", question)
    question = replace_word("bnbr", "be nice be respectful", question)
    question = replace_word("dollark", " dollar shop ", question)  # it is a shop
    question = replace_word("dream11", " fanatasy cricket ", question)
    question = replace_word("rupees", " rupees ", question)  # quite a lot of rupeesnumber
    question = replace_word("midnightexpressions", 'midnight expressions', question)
    question = replace_word("nanodegree", "nano degree",
                            question)  # is a word, but the split version retains a lot of the meaning
    question = replace_word('pokecoins', "poke coins", question)
    question = replace_word('0rs', '0 rupees', question)
    question = replace_word('graffittibooks', 'graffitti books', question)
    question = replace_word('conservativespirit', 'conservative spirit', question)
    question = replace_word('limerice', 'lime rice', question)
    question = replace_word('fundsindia', 'funds india', question)
    question = replace_word('filepile', 'file pile', question)
    question = replace_word('grain', ' grain', question)  # bullet stuff
    question = replace_word(' inr ', ' indian rupee ', question)
    question = replace_word('0inr', '0 inidan rupee', question)
    question = replace_word('udemydownloader', 'downloader', question)
    question = replace_word('unifunds', 'trading company', question)
    question = replace_word('drumpf', 'trump', question)
    question = replace_word('wheystar', 'protein pills', question)
    question = replace_word('fullstack', 'full stack', question)
    question = replace_word('mypromovideos', 'my promo videos', question)
    question = replace_word('likealittle', 'like a little', question)
    question = replace_word('listenonrepeat', 'listen on repeat', question)
    question = replace_word('healthprofs', 'health professional', question)

    # Spelling corrections
    question = replace_word('redmi', 'xiaomi smartphone', question)
    question = replace_word('oneplus', 'smartphone', question)
    question = replace_word('zenfone', 'smartphone', question)
    question = replace_word('chromecast', 'google media device', question)
    question = replace_word('demonetisation', 'removal of the value', question)
    question = replace_word('demonetize', 'removing the value', question)
    question = replace_word('demonetizing', 'removing the value', question)
    question = replace_word('demonitization', 'removing the value', question)
    question = replace_word('demonetise', 'removing the value', question)
    question = replace_word('demonetising', 'removing the value', question)
    question = replace_word('demonitizing', 'removing the value', question)
    question = replace_word('rlwl', 'Remote Location waitlist', question)
    question = replace_word('macbok', 'macbook', question)
    question = replace_word('krazybutterfly', 'crazy butterfly', question)
    question = replace_word('dellhi', 'delhi', question)
    question = replace_word('permantley', 'permanently', question)
    question = replace_word('sherperd', 'shepherd', question)
    question = replace_word("cph4", "drug", question)
    question = replace_word("massachisetts", "massachusetts", question)
    question = replace_word("travelling", "traveling", question)
    question = replace_word("vapour", "vapor", question)
    question = replace_word("proavisor", "provisor", question)  # it is a name ...
    question = replace_word("favourite", "favorite", question)
    question = replace_word("previus", "previous", question)
    question = replace_word("spoliers", "spoilers", question)
    question = replace_word("benifits", "benefits", question)
    question = replace_word("benifit", "benefit", question)
    question = replace_word("analysit", "analyst", question)
    question = replace_word("enthalphy", "enthalpy", question)
    question = replace_word("diffrence", "difference", question)
    question = replace_word("similarites", "similarities", question)
    question = replace_word("intresting", "interesting", question)
    question = replace_word("distinguishin", "distinguishing", question)
    question = replace_word("diffrerence", "difference", question)
    question = replace_word("hollaween", "halloween", question)
    question = replace_word("hypotheticaly", "hypothetically", question)
    question = replace_word("nowaday", "nowadays", question)
    question = replace_word("nowadayss", "nowadays", question)
    question = replace_word("eligibilty", "eligibility", question)
    question = replace_word("eductaion", "education", question)
    question = replace_word("religon", "religion", question)
    question = replace_word("whuch", "which", question)
    question = replace_word("plzz", "please", question)
    question = replace_word("hhow", "how", question)
    question = replace_word("hiii", "hi", question)
    question = replace_word("wwhat", "what", question)
    question = replace_word("fifthgenration", "fifth generation", question)
    question = replace_word("netwark", "network", question)
    question = replace_word("diffiulties", "difficulties", question)
    question = replace_word("whirphool", "whirlpool", question)
    question = replace_word("tutuion", "tuition", question)
    question = replace_word("lecturrer", "lecturer", question)
    question = replace_word("mongolion", "mongolian", question)
    question = replace_word("doesnt", "does not", question)
    question = replace_word("compassinate", "compassionate", question)
    question = replace_word("whicch", "which", question)
    question = replace_word("liablities", "liabilities", question)
    question = replace_word("aproximately", "approximately", question)
    question = replace_word("shuold", "should", question)
    question = replace_word("dektop", "desktop", question)
    question = replace_word("formated", "formatted", question)
    question = replace_word("kwality", "quality", question)
    question = replace_word("howbto", "how to", question)
    question = replace_word("corelation", "correlation", question)
    question = replace_word("harresment", "harassment", question)
    question = replace_word("slogen", "slogan", question)
    question = replace_word("athesim", "atheism", question)
    question = replace_word("penrive", "pendrive", question)
    question = replace_word("relliance", "reliance", question)
    question = replace_word("opportunuty", "opportunity", question)
    question = replace_word("marsmellow", "marshmallow", question)
    question = replace_word("wheather", "weather", question)
    question = replace_word("parfums", "perfume", question)
    question = replace_word("travelpune", "travel pune", question)  # pune is a city in India

    question = question.replace("Ex-", "Former ")

    # Math
    question = replace_word(u'limn\xe2\x86\x92\xe2\x88\x9e', "limit to infinity", question)
    question = question.replace(u'√', "square root")
    question = question.replace('\\sqrt', ' square root of ')
    question = question.replace('\\div', ' the division of ')
    question = question.replace('\\frac', ' the fraction of ')
    question = question.replace('\\dfrac', ' the fraction of ')
    question = question.replace('\\times', ' multiplied by ')
    question = question.replace('\\cdot', ' product of ')
    question = question.replace('\\sum', ' the sum of ')
    question = question.replace('\\sin', ' sine ')
    question = question.replace('\\cos', ' cosine ')
    question = question.replace('\\tan', ' tangent ')
    question = question.replace('\\pi', ' pi ')
    question = question.replace('\\exp', ' exponent ')
    question = question.replace('\\int_', ' integral of ')
    question = question.replace('\\int', ' integral of ')
    question = question.replace('\\partial', ' partial ')
    question = question.replace('\\log', ' the logarithm of ')
    question = question.replace('\\ln', ' the natural logarithm of ')
    question = question.replace('\\lim', ' the limit of ')
    question = question.replace('\\to', ' to ')
    question = question.replace('\\mathrm', ' math ')
    question = question.replace('\\infty', ' infinity ')
    question = question.replace('\\alpha', ' alpha ')
    question = question.replace('\\beta', ' beta ')
    question = question.replace('\\gamma', ' gamma ')
    question = question.replace('\\theta', ' theta ')
    question = question.replace('\\left', ' left ')
    question = question.replace('\\right', ' right ')
    question = question.replace('\\displaystyle', ' display style ')
    question = question.replace('\\ldots', ' ... ')
    question = question.replace('\\circ', ' degrees ')
    question = question.replace('\\ce', ' chemical ')
    question = question.replace('\\sec', ' secant ')
    question = question.replace('\\mathbb', '')
    question = question.replace('==', ' is equal to ')
    question = question.replace('^', ' to the power of ')
    question = question.replace("||", ' or ')

    # Signs
    question = question.replace("'", " ' ")
    question = question.replace('*', " times ")
    question = question.replace('"', ' " ')
    question = question.replace(":", " : ")
    question = question.replace(";", " ; ")
    question = question.replace(",", " , ")
    question = question.replace("/", " / ")
    question = question.replace("?", " ? ")
    question = question.replace("!", " ! ")
    question = question.replace("#", " # ")
    question = question.replace("(", " ( ")
    question = question.replace(")", " ) ")
    question = question.replace("[", " [ ")
    question = question.replace("]", " ] ")
    question = question.replace("$", " $ ")
    question = question.replace("%", " % ")
    question = question.replace(".", " . ")
    question = question.replace("-", " - ")
    question = question.replace("=", " = ")
    question = question.replace("+", " + ")
    # TODO: add more math


    # TODO: white/yellow => white or yellow, dialogue/scene
    # TODO: phonetic translation / removal characters different alphabet

    # TODO: Clean the text, read from a file
    return question


def replace_word(old_word, new_word, text):
    """
    replaces a word in a given text, regardless of it being lower- or upper case.
    
    :param old_word: the word that is being replaced
    :param new_word: the word that is the replacy
    :param text: the text in which the replacement (might) take place
    :return: the text with possible replacement(s)
    """

    if old_word.lower() in text.lower():
        idx = 0
        while idx < len(text):
            index_l = text.lower().find(old_word.lower(), idx)
            if index_l == -1:
                return text
            text = text[:index_l] + new_word + text[index_l + len(old_word):]
            idx = index_l + len(old_word)

    return text


def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])


def replace_in_tokens(tokens):
    find = ["n't", "rs", "&"]
    replacements = ["not", "rupees", "and"]

    for key, token in enumerate(tokens):
        for search, replace in zip(find, replacements):
            if token == search:
                if replace is not None:
                    tokens[key] = replace
                else:
                    del tokens[key]
    return tokens


def tokenize(q):
    return word_tokenize(q.lower())


def create_bigram(q):
    return zip(q, q[1:])


def create_trigram(q):
    return zip(q, q[1:], q[2:])


# Preprocess csv
# def preprocessing(config, file_in, file_out, file_labels=None):
#     logger.info("Preprocessing %s and outputting to %s." % (file_in, file_out))
#
#     data_path = config.get('data', 'data_path')
#
#     # Get input path
#     full_path = os.path.join(data_path, file_in)
#
#     # Load the source file
#     data_frame = pd.read_csv(full_path, encoding='utf-8')
#
#     if 'id' not in data_frame.columns:
#         data_frame = data_frame.rename(columns={'test_id': 'id'})
#
#     # If labels should be generated
#     if file_labels is not None:
#         # Get label path
#         full_label_path = os.path.join(data_path, file_labels)
#
#         logger.info("Writing labels to %s" % full_label_path)
#         # Create a label file
#         label_frame = data_frame.copy()
#         label_frame = label_frame.drop('qid1', axis=1)
#         label_frame = label_frame.drop('qid2', axis=1)
#         label_frame = label_frame.drop('question1', axis=1)
#         label_frame = label_frame.drop('question2', axis=1)
#         label_frame.to_csv(full_label_path, index=False, encoding='utf-8')
#
#     stemmer = config.get('preprocessing', 'stemmer')
#
#     stemmers = {
#         'porter': nltk.PorterStemmer(),
#         'lancaster': nltk.LancasterStemmer(),
#         'snowball': nltk.SnowballStemmer('english')
#     }
#
#     def stem(tokens):
#         return map(stemmers[stemmer].stem, tokens)
#
#     # Fill empty values
#     data_frame = data_frame.fillna("")
#     if file_labels is not None:
#         data_frame = data_frame.drop('is_duplicate', axis=1)
#
#         # New ID's
#         data_frame = data_frame.drop('qid1', axis=1)
#         data_frame = data_frame.drop('qid2', axis=1)
#         keys = list(set(data_frame['question1']).union(set(data_frame['question2'])))
#
#         questions = {}
#         id = 1
#         for item in keys:
#             questions[item] = id
#             id += 1
#
#         data_frame['qid1'] = data_frame['question1'].apply(lambda x: questions[x])
#         data_frame['qid2'] = data_frame['question2'].apply(lambda x: questions[x])
#
#     # Unigrams
#     data_frame['unigram1'] = data_frame['question1'].apply(replace_in_string).apply(tokenize).apply(
#         replace_in_tokens).apply(stem)
#     data_frame['unigram2'] = data_frame['question2'].apply(replace_in_string).apply(tokenize).apply(
#         replace_in_tokens).apply(stem)
#
#     # Save new frame
#     full_path_preprocessed = os.path.join(data_path, file_out)
#
#     data_frame.to_csv(full_path_preprocessed, index=False, encoding='utf-8')

def preprocessing(config, data_frame, full_path_preprocessed, file_labels=None):
    if not 'id' in data_frame.columns:
        data_frame_in = data_frame.rename(columns={'test_id': 'id'})

    # If labels should be generated
    if file_labels is not None:
        # Create a label file
        logging.info("Writing labels to %s" % file_labels)

        label_frame = data_frame.copy()
        label_frame = label_frame.drop('qid1', axis=1)
        label_frame = label_frame.drop('qid2', axis=1)
        label_frame = label_frame.drop('question1', axis=1)
        label_frame = label_frame.drop('question2', axis=1)
        label_frame.to_csv(file_labels, index=False, encoding='utf-8')

    stemmer = config.get('preprocessing', 'stemmer')

    pool = Pool(processes=config.getint('multiprocessing', 'cpu_cores'))

    stemmers = {
        'porter': nltk.PorterStemmer(),
        'lancaster': nltk.LancasterStemmer(),
        'snowball': nltk.SnowballStemmer('english')
    }

    def stem(tokens):
        return map(stemmers[stemmer].stem, tokens)

    # Fill empty values
    data_frame = data_frame.fillna("")
    if file_labels is not None:
        data_frame = data_frame.drop('is_duplicate', axis=1)

        # New ID's
        data_frame = data_frame.drop('qid1', axis=1)
        data_frame = data_frame.drop('qid2', axis=1)
        keys = list(set(data_frame['question1']).union(set(data_frame['question2'])))

        logger.info("Creating new question indexes")

        questions = {}
        id = 1
        for item in keys:
            questions[item] = id
            id += 1

        data_frame['qid1'] = data_frame['question1'].apply(lambda x: questions[x])
        data_frame['qid2'] = data_frame['question2'].apply(lambda x: questions[x])

    # Unigrams
    data_frame['unigram1'] = data_frame['question1']
    logger.info("Replace in string q1")
    data_frame['unigram1'] = pool.map(replace_in_string, data_frame['unigram1'])
    logger.info("Tokenize q1")
    data_frame['unigram1'] = pool.map(tokenize, data_frame['unigram1'])
    logger.info("Replace in tokens q1")
    data_frame['unigram1'] = pool.map(replace_in_tokens, data_frame['unigram1'])
    # logger.info("Stemming q1")
    # ['unigram1'] = pool.map(lambda x: stem(x), data_frame['unigram1'])

    data_frame['unigram2'] = data_frame['question2']
    logger.info("Replace in string q2")
    data_frame['unigram2'] = pool.map(replace_in_string, data_frame['unigram2'])
    logger.info("Tokenize q2")
    data_frame['unigram2'] = pool.map(tokenize, data_frame['unigram2'])
    logger.info("Replace in tokens q2")
    data_frame['unigram2'] = pool.map(replace_in_tokens, data_frame['unigram2'])
    # logger.info("Stemming q2")
    # data_frame['unigram2'] = pool.map(lambda x: stem(x), data_frame['unigram2'])

    logger.info("Saving preprocessed file to %s", full_path_preprocessed)
    data_frame.to_csv(full_path_preprocessed, index=False, encoding='utf-8')
    del data_frame

    # # Bigrams
    # bigram_path = full_path_preprocessed.replace("../kaggle-quora-data/", "../kaggle-quora-data/bigrams_")
    # bigrams_frame = pd.DataFrame({'id': data_frame.iloc[:, 0]})
    #
    # logger.info("Create bigram1")
    # bigrams_frame['bigram1'] = pool.map(create_bigram, data_frame['unigram1'])
    #
    # logger.info("Create bigram2")
    # bigrams_frame['bigram2'] = pool.map(create_bigram, data_frame['unigram2'])
    # bigrams_frame.to_csv(bigram_path, index=False, encoding='utf-8')
    # del bigrams_frame
    #
    # # Trigrams
    # trigram_path = full_path_preprocessed.replace("../kaggle-quora-data/", "../kaggle-quora-data/trigrams_")
    # trigrams_frame = pd.DataFrame({'id': data_frame.iloc[:, 0]})
    #
    # logger.info("Create trigram1")
    # trigrams_frame['trigram1'] = pool.map(create_trigram, data_frame['unigram1'])
    #
    # logger.info("Create trigram2")
    # trigrams_frame['trigram2'] = pool.map(create_trigram, data_frame['unigram2'])
    # trigrams_frame.to_csv(trigram_path, index=False, encoding='utf-8')
    # del trigrams_frame

    pool.close()
