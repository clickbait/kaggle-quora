import pandas as pd
import numpy as np
import seaborn as sns
import sys
import os

def getvalidationset(traindf):
    """Generates a possible validation set from unique questions, which hopefully ensures validation set independence. Afterwards we can draw multiple validation sets from this bag of possible val samples
    
    
    TO DO:
    
    Ensure class balance
    Maybe just return indices of val rows, to integrate it better in the pipeline
    
    Input:
    Total training data - Dataframe
    
    Output:
    Dataframe with validation rows
    """
    
    #Count value occurences
    q1 = traindf['qid1'].value_counts()
    q2 = traindf['qid2'].value_counts()

    #Take question ids with count 1 (uniques)
    q1uniq = q1.loc[q1 == 1]
    q2uniq = q2.loc[q2 == 1]
    q1index = list(q1uniq.index)
    q2index = list(q2uniq.index)
    #Get the unique question ids from original data frame
    q1df = traindf.loc[traindf['qid1'].isin(q1index)]
    q2df = traindf.loc[traindf['qid2'].isin(q2index)]
    #Drop duplicates
    #print q1df
    valsetdup = pd.concat([q1df, q2df]).drop_duplicates()
    #print valsetdup
    return valsetdup

def split_data(config, totalfolds):
    """ First version of a fold-based validation split function
    Call this with a number of folds, returns lists of lists containing the train and val sample indexes.

    TO-DO
    KILL THIS CODE WITH FIRE

    INPUT:
    totalfolds - total number of folds
    

    OUTPUT:
    list (len of totalfolds) of trainset indexes (original indexes)
    list (len of totalfolds) of valset indexes (orignal indexes)


    """
    #logger.info("Splitting data!")
    data_path = config.get('data', 'data_path')
    train_file_preprocessed = os.path.join(data_path, config.get('preprocessing', 'train_file'))

    traindf = pd.read_csv("%s.csv" % train_file_preprocessed, encoding='utf-8')

    # Read train csv
    #traindf = pd.read_csv('../../kaggle-quora-data/train.csv')
    
    # Shuffle and reset indexes; keep original indexes in 'index' column
    traindf = traindf.iloc[np.random.permutation(len(traindf))]
    #print traindf
    traindf = traindf.reset_index()
    traindf = traindf.rename(columns={'index': 'oldindex'})
    #print traindf
    datasetsize = traindf.shape[0]

    print "Total dataset size: ", datasetsize

    validationsetdf = getvalidationset(traindf)
    valsettotalsize = validationsetdf.shape[0]
    valsetsize = validationsetdf.shape[0]/totalfolds

    print "Possible validation samples: ", valsettotalsize
    print "Validation samples per fold: ", valsetsize

    fullindex = list(traindf.index)
    valsetlist = list(validationsetdf['oldindex'])
    fulltrainsets = []
    fullvalsets = []
    print "Preparing %s-fold cross-validation sets" %totalfolds
    for fold in range(1,totalfolds+1):
        
        valsetfoldlist = valsetlist[(fold*valsetsize-valsetsize):(fold*valsetsize)]
        print "Fold %s" %fold, "Validation samples", fold*valsetsize-valsetsize, "-",fold*valsetsize
#         print valsetfoldlist
        valsetfold = traindf.loc[traindf['oldindex'].isin(valsetfoldlist)]
        #print valsetfold[0:2]
        #print fullindex[0:10]
        trainindexes = set(fullindex) - set(valsetfold.index)
        #print valsetfold.index[0:10]
        #print list(trainindexes)[0:10]
        trainsetfold = traindf.ix[trainindexes]
        fulltrainsets.append(list(trainsetfold['oldindex']))
        fullvalsets.append(list(valsetfold['oldindex']))

    print len(fulltrainsets),"Training sets prepared","-", len(fullvalsets), "Validation sets prepared"

    return fulltrainsets, fullvalsets


