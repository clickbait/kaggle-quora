import pandas as pd
from collections import Counter

frame = pd.read_csv("../../../kaggle-quora-data/test_preprocessed.csv", encoding='utf-8').fillna("[]")

words = []
for key,row in frame['unigram1'].iteritems():
    row_words = row[1:-1].split(',')
    row_words = map(unicode, row_words)
    row_words = map(unicode.strip, row_words)
    words += row_words

count = Counter(words)
for word, occurence in count.most_common(5000):
    print "%s, %d" % (word, occurence)
