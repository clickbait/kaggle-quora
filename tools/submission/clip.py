import pandas as pd

clip = 0.975
classes = 2


def clip_csv(csv_file, clip, classes):
    # Read the submission file
    df2 = pd.read_csv(csv_file, index_col=0)
    df2.index = df2.index.astype(int)
    df2.to_csv("tmp_" + csv_file, index=False)

    df = df2#pd.read_csv("tmp_" + csv_file, index_col=0)
    print df.columns
    # Clip the values
    # df['is_duplicate'] = df['is_duplicate'].multiply(0.75)
    df['is_duplicate'] = df['is_duplicate'].clip(lower=(1.0 - clip) / float(classes - 1), upper=clip)
    # Normalize the values to 1
    # df = df.div(df.sum(axis=1), axis=0)

    # Save the new clipped values
    df.to_csv('clip.csv')
    print(df.head(10))


# Of course you are going to use your own submission here
clip_csv('blend.csv', clip, classes)