## Errors found (and hopefully solved)

## 1.
 ### Problem
 XGBoost cant find the DMatrix
### Solution
Reinstall and add the correct path, see the installation guide on their website, http://xgboost.readthedocs.io/en/latest/build.html, especially the python package.

## 2. 
### Problem
`IOError: File ../kaggle-quora-data/train_preprocessed.csv.csv does not exist`
### Solution
Go to your settings.ini file and set preprocessing > active to True

## 3. 
### Problem
`DeprecationWarning: This module was deprecated in version 0.18 in favor of the model_selection module into which all the refactored classes and functions are moved. Also note that the interface of the new CV iterators are different from that of this module. This module will be removed in 0.20.
  "This module will be removed in 0.20.", DeprecationWarning)`
### Solution
  No solution yet
  
## 4.
### Problem
`UnicodeDecodeError: 'ascii' codec can't decode byte 0xe2 in position 0: ordinal not in range(128)`

### Solution
There are some strings defined as ascii while they should be unicode. Example `str = '\x01\x10'` should be `str = u'\x01\x10'` to cast it to unicode.
 
## 5.
### Problem
`PicklingError: Can't pickle <type 'function'>: attribute lookup __builtin__.function failed`

### Solution
The function called cannot be pickled, hence cannot be used in parallel.
More solutions here: http://stackoverflow.com/questions/8804830/python-multiprocessing-pickling-error

## 6.
### Problem
`IndexError: string index out of range`
### Solution
The pos tagger expect a list with no empty values. Removing them fixes the issue.


## 7.
### Problem
The dataframe shows an integer while it should have a float.

### Solution
Stop using df.set_value, it has a weird bug with floats.