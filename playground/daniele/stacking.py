import pandas as pd
import numpy as np
import glob
import os
import random
import csv

from ConfigParser import SafeConfigParser
from sklearn.model_selection import train_test_split

from data.dataset_splitter import split_data
from classification.xgboost_simple import ClassifierXgboost

config = SafeConfigParser()
config.read('../../config/daniele.ini')

train_path = '/data/mlip/kaggle-quora-data/train.csv'
submission_path = '/home/macco/mlip/kaggle-quora/results/asd/'

nfolds = 3

"""
:param train_files_pred 
    a list of file names, one for each classifier, containing the predictions for the training set
:param train_files_feat 
    the name of the file containing the features for the training set
:param test_files_pred
    a list of file names, one for each classifier, containing the predictions for the test set
:param test_files_feat
    a list of file names, one for each split, containing the features for the test set
:param fold_no 
    the number of folds to use for k-fold
"""
def stacking(train_files_pred, train_file_feat, test_files_pred, test_files_feat, nfolds=3):
    print "Stacking..."

    train_lbls = pd.read_csv(train_path, encoding='utf-8')['is_duplicate'].values

    # if len(test_files_pred) != len(test_files_feat):
    #     raise ValueError("<test_files_pred> and <test_files_feat> should have the same length, but are different: %d, %d"
    #                      % (len(test_files_pred), len(test_files_feat)))

    # 1. Read the predicted probabilities for the training set
    train_proba = []
    train_feat = []
    for f in train_files_pred:
        train_proba_df = pd.read_csv(f, encoding='utf-8')
        train_proba.append(np.asarray(train_proba_df['is_duplicate']))

        del train_proba_df

    train_feat_df = pd.read_csv(train_file_feat, encoding='utf-8')
    train_feat.append(np.asarray(train_feat_df.iloc[:, 1:]))

    del train_feat_df

    # for feat in train_files_feat:
    #     train_feat_df = pd.read_csv(feat, encoding='utf-8')
    #     train_feat.append(np.asarray(train_feat_df.iloc[:, 1:]))
    #
    #     del train_feat_df

    # vstack add to the bottom, that's why we have to transpose (we're dealing with row arrays)
    train_pred_stack = np.vstack(train_proba)
    train_pred_stack = train_pred_stack.transpose()
    train_feat_stack = np.hstack(train_feat)

    train_stack = np.hstack([train_pred_stack, train_feat_stack])

    del train_proba, train_feat, train_pred_stack, train_feat_stack

    # Train a meta-classifier for a number of folds
    meta_clf = ClassifierXgboost(config)
    for fold in range(0, nfolds):
        fold_dir = os.path.join(submission_path, 'fold%d' % fold)
        if not os.path.isdir(fold_dir):
            os.mkdir(fold_dir)

        x_train, x_valid, y_train, y_valid = train_test_split(train_stack, train_lbls, test_size=0.2, random_state=4242 + fold)
        meta_clf.fit(x_train, y_train, x_valid, y_valid)

        # 2. Read the predicted probabilities for the test set. Take care of the split
        test_proba = []
        test_feat = []
        start = 0

        # 3. Get the features for each split
        for i in range(0, len(test_files_feat)):
            test_feat_df = pd.read_csv(test_files_feat[i], encoding='utf-8')
            test_feat.append(np.asarray(test_feat_df.iloc[:, 1:]))

            test_ids = test_feat_df['id']
            del test_feat_df

            # read proba 'till the end of the feat split, and append to the previously read predictions
            end = start + len(test_feat[0])

            # Get predictions for each classifier and stack them together. Only retrieve rows in the feat set
            for clf in test_files_pred:
                test_proba_df = pd.read_csv(clf, encoding='utf-8')
                test_proba.append(np.asarray(test_proba_df['is_duplicate'][start:end]))

                del test_proba_df

            test_pred_stack = np.vstack(test_proba)
            test_pred_stack = test_pred_stack.transpose()
            test_feat_stack = np.hstack(test_feat)

            test_stack = np.hstack([test_pred_stack, test_feat_stack])

            del test_pred_stack, test_feat_stack

            print "Done stacking. Creating submission file...[fold %d - split %d]" % (fold, i)

            test_predictions = meta_clf.predict(test_stack)
            final_submission_split = pd.DataFrame(columns=['test_id', 'is_duplicate'])
            final_submission_split['test_id'] = test_ids
            final_submission_split['is_duplicate'] = test_predictions

            final_submission_split.to_csv(
                os.path.join(fold_dir, 'final_submission_split_%d_fold_%d.csv' % (i, fold)), encoding='utf-8', index=False)

            start = end # go on with the next split
            test_proba = []
            test_feat = []

        del test_proba, test_feat

def blend_folds(nfolds):
    stacked_df = concat_predictions(os.path.join(submission_path, 'fold0'))
    for fold in range(1, nfolds):
        fold_dir = os.path.join(submission_path, 'fold%d' % fold)
        stacked_df['is_duplicate'] += concat_predictions(fold_dir)['is_duplicate']

    stacked_df['is_duplicate'] /= nfolds
    stacked_df.to_csv(os.path.join(submission_path, 'stacked_submission.csv'), encoding='utf-8', index=False)

def concat_predictions(dir):
    concat_submission = pd.DataFrame(columns=['test_id', 'is_duplicate'])

    files = sorted(glob.glob(os.path.join(dir, 'final_submission*')))
    for f in files:
        concat_submission = concat_submission.append(pd.read_csv(f, encoding='utf-8'))

    # final_submission.to_csv(os.path.join(submission_path, 'concat_submission.csv'), encoding='utf-8', index=False)
    # return 'concat_submission.csv'
    return concat_submission

def get_train_files():
    # Generate some random predictions
    # train_files_pred = []
    #
    # df = pd.DataFrame(columns=['id', 'is_duplicate'])
    # df['id'] = range(0, 404290)
    # df['is_duplicate'] = [random.random() for _ in range(404290)]
    #
    # file_path = os.path.join(submission_path, 'clf1.train_pred.csv')
    # df.to_csv(file_path, encoding='utf-8', index=False)
    #
    # train_files_pred.append(file_path)
    # train_files_pred.append(os.path.join(submission_path, 'train_pred_conv5.csv'))

    # df['is_duplicate'] = [random.randint(0, 1) for _ in range(100)]
    # file_path = os.path.join(submission_path, 'clf2.train_pred.csv')
    # df.to_csv(file_path, encoding='utf-8', index=False)
    # train_files_pred.append(file_path)
    #
    # df['is_duplicate'] = [0.2 for _ in range(100)]
    # file_path = os.path.join(submission_path, 'clf3.train_pred.csv')
    # df.to_csv(file_path, encoding='utf-8', index=False)
    # train_files_pred.append(file_path)
    #
    # # Generate some random features
    # df = pd.DataFrame(columns=['id', 'feat1', 'feat2', 'feat3'])
    # df['id'] = range(0, 100)
    # df['feat1'] = [random.random() for _ in range(100)]
    # df['feat2'] = [random.random() for _ in range(100)]
    # df['feat3'] = [random.random() for _ in range(100)]
    #
    # train_feat_file = os.path.join(submission_path, 'train_feat.csv')
    # df.to_csv(train_feat_file, encoding='utf-8', index=False)

    # return train_files_pred, os.path.join(submission_path, 'train_feat_manual.csv')
    train_preds = map(lambda x: os.path.join(submission_path, x), ['1D_conv_0.1650_train_pred.csv', 'lstm_0.1739_train_pred.csv'])
    train_feat = os.path.join(submission_path, 'train_merge_feat.csv')

    return train_preds, train_feat

def get_test_files():
    # Generate some random predictions
    # test_files_pred = []
    #
    # df = pd.DataFrame(columns=['id', 'is_duplicate'])
    # df['test_id'] = range(0, 2345796)
    # df['is_duplicate'] = [random.random() for _ in range(2345796)]
    #
    # file_path = os.path.join(submission_path, 'clf1.test_pred.csv')
    # df.to_csv(file_path, encoding='utf-8', index=False)
    #
    # test_files_pred.append(file_path)
    # test_files_pred.append(os.path.join(submission_path, 'test_pred_conv.csv'))
    #
    # df['is_duplicate'] = [random.randint(0, 1) for _ in range(500)]
    # file_path = os.path.join(submission_path, 'clf2.test_pred.csv')
    # df.to_csv(file_path, encoding='utf-8', index=False)
    # test_files_pred.append(file_path)
    #
    # df['is_duplicate'] = [0.3 for _ in range(500)]
    # file_path = os.path.join(submission_path, 'clf3.test_pred.csv')
    # df.to_csv(file_path, encoding='utf-8', index=False)
    # test_files_pred.append(file_path)
    #
    # test_files_feat = []
    # # Generate some random features
    # for i in range(5):
    #     df = pd.DataFrame(columns=['test_id', 'feat1', 'feat2', 'feat3'])
    #     df['test_id'] = range(i*100, i*100 + 100)
    #     df['feat1'] = [random.random() for _ in range(100)]
    #     df['feat2'] = [random.random() for _ in range(100)]
    #     df['feat3'] = [random.random() for _ in range(100)]
    #
    #     file_path = os.path.join(submission_path, 'test_feat.%d.csv' % i)
    #     df.to_csv(file_path, encoding='utf-8', index=False)
    #
    #     test_files_feat.append(file_path)

    # return test_files_pred, test_files_feat
    # return test_files_pred, \
    #        [os.path.join(submission_path, 'test_feat_manual.0.csv'),
    #         os.path.join(submission_path, 'test_feat_manual.1.csv'),
    #         os.path.join(submission_path, 'test_feat_manual.2.csv'),
    #         os.path.join(submission_path, 'test_feat_manual.3.csv'),
    #         os.path.join(submission_path, 'test_feat_manual.4.csv')]
    test_preds = map(lambda x: os.path.join(submission_path, x), ['1D_conv_0.1650_test.csv', 'lstm_0.1739_test.csv'])
    test_feats = map(lambda x: os.path.join(submission_path, x), ['test_merge_feat.0.csv',
                  'test_merge_feat.1.csv',
                  'test_merge_feat.2.csv',
                  'test_merge_feat.3.csv',
                  'test_merge_feat.4.csv'])

    return test_preds, test_feats

def main():
    # Generate some random predictions for a bunch of classifiers
    train_preds, train_feat = get_train_files()
    test_preds, test_feats = get_test_files()

    stacking(train_preds, train_feat, test_preds, test_feats, nfolds)
    blend_folds(nfolds)

if __name__ == '__main__':
    main()