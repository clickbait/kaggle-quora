import pandas as pd
import numpy as np
import seaborn as sns

def validationset(traindf, size):
    """Generates a validation set from unique questions, which hopefully ensures validation set independence
    
    
    TO DO:
    
    Ensure class balance
    Maybe just return indices of val rows, to integrate it better in the pipeline
    
    Input:
    Size: Number of validation samples needed
    
    Output:
    Dataframe with validation rows
    """
    
    #Count value occurences
    q1 = traindf['qid1'].value_counts()
    q2 = traindf['qid2'].value_counts()

    #Take question ids with count 1 (uniques)
    q1uniq = q1.loc[q1 == 1]
    q2uniq = q2.loc[q2 == 1]
    q1index = list(q1uniq.index)
    q2index = list(q2uniq.index)
    #Get the unique question ids from original data frame
    q1df = traindf.loc[traindf['qid1'].isin(q1index)]
    q2df = traindf.loc[traindf['qid2'].isin(q2index)]
    #Drop duplicates
    valsetdup = pd.concat([q1df, q2df]).drop_duplicates()
    
    return valsetdup.sample(size)


def generatesplit(ratio):
    """"Generates a training and validation split
    
    
    INPUT:
    
    Validation/training set ratio (max: ~ 0.7, sensible value: 0.2)
    
    OUTPUT:
    
    Training set
    Validation set 
    """
    #Read train csv
    traindf = pd.read_csv('kaggle-quora-data/train.csv')
    datasetsize = traindf.shape[0]
    validationsetsize = datasetsize*ratio
    
    #Call validationset function
    validationsetdf = validationset(traindf,validationsetsize)
    valsetlist = list(validationsetdf['id'])

    
    # Construct training and validation dataframes
    indexes = list(traindf.index)
    
    # Construction of two exclusive datasets
    valset = traindf.ix[valsetlist]
    trainindexes = set(indexes) - set(valsetlist)
    valset = valset.sort_values(by='id')
    trainingset = traindf.ix[trainindexes]
    
    return trainingset, valset
    

