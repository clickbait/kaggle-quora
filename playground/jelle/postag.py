import nltk
import numpy as np
from nltk.tokenize import word_tokenize

from collections import Counter

#Because I am crazy
tag_dictionary = {'VBG': '29', 'FW': '5', 'VBN': '30', ',': '40', 'VBP': '31', 'WDT': '33', 'JJ': '7', 'WP': '35', 'VBZ': '32', 'DT': '3', 'PP': '19', '"': '48', 'RP': '23', '$': '38', 'NN': '12', ')': '43', '(': '42', 'VBD': '28', 'POS': '17', '.': '39', 'TO': '25', 'PRP': '18', 'RB': '20', ':': '41', 'NNS': '13', 'NNP': '14', '#': '37', 'VB': '27', 'WRB': '36', '`': '45', 'CC': '1', 'LS': '10', 'PDT': '16', 'RBS': '22', 'RBR': '21', 'CD': '2', 'EX': '4', 'IN': '6', 'MD': '11', 'NNPS': '15', "'": '47', 'JJS': '9', 'JJR': '8', 'SYM': '24', 'UH': '26'}
text = word_tokenize("And now for something completely different") 

def count_wordtypes(tokenized_text):
    text_tags = [k[1] for k in nltk.pos_tag(tokenized_text)]
    count_array = np.zeros(48)
    counts = Counter(text_tags)
    for wordtype in counts:
        count_array[int(tag_dictionary[wordtype])-1] = int(counts[wordtype])
    return count_array

def encode_wordtypes(tokenized_text):
    text_tagged = nltk.pos_tag(tokenized_text)
    sen_length = len(text_tagged)
    encode_array = np.zeros((48,sen_length))
    for i in range(0,sen_length):
        print i
        print int(tag_dictionary[text_tagged[i][1]])
        encode_array[int(tag_dictionary[text_tagged[i][1]])-1][i] = 1
    return encode_array
    
        
#print count_wordtypes(text)
#print encode_wordtypes(text)   
 
