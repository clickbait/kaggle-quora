from sklearn.linear_model import LogisticRegression as LR
import pandas as pd
# import seaborn as sns
# from scipy import stats

train_path = './playground/roeland/predictions_XGB_leaky.train_run_0.csv'
test_path = './playground/roeland/predictions_XGB_leaky.test_run_0.csv'

p_train = pd.read_csv(train_path , encoding='utf-8')['is_duplicate'].values
p_test = pd.read_csv(test_path, encoding='utf-8')['is_duplicate'].values
y_train = pd.read_csv("./playground/roeland/y_train_prep.csv")
y_train = y_train['is_duplicate'].values
lr = LR()
lr.fit( p_train.reshape( -1, 1 ), y_train )     # LR needs X to be 2-dimensional
p_calibrated = lr.predict_proba(p_test.reshape( -1, 1 ))[:,1]

cal = pd.DataFrame()
y_test_ids = pd.read_csv(test_path , encoding='utf-8')['test_id'].values

cal['test_id'] = y_test_ids
cal['is_duplicate'] = p_calibrated


cal.to_csv('calibratated_'+test_path, index=False)

# x = p_test
# y = p_calibrated
# sns.distplot(x, kde=False, fit=stats.gamma);
# sns.distplot(y, kde=False);
