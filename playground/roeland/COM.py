import os
import glob
import gensim
import shutil
import smart_open
import pandas as pd
from multiprocessing import Pool

from collections import Counter


def glove2word2vec(input_path, output_path):
    """
    Converts a glove file into a word2vec file
    
    :param input_path: path to the glove file
    :param output_path: path to the output file
    :return: nothing, but we create the new file
    """

    with smart_open.smart_open(input_path, 'r') as f:
        num_lines = sum(1 for line in f)
    with smart_open.smart_open(input_path, 'r') as f:
        num_dims = len(f.readline().split()) - 1

    header_line = "{} {}".format(num_lines, num_dims)

    with open(input_path, 'r') as old:
        with open(output_path, 'w') as new:
            new.write(str(header_line) + "\n")
            shutil.copyfileobj(old, new)


def replace_in_string(question):
    # Parsing errors
    question = question.replace("\xe2\x80\x99", "'")
    question = question.replace("\xe2\x80\x9c", '"')
    question = question.replace("\xc2\xb4", "'")
    question = question.replace("\xe2\x80\x98", "'")
    question = question.replace("\xc3\xa9", "e")
    question = question.replace("\xc3\xa0", "a")
    question = question.replace("\xce\xbc", "u") # micro
    question = question.replace("\x7f", "") # delete control character
    question = question.replace("\xe2\x80\xa2", "") # bullet point for enumeration
    question = question.replace("\xe2\x80\x8b", " ") # zero width space
    question = question.replace("\xe2\x80\x93", "-") # dash
    question = question.replace("\xc2\xbf", "") # question mark turned
    question = question.replace("\xe2\x88\x9a", "square root ") # square root
    question = question.replace("\xe2\x80\xa6", "...") # ...
    question = question.replace("\xef\xbf\xbc", "") # object replacement

    # Replace common combo's
    question = replace_word("Don't", "Do not", question)
    question = replace_word("Doesn't", "Does not", question)
    question = replace_word("Didn't", "Did not", question)
    question = replace_word("Aren't", "Are not", question)
    question = replace_word("Haven't", "Have not", question)
    question = replace_word("Hasn't", "Has not", question)
    question = replace_word("Isn't", "Is not", question)
    question = replace_word("It's", "It is", question)
    question = replace_word("I'm", "I am", question)
    question = replace_word("I'll", "I will", question)
    question = replace_word("I've", "I have", question)
    question = replace_word("You've", "You have", question)
    question = replace_word("You're", "You are", question)
    question = replace_word("We're", "We are", question)
    question = replace_word("We've", "We have", question)
    question = replace_word("Where's", "Where is", question)
    question = replace_word("Why's", "Why is", question)
    question = replace_word("What`s", "What is", question)
    question = replace_word("What's", "What is", question)
    question = replace_word("What're", "What are", question)
    question = replace_word("Wasn't", "Was not", question)
    question = replace_word("Who'll", "Who will", question)
    question = replace_word("Who's", "Who is", question)
    question = replace_word("Can't", "cannot", question)
    question = replace_word("How's", "How is", question)
    question = replace_word("Shouldn't", "Should not", question)
    question = replace_word("Wouldn't", "Would not", question)
    question = replace_word("Couldn't", "Could not", question)
    question = replace_word("There's", "There is", question)
    question = replace_word("WhAt", "what", question)


    # Added by Simon later on
    question = replace_word("don't", "do not", question) # Comment Simon: precompile regexes for better performance
    question = replace_word("what's", "what is", question)
    question = replace_word("let's", "let us", question)
    question = replace_word("won't", "will not", question)
    question = replace_word("i`m", "i am", question)
    question = replace_word("i'm", "i am", question)
    question = replace_word("is'nt", "is not", question)

    # Split words
    question = replace_word("toothbrushing", "tooth brushing", question)
    question = replace_word("timetravel", "time travel", question)
    question = replace_word("howmany", "how many", question)
    question = replace_word("howdo", "how do", question)
    question = replace_word("howto", "how to", question)
    question = replace_word("anyguidance", "any guidance", question)
    question = replace_word("ihave", "i have", question)
    question = replace_word("how-to", "how to", question)
    question = replace_word("howdifficult", "how difficult", question)
    question = replace_word("currentpolitical", "current political", question)
    question = replace_word("whatdo", "what do", question)
    question = replace_word("whatis", "what is", question)
    question = replace_word("what_are", "what are", question)
    question = replace_word("stand-up", "stand up", question)
    question = replace_word("self-improvement", "self improvement", question)
    question = replace_word("self-help", "self help", question)
    question = replace_word("ride-sharing", "ride sharing", question)
    question = replace_word("non-mexican", "non mexican", question)
    question = replace_word("non-native", "non native", question)
    question = replace_word("2-step", "two step", question)
    question = replace_word("multicolour", "multicolor", question)
    question = replace_word("remote-controlled", "remote controlled", question)
    question = replace_word("t-shirts", "shirts", question)
    question = replace_word("brexit", "great britain europa exit", question)

    # Spelling corrections
    question = replace_word("spoliers", "spoilers", question)
    question = replace_word("benifits", "benefits", question)
    question = replace_word("benifit", "benefit", question)
    question = replace_word("analysit", "analyst", question)
    question = replace_word("enthalphy", "enthalpy", question)
    question = replace_word("diffrence", "difference", question)
    question = replace_word("similarites", "similarities", question)
    question = replace_word("intresting", "interesting", question)
    question = replace_word("distinguishin", "distinguishing", question)
    question = replace_word("diffrerence", "difference", question)
    question = replace_word("hollaween", "halloween", question)
    question = replace_word("hypotheticaly", "hypothetically", question)
    question = replace_word("nowaday", "nowadays", question)
    question = replace_word("nowadayss", "nowadays", question)
    question = replace_word("eligibilty", "eligibility", question)
    question = replace_word("eductaion", "education", question)
    question = replace_word("religon", "religion", question)
    question = replace_word("whuch", "which", question)
    question = replace_word("plzz", "please", question)
    question = replace_word("hhow", "how", question)
    question = replace_word("hiii", "hi", question)
    question = replace_word("wwhat", "what", question)
    question = replace_word("fifthgenration", "fifth generation", question)
    question = replace_word("netwark", "network", question)
    question = replace_word("diffiulties", "difficulties", question)
    question = replace_word("whirphool", "whirlpool", question)
    question = replace_word("tutuion", "tuition", question)
    question = replace_word("lecturrer", "lecturer", question)
    question = replace_word("mongolion", "mongolian", question)
    question = replace_word("doesnt", "does not", question)
    question = replace_word("compassinate", "compassionate", question)
    question = replace_word("whicch", "which", question)
    question = replace_word("liablities", "liabilities", question)
    question = replace_word("aproximately", "approximately", question)
    question = replace_word("shuold", "should", question)
    question = replace_word("dektop", "desktop", question)
    question = replace_word("formated", "formatted", question)
    question = replace_word("kwality", "quality", question)
    question = replace_word("howbto", "how to", question)
    question = replace_word("corelation", "correlation", question)
    question = replace_word("harresment", "harassment", question)
    question = replace_word("slogen", "slogan", question)
    question = replace_word("athesim", "atheism", question)
    question = replace_word("penrive", "pendrive", question)
    question = replace_word("relliance", "reliance", question)
    question = replace_word("opportunuty", "opportunity", question)
    question = replace_word("marsmellow", "marshmallow", question)
    question = replace_word("wheather", "weather", question)
    question = replace_word("parfums", "perfume", question)
    question = replace_word("travelpune", "travel pune", question) #pune is a city in India

    question = question.replace("Ex-", "Former ")

    # Math
    question = replace_word("limn\xe2\x86\x92\xe2\x88\x9e", "limit to infinity", question)

    # Signs
    question = question.replace("'", " ' ")
    question = question.replace('*', ' * ')
    question = question.replace('"', ' " ')
    question = question.replace(":", " : ")
    question = question.replace(";", " ; ")
    question = question.replace(",", " , ")
    question = question.replace("/", " / ")
    question = question.replace("?", " ? ")
    question = question.replace("!", " ! ")
    question = question.replace("#", " # ")
    question = question.replace("(", " ( ")
    question = question.replace(")", " ) ")
    question = question.replace("[", " [ ")
    question = question.replace("]", " ] ")
    question = question.replace("$", " $ ")
    question = question.replace("%", " % ")
    question = question.replace(".", " . ") # Comment Simon: destroys www.google.com
    return question


def tokenize(question):
    splits = question.split()
    splits = filter(lambda x: x not in [":", '"', '[', '(', '.', '/', "'", ']',',','*','~', ')','!','#','?','-'], splits)
    return splits


def find_unknowns(path_model, questions):

    # Check if we can parse it with gensim
    dir_model = os.path.dirname(path_model)
    name_model = os.path.basename(path_model)

    # We need to change it if it is in the native glove format
    if 'glove' in dir_model:
        # Check if we have a corrected version:
        if not 'w2v_' in name_model:
            files = glob.glob(dir_model)
            if not 'w2v_' + name_model in files:
                # Make it so format can be converged
                glove2word2vec(path_model, dir_model + '/w2v_' + name_model)
                name_model = 'w2v_' + name_model
                path_model = dir_model +  '/' + name_model


    if os.path.splitext(path_model)[1] == '.bin':
        model = gensim.models.KeyedVectors.load_word2vec_format(path_model, binary=True)
    else:
        model = gensim.models.KeyedVectors.load_word2vec_format(path_model, binary=False)

    # List for the unrecognized words
    list_illegal_aliens = []

    # Glove only has lowercase, so we need to split
    if 'glove' in dir_model or 'numberbatch' in dir_model or 'fasttext' in dir_model:
        # Obtain question
        for i, question in enumerate(questions):
            # Obtain words
            for word in tokenize(question):
                # Try to run it through the model
                try:
                    i = model[word.lower()]
                    break
                except:
                    list_illegal_aliens.append(word.lower())

    else:
        # Obtain question
        for i, question in enumerate(questions):
            # Obtain words
            for word in tokenize(question):
                # Try to run it through the model
                try:
                    i = model[word]
                    break
                except:
                    list_illegal_aliens.append(word)

    # Count stuff
    counts = Counter(list_illegal_aliens)

    # Print it sorted from high to low
    print 'The embedding ' + name_model
    print 'Number of unrecognized words:' + str(len(counts))
    print 'Number of failures:' + str(sum(counts.values()))
    print 'Worst offenders:' + str(sorted(counts.items(), key=lambda x: x[1], reverse=True)[:250])
    print '\n'
    return counts


def replace_word(old_word, new_word, text):
    """
    
    :param new_word: 
    :param old_word: 
    :param text: 
    :return: 
    """

    if old_word.lower() in text.lower():
        idx = 0
        while idx < len(text):
            index_l = text.lower().find(old_word.lower(), idx)
            if index_l == -1:
                return text
            text = text[:index_l] + new_word + text[index_l + len(old_word):]
            idx = index_l + len(old_word)

    return text


if __name__ == "__main__":
    #
    # So this is kinda ugly, some disclaimers:
    # - I use the .vec version for the fasttext, HOWEVER the .bin version is better (contains more info), but you need
    # python 3 for that and I dont want compatibility issues
    # - The download links for the embeddings will be presented below (with the freaking size of the things)
    # - I made the shitty replace_in_string on the training set (far from perfect), did not look at the test set
    # - All non binary stuff loads slow, give it some time
    # - The fasttext, numberbatch and glove embeddings need lowercase inputs, fyi
    # - Some of the uncertainties are shit: she's -> she is or she has, I know the first one is ugly
    # - The twitter Glove is not included ... we could try it though (it is not as clean as the rest)
    # - Change the paths to the models if you have a different location
    #
    # Download links
    #
    # Fasttext - 6.6 Gig - https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki.en.vec
    # Numberbatch - 1.0 Gig - http://conceptnet.s3.amazonaws.com/downloads/2017/numberbatch/numberbatch-en-17.04.txt.gz
    # Glove 6B - 1.0 Gig - http://nlp.stanford.edu/data/glove.6B.zip
    # Glove 42B - 5.0 Gig - http://nlp.stanford.edu/data/glove.42B.300d.zip
    # Glove 840B - 5.6 Gig - http://nlp.stanford.edu/data/glove.840B.300d.zip
    # GoogleNews - 3.6 Gig - https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit?usp=sharing

    # paths to the data
    trn_path = "../../../kaggle-quora-data/train.csv"
    tst_path = "../../../kaggle-quora-data/test.csv"

    # paths to the models
    models = [
        "../../../kaggle-quora-data/models/fasttext/wiki.en.vec",
        "../../../kaggle-quora-data/models/numberbatch/numberbatch-en.txt",
        "/media/rwiersema/UBUNTU 16_0/GoogleNews-vectors-negative300.bin",
        "../../../kaggle-quora-data/models/glove/glove.6B.300d.txt",
        "../../../kaggle-quora-data/models/glove/glove.42B.300d.txt",
        "../../../kaggle-quora-data/models/glove/glove.840B.300d.txt"
    ]

    # Load the questions
    data = pd.read_csv(trn_path).fillna('')
    
    pool = Pool(processes=4)
    
    data['question1'] = pool.map(replace_in_string, data['question1'])
    print 'Loaded and preprocessed question 1'
    
    data['question2'] = pool.map(replace_in_string, data['question2'])
    print 'Loaded and preprocessed question 2'
    
    pool.close()
    
    q1 = data['question1'].tolist()
    q2 = data['question2'].tolist()
    questions = q1 + q2

    ##
    if os.path.splitext(models[2])[1] == '.bin':
        model = gensim.models.KeyedVectors.load_word2vec_format(models[2], binary=True)
    else:
        model = gensim.models.KeyedVectors.load_word2vec_format(models[2], binary=False)
    ##
Qone = []
Qtwo = []    
for word in tokenize(q1[10]):
    try:
        Qone.append(model[word])
    except:
        print 'word %s not found' %word 
        
for word in tokenize(q2[10]):
    try:
        Qtwo.append(model[word])
    except:
        print 'word %s not found' %word 
    ##
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import numpy as np
C = 40
X = np.array(Qone)
model = TSNE(n_components=2, random_state=0,perplexity=C)
np.set_printoptions(suppress=True)
Tcoord1 = model.fit_transform(X)     

Y = np.array(Qtwo)
model2 = TSNE(n_components=2, random_state=0,perplexity=C)
np.set_printoptions(suppress=True)
Tcoord2 = model.fit_transform(Y)    
 
plt.plot(Tcoord2[0:11,0],Tcoord2[0:11,1],'r.',Tcoord1[0:6,0],Tcoord1[0:6,1],'b*')

plt.show()
