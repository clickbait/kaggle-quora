import seaborn
import pandas as pd
import missing_word_embedding as mwe
import matplotlib.pyplot as plt
import numpy as np
import string


trn_path = "../../../kaggle-quora-data/train.csv"
tst_path = "../../../kaggle-quora-data/test.csv"

data = pd.read_csv(tst_path).fillna('')
data['question1'] = data['question1'].apply(mwe.replace_in_string)
print 'Loaded and preprocessed question 1'

data['question2'] = data['question2'].apply(mwe.replace_in_string)
print 'Loaded and preprocessed question 2'

q1 = data['question1'].tolist()
q2 = data['question2'].tolist()
questions = q1 + q2

Q1 = [mwe.tokenize(f) for f in q1]
print 'Segmented question 1'
Q2 = [mwe.tokenize(f) for f in q2]
print 'Segmented question 2'

##
'''
Possible starts for question
Check if any part of the sentence starts with these words.
'''

wcounts = {'how':0,'should':0,'what':0,'which':0,'can':0,'when':0,'after':0,'is':0,'why':0, 'math':0,'if':0}
empty_sentences = 0
for i in range(0,len(Q1)):

    temp = questions[i].split()
    puncts = [k for k in temp if all(j in string.punctuation for j in k)]

    idx = []
    num_puncts = 1
    for f in range(0,len(puncts)-1):
        idx.append(temp.index(puncts[f])-num_puncts-1)
        num_puncts += 1
        
    idx.append(0)

    try:
        for j in range(0, len(idx)):
            if Q1[i][idx[j]].lower() == 'how':
                wcounts['how'] += 1
            if Q1[i][idx[j]].lower() == 'if':
                wcounts['if'] += 1                
            if Q1[i][idx[j]].lower() == 'should':
                wcounts['should'] += 1    
            if Q1[i][idx[j]].lower() == 'what':
                wcounts['what'] += 1
            if Q1[i][idx[j]].lower() == 'which':
                wcounts['which'] += 1
            if Q1[i][idx[j]].lower() == 'can':
                wcounts['can']+= 1
            if Q1[i][idx[j]].lower() == 'when':
                wcounts['when'] += 1
            if Q1[i][idx[j]].lower() == 'after':
                wcounts['after'] += 1
            if Q1[i][idx[j]].lower() == 'is':
                wcounts['is'] += 1
            if Q1[i][idx[j]].lower() == 'why':
                wcounts['why'] += 1                                                                       
        if any(word in Q1[i] for word in ['math','\\x']):
            wcounts['math'] += 1

    except: 
        empty_sentences +=1


    
print 'empty sentences = %i' % empty_sentences     
        
X = np.arange(len(wcounts))
plt.bar(X, wcounts.values(), align='center', width=0.5)
plt.xticks(X, wcounts.keys())
plt.show()

# How many are missing?
unidf = len(Q1)-np.sum(wcounts.values())
print 'How many are unidentified? %i' % unidf
print 
## find amount of negations
negations = 0
for i in range(0,len(questions)): 
    if any(word in questions[i].split() for word in ['not']):
            negations += 1
print negations

## longest sentence train = 19 words, longest sentence test = words
longest = ''
lens = []
for i in range(0,len(questions)):
    lens.append(len(questions[i].split()))
    if len(questions[i].split())>len(longest.split()):
        longest = questions[i]

print longest

plt.hist(lens,bins=100)
plt.show()