import nltk, re, pprint
import pandas as pd
import logging
from nltk.corpus import wordnet as wn
from nltk.corpus.reader.wordnet import WordNetError
import pattern.en as pat

logger = logging.getLogger(__name__)

doc_path = "/home/rooler/Documents/MLIP/kaggle-quora-data/train_preprocessed.csv"
label_path = "/home/rooler/Documents/MLIP/kaggle-quora-data/train_labels.csv"

data_frame = pd.read_csv(doc_path, encoding='utf-8')
data_frame_labels = pd.read_csv(label_path, encoding='utf-8')


def ie_preprocess(document):
    '''
    Preprocess the data
    :param document: 
    u'[where, is, my, mind]'

    :return: POS tagged list of tuples
    EXAMPLE
    [[(u'where', 'WRB'), (u'is', 'VBZ'), (u'my', 'PRP$'), (u'mind', 'NN')]]

    '''
    logger.info("Pos-tagging sentences")
    document = re.sub('[\[\]\?\,]', '', document)
    sentences = nltk.sent_tokenize(document)
    sentences = [nltk.word_tokenize(sent) for sent in sentences]
    sentences = [nltk.pos_tag(sent) for sent in sentences]
    return sentences

logger.info("Pos-tagging sentences")
# this is kinda slow
pos_q1 = data_frame['unigram1'][0:10000].apply(ie_preprocess)
pos_q2 = data_frame['unigram2'][0:10000].apply(ie_preprocess)

def filter_dupl_synsets(synset):
    names = list(set([syn.name().rsplit( ".", 2 )[0] for syn in synset]))
    return [wn.synset(name+'.n.01') for name in names]


def generate_syn_sentence(sentence, pos_sentence, sim_thresh = 0.1):
    '''
    Generate new synonomous sentences based using nltk wordnet
    
    :param sentence: list of strings, or simply list of single string
    EXAMPLE:
    u'[where, is, my, mind?]'

    :param pos_sentence: pos sentence generated with ie_preprocess
    EXAMPLE:
    [[(u'where', 'WRB'), (u'is', 'VBZ'), (u'my', 'PRP$'), (u'mind', 'NN')]]

    :param sim_thresh: simple double that thresholds, between 0.05 and 0.4 is appropriate
    
    :return: list of list of strings
    '''
    # get the unique words that are Nouns
    #TODO currently only works for nouns, need to add Nouns(NN,NNS), verbs(VB, VB), adjectives(RB) and adverbs (JJ)
    temp = list(set([word[0] for word in pos_sentence if word[1] in ['NN','NNS']]))
    syn_sents = []

    if temp == []:
        print pos_sentence
        return syn_sents

    for word in temp:
        word = pat.singularize(word)
        # empty set to ensure no duplication
        syn_words = set()
        # get a tuple of the synonym objects for the each <word>
        synset =  tuple(filter_dupl_synsets(wn.synsets(word, pos=wn.NOUN)))
        if synset==():
            break
        try:
            # iterate through the synonyms and compare them to the original word, distance is based on Hypernym/Hyponym distance
            for a, b in zip([wn.synset(word+'.n.01')]*len(synset), synset):
                # if the words are similar beyond a certain threshold, add them to the word set

                if a.path_similarity(b)>=sim_thresh:
                    syn_words.add(b)
            # for each item in syn_words we replace the original word with the synonym
            for item in syn_words:
                if item.name().rsplit(".", 2)[0]!=word:
                    syn_sents.append(sentence.replace(word, item.name().rsplit(".", 2)[0]))
        except WordNetError:
            print 'word not in wordnet: '
            print word
            break

    print len(temp)
    print temp
    return syn_sents

count = 0
num = range(0, 10000)
for qn, sentence, pos_sentence in zip(num, data_frame['unigram1'][0:10000], pos_q1):
    A = generate_syn_sentence(sentence, pos_sentence[0], sim_thresh = 0.1)
    if A == []:
        count+=1
        print count
