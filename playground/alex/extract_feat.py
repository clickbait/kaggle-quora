import os
import time
import spacy
import pandas as pd
import numpy as np

from fuzzywuzzy import fuzz

# Define a few global thingies
nlp = spacy.load("en")


def common_get_word_match_share(q1, q2):
    """
    
    :param q1: 
    :param q2: 
    :return: 
    """
    q1_words = {}
    q2_words = {}

    sentence_q1 = nlp(unicode(q1, encoding="utf-8"))
    for token in sentence_q1:
        if not token.is_stop:
            q1_words[token] = 1

    sentence_q2 = nlp(unicode(q2, encoding="utf-8"))
    for token in sentence_q2:
        if not token.is_stop:
            q2_words[token] = 1

    if len(q1_words) == 0 or len(q2_words) == 0:
        return 0
    else:
        shared_words_q1 = [w for w in q1_words.keys() if w in q2_words]
        shared_words_q2 = [w for w in q2_words.keys() if w in q1_words]
        R = (len(shared_words_q1) + len(shared_words_q2))/float(len(q1_words) + len(q2_words))
        return R


def common_get_word_match_notshare(q1, q2):
    """

    :param q1: 
    :param q2: 
    :return: 
    """
    q1_words = {}
    q2_words = {}

    sentence_q1 = nlp(unicode(q1, encoding="utf-8"))
    for token in sentence_q1:
        if not token.is_stop:
            q1_words[token] = 1

    sentence_q2 = nlp(unicode(q2, encoding="utf-8"))
    for token in sentence_q2:
        if not token.is_stop:
            q2_words[token] = 1

    if len(q1_words) == 0 or len(q2_words) == 0:
        return 0
    else:
        notshared_words_q1 = [w for w in q1_words.keys() if w  not in q2_words]
        notshared_words_q2 = [w for w in q2_words.keys() if w not in q1_words]
        R = (len(notshared_words_q1) + len(notshared_words_q2)) / float(len(q1_words) + len(q2_words))
        return R


def get_question_features(q, q_features_list):
    """
    Extract the features unique to the individual  question
    
    :param q: The string containing the question 
    :param q_feature_list: A list of functions with all the features we want
    :return: The features
    """

    question_features = np.zeros((len(q_features_list)), dtype=np.float32)
    for i, func in enumerate(q_features_list):
        question_features[i] = q_features_list[i](q)

    return question_features


def get_common_features(q1, q2, c_features_list):
    """
    Extract the features unique to the individual  question

    :param q1: The string containing question 1
    :param q2: The string containing question 2
    :param c_features_list: A list with the functions for all the features we want
    :return: The features
    """

    common_features = np.zeros((len(c_features_list)), dtype=np.float32)
    for i, func in enumerate(c_features_list):
        common_features[i] = c_features_list[i](q1, q2)

    return common_features


def extract_feature(q1, q2, question_features_list, common_features_list):
    """
    Extract the features of Questions 1 and 2 and those 
    that are dependent on both of them.
    
    :param q1: The string containing the first question
    :param q2: The string containing the second question
    :param feature_selection: A list containing the ids of the features that we want to extract
    :return: Features of Q1, Q2 and common features (double, since we switch Q1 and Q2 for the second half)
    """

    # Need to add: Check for names

    # Setup the storage for the features
    q1_features = np.zeros((2, len(question_features_list)), dtype = np.float32)
    q2_features = np.zeros((2, len(question_features_list)), dtype = np.float32)
    common_features = np.zeros((2, len(common_features_list)), dtype = np.float32)

    # Get the features
    q1_features[0, :] = get_question_features(q1, question_features_list)
    q2_features[0, :] = get_question_features(q2, question_features_list)
    common_features[0, :] = get_common_features(q1, q2, common_features_list)

    # Switch question 1 and question 2
    q1_features[1, :] = q2_features[0, :]
    q2_features[1, :] = q1_features[0, :]
    common_features[1, :] = common_features[0, :]

    # Merge the feature sets into one
    _features = np.concatenate((q1_features, q2_features, common_features), axis=1)

    return _features


def get_feature(path, question_features_list, common_features_list):
    """
    
    :param path: 
    :param question_features_list: 
    :param common_features_list: 
    :return: 
    """

    # Obtain the file
    trn_data = pd.read_csv(path)

    # Listify the questions
    list_q1 = trn_data['question1'].tolist()
    list_q2 = trn_data['question2'].tolist()
    list_lbl = trn_data['is_duplicate'].tolist()

    print trn_data.shape

    # Object to store the features
    nb_features = 2*len(question_features_list) + len(common_features_list)
    feature_storage = np.zeros((2*len(list_lbl), nb_features), dtype=np.float32)
    lbl_storage = np.asarray(2*list_lbl, dtype=np.bool_)

    t0 = time.time()
    # Extract the features for each question
    for index in trn_data['id']:
        print 'percentage done:',  100*index/float(len(list_lbl)), '\r',

        # Obtain the questions
        q1 = str(list_q1[index])
        q2 = str(list_q2[index])
        feature_storage[2*index:2*index+2, :] = extract_feature(q1, q2, question_features_list, common_features_list)
        lbl_storage[2*index:2*index+2] = list_lbl[index]
    print t0 - time.time()

    print '\n', feature_storage.shape
    np.savetxt(os.path.dirname(path) + '/trn_feat.csv', feature_storage,  delimiter=',')
    np.savetxt(os.path.dirname(path) + '/trn_lbl.csv', lbl_storage, delimiter=',')

if __name__ == '__main__':

    # Define the question features
    question_features_list = [
        lambda x: len(x),                               # Number of characters
        lambda x: len(x.split()),                       # Number of words
        lambda x: len(x)/float(len(x.split())),         # Number of characters per word
    ]

    # Define the common features
    common_features_list = [
        lambda x, y: np.abs(len(x) - len(y)),                       # Difference in number of characters
        lambda x, y: np.abs(len(x.split()) - len(y.split())),       # Difference in number of words
        lambda x, y: np.abs(len(set(x.split()) & set(y.split()))),  # Number of words in common
        lambda x, y: np.abs(len(set(x.split()) ^ set(y.split()))),  # Number of words that are unique to both x and y
        lambda x, y: fuzz.ratio(x, y),                              # The ratio from fuzzy string matching
        lambda x, y: fuzz.partial_ratio(x, y),                      # The partial ratio from fuzzy string matching
        lambda x, y: fuzz.partial_token_sort_ratio(x, y),           # The partial ratio over the set
        lambda x, y: common_get_word_match_share(x, y),             # The ratio of words shared without stopwords
        lambda x, y: common_get_word_match_notshare(x, y)           # The ratio of words not shared without stopwords
    ]

    path = trn_path = '../../kaggle-quora-data/train.csv'
    get_feature(path, question_features_list, common_features_list)

