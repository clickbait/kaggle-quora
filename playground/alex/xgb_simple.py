import numpy as np
import xgboost as xgb
import matplotlib.pyplot as plt

# from sklearn.model_selection import train_test_split

def xgb_classifier(_feat, _lbl):
    """
    Uses xgboost to classify whether a question is
    a duplicate or not. So far we only train ...
    
    :param _feat: 
    :param _lbl: 
    :return: 
    """

    # Make a training and validation split
    trn_feat, val_feat, trn_lbl, val_lbl = train_test_split(_feat, _lbl, test_size=0.5)

    # Set our parameters for xgboost
    params = {}
    params['objective'] = 'binary:logistic'
    params['eval_metric'] = 'logloss'
    params['eta'] = 0.01
    params['max_depth'] = 4
    params['verbose'] = 0

    d_train = xgb.DMatrix(trn_feat, label=trn_lbl)
    d_valid = xgb.DMatrix(val_feat, label=val_lbl)

    watchlist = [(d_train, 'train'), (d_valid, 'valid')]

    bst = xgb.train(params, d_train, 400, watchlist)
    xgb.plot_importance(bst)
    plt.show()

if __name__ == '__main__':
    path = "../../kaggle-quora-data/"
    _feat = np.loadtxt(path + 'trn_feat.csv', delimiter=',')
    _lbl = np.loadtxt(path + 'trn_lbl.csv', delimiter=',')

    xgb_classifier(_feat, _lbl)