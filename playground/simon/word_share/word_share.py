import numpy as np
import pandas as pd
tr = pd.read_csv('../input/train.csv')
te = pd.read_csv('../input/test.csv')
from nltk.corpus import stopwords

def word_match_share(x, stops=None):
    '''
    The much-loved word_match_share feature.

    Args:
        x: source data with question1/2
        stops: set of stop words to remove; default is None for nltk stops
        
    Returns:
        the ever-popular word_match_share feature as a pandas Series
    '''
    if stops is None:
        stops = set(stopwords.words('english'))
    q1 = x.question1.fillna(' ').str.lower().str.split()
    q2 = x.question2.fillna(' ').str.lower().str.split()
    q1 = q1.map(lambda l : set(l) - stops)
    q2 = q2.map(lambda l : set(l) - stops)
    q = pd.DataFrame({'q1':q1, 'q2':q2})
    q['len_inter'] = q.apply(lambda row : len(row['q1'] & row['q2']), axis=1)
    q['len_tot'] = q.q1.map(len) + q.q2.map(len)
    return (2 * q.len_inter / q.len_tot).fillna(0)

def get_stats(tr, c=10):
    '''
    Compute word_match_share on <tr>, bin the values and average the targets.
    Returns a dict from rounded word_match_share statistics to probabilities.

    Args:
        tr: pandas DataFrame with question1/2 in it
        c: word shares are rounded to whole numbers after multiplying by c.
            The default is 10.

    Returns:
        2 dicts from rounded word_match_share values to counts and positives
    '''
    x = tr[['is_duplicate']].copy()
    wms = word_match_share(tr)
    x['round_wms'] = (c * wms).round()
    gp = x.groupby('round_wms')
    pos = gp.is_duplicate.sum().to_dict()
    cts = x.round_wms.value_counts().to_dict()
    return pos, cts

probs, cts = get_stats(tr, c=100)

def apply_stats(te, pos, cts, v_pos, vss, c=10):
    '''
    Applies the stats from get_stats to the test frame.

    Args:
        te: test data frame
        pos: dict of { hash(question) : positive count } for train
        cts: dict of { hash(question) : occurance counts } for train
        v_pos: number of virtual positives for smoothing (can be non-integer)
        vss: virtual sample size for smoothing (can be non-integer)
        c: word shares are rounded to whole numbers after multiplying by c.
            The default is 10.

    Returns:
        pandas Series of probabilities.
    '''
    wms = word_match_share(te)
    round_wms = (c * wms).round()
    te_pos = round_wms.map(lambda x : pos.get(x, 0))
    te_cts = round_wms.map(lambda x : cts.get(x, 0))
    prob = (te_pos + v_pos) / (te_cts + vss)
    return prob

raw_pred = apply_stats(te, probs, cts, 1, 3, c=100)

def mean_label_adjust(sub, target, current):
    '''
    Uses case-control type log-odds additive shift to correct label bias.

    Args:
        sub: the submission frame
        target: the target value for the mean label
        current: the current OOB or CV mean label, 
                or mean label of training data

    Returns:
        copy of sub with mean prediction shifted.
    '''
    target_log_odds = np.log(target / (1 - target))
    current_log_odds = np.log(current / (1 - current))
    out = sub.copy()
    out['log_odds'] = np.log(out.is_duplicate/ (1 - out.is_duplicate))
    out['adj_log_odds'] = out.log_odds - current_log_odds + target_log_odds
    out.is_duplicate = 1 / (1 + np.exp(-out.adj_log_odds))
    return out[['test_id', 'is_duplicate']]



sub = te[['test_id']].copy()
sub['is_duplicate'] = raw_pred
sub = mean_label_adjust(sub, 0.175, 0.37)
sub.to_csv('no_ml_model.csv', index=False, float_format='%.6f')
