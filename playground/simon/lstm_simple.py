import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Dense, Input, LSTM, Embedding, Dropout, Activation
from keras.layers.merge import concatenate
from keras.models import Model
from keras.layers.normalization import BatchNormalization
from keras.callbacks import EarlyStopping, ModelCheckpoint


class ClassifierLSTMSimple(object):
    def __init__(self, config):
        self.config = config
        self.model = None

    def fit(self, trn_feat, trn_lbl, val_feat, val_lbl):
        MAX_SEQUENCE_LENGTH = 30
        MAX_NB_WORDS = 200000
        EMBEDDING_DIM = 300

        num_lstm = np.random.randint(175, 275)
        num_dense = np.random.randint(100, 150)
        rate_drop_lstm = 0.15 + np.random.rand() * 0.25
        rate_drop_dense = 0.15 + np.random.rand() * 0.25

        act = 'relu'

        STAMP = 'lstm_%d_%d_%.2f_%.2f' % (num_lstm, num_dense, rate_drop_lstm, \
                                          rate_drop_dense)

        ########################################
        ## prepare embeddings
        ########################################
        print('Preparing embedding matrix')
        nb_words = MAX_NB_WORDS

        embedding_matrix = np.zeros((nb_words, EMBEDDING_DIM))
        for word, i in word_index.items():
            if word in word2vec.vocab:
                embedding_matrix[i] = word2vec[word]
        print('Null word embeddings: %d' % np.sum(np.sum(embedding_matrix, axis=1) == 0))


        ########################################
        ## define the model structure
        ########################################
        embedding_layer = Embedding(MAX_NB_WORDS,
                                    EMBEDDING_DIM,
                                    weights=[embedding_matrix],
                                    input_length=MAX_SEQUENCE_LENGTH,
                                    trainable=False)
        lstm_layer = LSTM(num_lstm, dropout=rate_drop_lstm, recurrent_dropout=rate_drop_lstm)

        sequence_1_input = Input(shape=(MAX_SEQUENCE_LENGTH,), dtype='int32')
        embedded_sequences_1 = embedding_layer(sequence_1_input)
        x1 = lstm_layer(embedded_sequences_1)

        sequence_2_input = Input(shape=(MAX_SEQUENCE_LENGTH,), dtype='int32')
        embedded_sequences_2 = embedding_layer(sequence_2_input)
        y1 = lstm_layer(embedded_sequences_2)

        merged = concatenate([x1, y1])
        merged = Dropout(rate_drop_dense)(merged)
        merged = BatchNormalization()(merged)

        merged = Dense(num_dense, activation=act)(merged)
        merged = Dropout(rate_drop_dense)(merged)
        merged = BatchNormalization()(merged)

        preds = Dense(1, activation='sigmoid')(merged)

        ########################################
        ## train the model
        ########################################
        model = Model(inputs=[sequence_1_input, sequence_2_input], \
                      outputs=preds)
        model.compile(loss='binary_crossentropy',
                      optimizer='nadam',
                      metrics=['acc'])
        model.summary()

        early_stopping = EarlyStopping(monitor='val_loss', patience=3)
        bst_model_path = STAMP + '.h5'
        model_checkpoint = ModelCheckpoint(bst_model_path, save_best_only=True, save_weights_only=True)

        hist = model.fit([data_1_train, data_2_train], labels_train, \
                         validation_data=([data_1_val, data_2_val], labels_val, weight_val), \
                         epochs=200, batch_size=2048, shuffle=True, \
                         class_weight=class_weight, callbacks=[early_stopping, model_checkpoint])

        model.load_weights(bst_model_path)
        bst_val_score = min(hist.history['val_loss'])

    def predict(self, tst_feat):
        pass