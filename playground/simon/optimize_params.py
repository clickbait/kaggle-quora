import cPickle as pickle
import pandas as pd
import numpy as np
import xgboost as xgb
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from sklearn.metrics import log_loss


def score(params):
    params['max_depth'] = int(params['max_depth'])
    params['n_estimators'] = int(params['n_estimators'])
    params['min_child_weight'] = int(params['min_child_weight'])
    if params['oversample'] == 1:
        dtrain = xgb.DMatrix(X_train_new, label=Y_train_new)
    else:
        dtrain = xgb.DMatrix(X_train, label=Y_train)

    print "Training with params : "
    print params
    num_round = int(params['n_estimators'])
    del params['n_estimators']
    dvalid = xgb.DMatrix(X_test, label=Y_test)
    # watchlist = [(dvalid, 'eval'), (dtrain, 'train')]
    model = xgb.train(params, dtrain, num_round)
    predictions = model.predict(dvalid)
    score = log_loss(Y_test, predictions)
    print "\tScore {0}\n\n".format(score)
    return {'loss': score, 'status': STATUS_OK}


def optimize(trials):
    space = {
        'n_estimators': hp.quniform('n_estimators', 100, 5000, 1),
        'eta': hp.quniform('eta', 0.025, 0.5, 0.025),
        'max_depth': hp.quniform('max_depth', 1, 25, 1),
        'min_child_weight': hp.quniform('min_child_weight', 1, 6, 1),
        'subsample': hp.quniform('subsample', 0.5, 1, 0.05),
        'gamma': hp.quniform('gamma', 0.5, 1, 0.05),
        'colsample_bytree': hp.quniform('colsample_bytree', 0.5, 1, 0.05),
        #'scale_pos_weight': hp.choice('scale_pos_weight', [0.17, 0.37, 1.7, 4.5]),
        'eval_metric': 'logloss',
        'oversample': hp.choice('oversample', [0, 1]),
        'objective': 'binary:logistic',
        'seed': 42,
        'nthread': 8,
        'silent': 1,
        'verbose': 0,
    }

    best = fmin(score, space, algo=tpe.suggest, trials=trials, max_evals=1000, verbose=1)

    print best


print "Loading data"
with open('../../x_train_feat.pickle', 'r+') as f:
    X_train = pickle.load(f)

with open('../../x_test_feat.pickle', 'r+') as f:
    X_test = pickle.load(f)

with open('../../y_train.pickle', 'r+') as f:
    Y_train = pickle.load(f)

with open('../../y_test.pickle', 'r+') as f:
    Y_test = pickle.load(f)

pos_train = X_train[Y_train == 1]
neg_train = X_train[Y_train == 0]

print("Oversampling started for proportion: {}".format(len(pos_train) / (len(pos_train) + len(neg_train))))
p = 0.165
scale = ((len(pos_train) / (len(pos_train) + len(neg_train))) / p) - 1
while scale > 1:
    neg_train = pd.concat([neg_train, neg_train])
    scale -= 1
neg_train = pd.concat([neg_train, neg_train[:int(scale * len(neg_train))]])
print("Oversampling done, new proportion: {}".format(len(pos_train) / (len(pos_train) + len(neg_train))))

X_train_new = pd.concat([pos_train, neg_train])
Y_train_new = (np.zeros(len(pos_train)) + 1).tolist() + np.zeros(len(neg_train)).tolist()
del pos_train, neg_train

print "Starting trails"
# Trials object where the history of search will be stored
trials = Trials()

optimize(trials)
