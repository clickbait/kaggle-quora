#mkdir data
cd data
wget --tries=500 http://nlp.stanford.edu/data/glove.840B.300d.zip
unzip glove.840B.300d.zip
rm glove.840B.300d.zip
# wget http://qim.ec.quoracdn.net/quora_duplicate_questions.tsv
#wget https://s3.amazonaws.com/dl4j-distribution/GoogleNews-vectors-negative300.bin.gz
#sudo python -m nltk.downloader stopwords
cd ..
