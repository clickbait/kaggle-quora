from multiprocessing import Pool

import gensim
import pandas as pd
import numpy as np
from tqdm import tqdm
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM, GRU
from keras.layers.wrappers import Bidirectional
from keras.layers.normalization import BatchNormalization
from keras.utils import np_utils
from keras.layers import TimeDistributed, Lambda, concatenate, Merge, SpatialDropout1D
from keras.layers import Convolution1D, GlobalMaxPooling1D
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras import backend as K
from keras.layers.advanced_activations import PReLU
from keras.preprocessing import sequence, text
from keras.preprocessing.text import text_to_word_sequence
import nltk
import cPickle as pickle
import sys
from tmp import platslaan_ja, replace_in_string

TensorBoard(log_dir='./logs_awesome', histogram_freq=1, write_graph=True, write_images=True, embeddings_freq=1)

data = pd.read_csv('../../../kaggle-quora-data/quora_duplicate_questions.tsv', sep='\t')
y = data.is_duplicate.values

pool = Pool(processes=8)

num_words = 400000  # 200000
num_pos_tags = 49
q1 = list(data.question1.values)
q1 = pool.map(replace_in_string, q1)
q2 = list(data.question2.values.astype(str))
q2 = pool.map(replace_in_string, q2)

questions = q1 + q2

max_len = 40

# pos_tagged_questions_q1 = pool.map(text_to_word_sequence, q1)
# pos_tagged_questions_q1 = pool.map(nltk.pos_tag, pos_tagged_questions_q1)
# pos_tagged_questions_q1 = pool.map(platslaan_ja, pos_tagged_questions_q1)
#
# with open('pos_tagged_questions_q1.pickle', 'w+') as f:
#     pickle.dump(pos_tagged_questions_q1, f)

# with open('pos_tagged_questions_q1.pickle', 'r+') as f:
#     pos_tagged_questions_q1 = pickle.load(f)

# pos_tagged_questions_q2 = pool.map(text_to_word_sequence, q2)
# pos_tagged_questions_q2 = pool.map(nltk.pos_tag, pos_tagged_questions_q2)
# pos_tagged_questions_q2 = pool.map(platslaan_ja, pos_tagged_questions_q2)
#
# with open('pos_tagged_questions_q2.pickle', 'w+') as f:
#     pickle.dump(pos_tagged_questions_q2, f)

# with open('pos_tagged_questions_q2.pickle', 'r+') as f:
#     pos_tagged_questions_q2 = pickle.load(f)

# pos_tagged_questions = pos_tagged_questions_q1 + pos_tagged_questions_q2
#
# tk_pos = text.Tokenizer(num_words=num_pos_tags)
# tk_pos.fit_on_texts(pos_tagged_questions)
# del pos_tagged_questions
#
# p1 = tk_pos.texts_to_sequences(pos_tagged_questions_q1)
# p1 = sequence.pad_sequences(p1, maxlen=max_len)
#
# p2 = tk_pos.texts_to_sequences(pos_tagged_questions_q2)
# p2 = sequence.pad_sequences(p2, maxlen=max_len)
#
# pos_index = tk_pos.word_index

tk = text.Tokenizer(num_words=num_words)
tk.fit_on_texts(questions)
del questions

x1 = tk.texts_to_sequences(data.question1.values)
x1 = sequence.pad_sequences(x1, maxlen=max_len)

x2 = tk.texts_to_sequences(data.question2.values.astype(str))
x2 = sequence.pad_sequences(x2, maxlen=max_len)

word_index = tk.word_index

ytrain_enc = np_utils.to_categorical(y)

embeddings_index_w2v = {}
with open('../../../kaggle-quora-data/models/glove/glove.840B.300d.txt') as f:
    for line in tqdm(f):
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index_w2v[word] = coefs



# embeddings_index_pos = {}
print('Found %s word vectors.' % len(embeddings_index_w2v))

embedding_matrix_w2v = np.zeros((len(word_index) + 1, 300))
for word, i in tqdm(word_index.items()):
    embedding_vector = embeddings_index_w2v.get(word)
    if embedding_vector is not None:
        embedding_matrix_w2v[i] = embedding_vector

embeddings_index_nb = {}
model_nb = gensim.models.KeyedVectors.load_word2vec_format('../../../kaggle-quora-data/models/numberbatch/numberbatch-en.txt', binary=False)

embedding_matrix_nb = np.zeros((len(word_index) + 1, 300))
for word, i in tqdm(word_index.items()):
    if word in model_nb.vocab:
        embedding_matrix_nb[i] = model_nb[word]

    # embedding_vector_pos = embeddings_index_pos.get(word)

    # if embedding_vector is not None and embedding_vector_pos is not None:
    #     #embedding_matrix_w2v[i] = embedding_vector
    #     embedding_matrix_w2v[i][:300] = embedding_vector
    #     embedding_matrix_w2v[i][300:] = embedding_vector_pos

# embedding_matrix_pos = np.zeros((len(word_index) + 1, 49))  # or 300 + 49
# for word, i in tqdm(pos_index.items()):
#     embedding_vector = embeddings_index_pos.get(word)
#     if embedding_vector is not None:
#         embedding_matrix_pos[i] = embedding_vector

pool.close()

max_features = num_words
filter_length = 10  # 5
nb_filter = 64
pool_length = 5  # 4
dropout = 0.2  # 0.2
batch_size = 768  # 384

input_length = max_len  # 40

model = Sequential()
print('Build model...')

model1 = Sequential()
model1.add(Embedding(len(word_index) + 1,
                     300,
                     weights=[embedding_matrix_nb],
                     input_length=input_length,
                     trainable=False))

model1.add(TimeDistributed(Dense(300, kernel_initializer='uniform')))
model1.add(PReLU(alpha_initializer="zero", weights=None))
model1.add(Lambda(lambda x: K.sum(x, axis=1), output_shape=(300,)))

model2 = Sequential()
model2.add(Embedding(len(word_index) + 1,
                     300,
                     weights=[embedding_matrix_nb],
                     input_length=input_length,
                     trainable=False))

model2.add(TimeDistributed(Dense(300, kernel_initializer='uniform')))
model2.add(PReLU(alpha_initializer="zero", weights=None))
model2.add(Lambda(lambda x: K.sum(x, axis=1), output_shape=(300,)))

model3 = Sequential()
model3.add(Embedding(len(word_index) + 1,
                     300,
                     weights=[embedding_matrix_nb],
                     input_length=input_length,
                     trainable=False))
model3.add(Convolution1D(filters=nb_filter,
                         kernel_size=filter_length,
                         padding='valid',
                         activation='relu',
                         strides=1))
model3.add(Dropout(dropout))

model3.add(Convolution1D(filters=nb_filter,
                         kernel_size=filter_length,
                         padding='valid',
                         activation='relu',
                         strides=1))

model3.add(GlobalMaxPooling1D())
model3.add(Dropout(dropout))

model3.add(Dense(300))
model3.add(Dropout(dropout))
model3.add(BatchNormalization())

model4 = Sequential()
model4.add(Embedding(len(word_index) + 1,
                     300,
                     weights=[embedding_matrix_nb],
                     input_length=input_length,
                     trainable=False))
model4.add(Convolution1D(filters=nb_filter,
                         kernel_size=filter_length,
                         padding='valid',
                         activation='relu',
                         strides=1))
model4.add(Dropout(dropout))

model4.add(Convolution1D(filters=nb_filter,
                         kernel_size=filter_length,
                         padding='valid',
                         activation='relu',
                         strides=1))

model4.add(GlobalMaxPooling1D())
model4.add(Dropout(dropout))

model4.add(Dense(300))
model4.add(Dropout(dropout))
model4.add(BatchNormalization())

model5 = Sequential()
model5.add(Embedding(len(word_index) + 1, 300, input_length=input_length))
model5.add(SpatialDropout1D(dropout))
model5.add(LSTM(300, dropout=dropout, recurrent_dropout=0.2))

model6 = Sequential()
model6.add(Embedding(len(word_index) + 1, 300, input_length=input_length))
model6.add(SpatialDropout1D(dropout))
model6.add(LSTM(300, dropout=dropout, recurrent_dropout=0.2))

# POS tagged input
# model7 = Sequential()
# model7.add(Embedding(len(pos_index) + 1, 49, input_length=input_length, dropout=dropout))
# model7.add(LSTM(49, dropout=dropout, recurrent_dropout=0.2))
# #
# model8 = Sequential()
# model8.add(Embedding(len(pos_index) + 1, 49, input_length=input_length, dropout=dropout))
# model8.add(LSTM(49, dropout=dropout, recurrent_dropout=0.2))

# Reverse Input stuff
# model9 = Sequential()
# model9.add(Embedding(len(word_index) + 1, 300, input_length=input_length, dropout=dropout))
# model9.add(Bidirectional(LSTM(300, dropout=dropout, recurrent_dropout=0.2)))
#
# model10 = Sequential()
# model10.add(Embedding(len(word_index) + 1, 300, input_length=input_length, dropout=dropout))
# model10.add(Bidirectional(LSTM(300, dropout=dropout, recurrent_dropout=0.2)))
# /Reverse LSTM

merged_model = Sequential()
merged_model.add(Merge([model1, model2, model3, model4, model5, model6], mode='concat'))
merged_model.add(BatchNormalization())

merged_model.add(Dense(300))
merged_model.add(PReLU())
merged_model.add(Dropout(dropout))
merged_model.add(BatchNormalization())

merged_model.add(Dense(300))
merged_model.add(PReLU())
merged_model.add(Dropout(dropout))
merged_model.add(BatchNormalization())

merged_model.add(Dense(300))
merged_model.add(PReLU())
merged_model.add(Dropout(dropout))
merged_model.add(BatchNormalization())

merged_model.add(Dense(300))
merged_model.add(PReLU())
merged_model.add(Dropout(dropout))
merged_model.add(BatchNormalization())

merged_model.add(Dense(300))
merged_model.add(PReLU())
merged_model.add(Dropout(dropout))
merged_model.add(BatchNormalization())

# TODO: make two points
merged_model.add(Dense(1))
merged_model.add(Activation('sigmoid'))

merged_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

checkpoint = ModelCheckpoint('weights.h5', monitor='val_acc', save_best_only=True, verbose=2)

merged_model.fit([x1, x2, x1, x2, x1, x2], y=y, batch_size=batch_size, epochs=200,
                 verbose=2, validation_split=0.1, shuffle=True, callbacks=[checkpoint])

# Best, dropout = 0.2
# loss: 0.0220 - acc: 0.9920 - val_loss: 1.1816 - val_acc: 0.8191 [epoch 30]
