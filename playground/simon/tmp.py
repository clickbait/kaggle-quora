#!/usr/bin/python
# -*- coding: utf-8 -*-

def platslaan_ja(x):
    tag_dictionary = {'VBG': '29', 'FW': '5', 'VBN': '30', ',': '40', 'VBP': '31', 'WDT': '33', 'JJ': '7', 'WP': '35',
                      'VBZ': '32', 'DT': '3', 'PP': '19', '"': '48', 'RP': '23', '$': '38', 'NN': '12', ')': '43',
                      '(': '42', 'VBD': '28', 'POS': '17', '.': '39', 'TO': '25', 'PRP': '18', 'RB': '20', ':': '41',
                      'NNS': '13', 'NNP': '14', '#': '37', 'VB': '27', 'WRB': '36', '`': '45', 'CC': '1', 'LS': '10',
                      'PDT': '16', 'RBS': '22', 'RBR': '21', 'CD': '2', 'EX': '4', 'IN': '6', 'MD': '11', 'NNPS': '15',
                      "'": '47', 'JJS': '9', 'JJR': '8', 'SYM': '24', 'UH': '26', 'PRP$': '49'}

    tags = [k[1] for k in x if k[1] in tag_dictionary.keys()]
    if len(tags) == 0:
        return ""
    return " ".join(tags)


def replace_in_string(question):
    # Replace common combo's
    question = replace_word("Don't", "Do not", question)
    question = replace_word("Doesn't", "Does not", question)
    question = replace_word("Didn't", "Did not", question)
    question = replace_word("Aren't", "Are not", question)
    question = replace_word("Haven't", "Have not", question)
    question = replace_word("Hasn't", "Has not", question)
    question = replace_word("Isn't", "Is not", question)
    question = replace_word("It's", "It is", question)
    question = replace_word("I'm", "I am", question)
    question = replace_word("I'll", "I will", question)
    question = replace_word("I've", "I have", question)
    question = replace_word("You've", "You have", question)
    question = replace_word("You're", "You are", question)

    question = replace_word("We're", "We are", question)
    question = replace_word("We've", "We have", question)
    question = replace_word("Where's", "Where is", question)
    question = replace_word("Why's", "Why is", question)
    question = replace_word("What`s", "What is", question)
    question = replace_word("What's", "What is", question)
    question = replace_word("What're", "What are", question)
    question = replace_word("Wasn't", "Was not", question)
    question = replace_word("Who'll", "Who will", question)
    question = replace_word("Who's", "Who is", question)
    question = replace_word("Can't", "cannot", question)
    question = replace_word("How's", "How is", question)
    question = replace_word("Shouldn't", "Should not", question)
    question = replace_word("Wouldn't", "Would not", question)
    question = replace_word("Couldn't", "Could not", question)
    question = replace_word("There's", "There is", question)
    question = replace_word("WhAt", "what", question)

    # Added by Simon later on
    question = replace_word("don't", "do not", question)  # Comment Simon: precompile regexes for better performance
    question = replace_word("what's", "what is", question)
    question = replace_word("let's", "let us", question)
    question = replace_word("won't", "will not", question)
    question = replace_word("i`m", "i am", question)
    question = replace_word("i'm", "i am", question)
    question = replace_word("is'nt", "is not", question)

    # Split words
    question = replace_word("toothbrushing", "tooth brushing", question)
    question = replace_word("timetravel", "time travel", question)
    question = replace_word("howmany", "how many", question)
    question = replace_word("howdo", "how do", question)
    question = replace_word("howto", "how to", question)
    question = replace_word("anyguidance", "any guidance", question)
    question = replace_word("ihave", "i have", question)
    question = replace_word("how-to", "how to", question)
    question = replace_word("howdifficult", "how difficult", question)
    question = replace_word("currentpolitical", "current political", question)
    question = replace_word("whatdo", "what do", question)
    question = replace_word("whatis", "what is", question)
    question = replace_word("what_are", "what are", question)
    question = replace_word("stand-up", "stand up", question)
    question = replace_word("self-improvement", "self improvement", question)
    question = replace_word("self-help", "self help", question)
    question = replace_word("ride-sharing", "ride sharing", question)
    question = replace_word("non-mexican", "non mexican", question)
    question = replace_word("non-native", "non native", question)
    question = replace_word("2-step", "two step", question)
    question = replace_word("multicolour", "multicolor", question)
    question = replace_word("remote-controlled", "remote controlled", question)
    question = replace_word("t-shirts", "shirts", question)
    question = replace_word("brexit", "great britain europa exit", question)

    # Spelling corrections
    question = replace_word("spoliers", "spoilers", question)
    question = replace_word("benifits", "benefits", question)
    question = replace_word("benifit", "benefit", question)
    question = replace_word("analysit", "analyst", question)
    question = replace_word("enthalphy", "enthalpy", question)
    question = replace_word("diffrence", "difference", question)
    question = replace_word("similarites", "similarities", question)
    question = replace_word("intresting", "interesting", question)
    question = replace_word("distinguishin", "distinguishing", question)
    question = replace_word("diffrerence", "difference", question)
    question = replace_word("hollaween", "halloween", question)
    question = replace_word("hypotheticaly", "hypothetically", question)
    question = replace_word("nowaday", "nowadays", question)
    question = replace_word("nowadayss", "nowadays", question)
    question = replace_word("eligibilty", "eligibility", question)
    question = replace_word("eductaion", "education", question)
    question = replace_word("religon", "religion", question)
    question = replace_word("whuch", "which", question)
    question = replace_word("plzz", "please", question)
    question = replace_word("hhow", "how", question)
    question = replace_word("hiii", "hi", question)
    question = replace_word("wwhat", "what", question)
    question = replace_word("fifthgenration", "fifth generation", question)
    question = replace_word("netwark", "network", question)
    question = replace_word("diffiulties", "difficulties", question)
    question = replace_word("whirphool", "whirlpool", question)
    question = replace_word("tutuion", "tuition", question)
    question = replace_word("lecturrer", "lecturer", question)
    question = replace_word("mongolion", "mongolian", question)
    question = replace_word("doesnt", "does not", question)
    question = replace_word("compassinate", "compassionate", question)
    question = replace_word("whicch", "which", question)
    question = replace_word("liablities", "liabilities", question)
    question = replace_word("aproximately", "approximately", question)
    question = replace_word("shuold", "should", question)
    question = replace_word("dektop", "desktop", question)
    question = replace_word("formated", "formatted", question)
    question = replace_word("kwality", "quality", question)
    question = replace_word("howbto", "how to", question)
    question = replace_word("corelation", "correlation", question)
    question = replace_word("harresment", "harassment", question)
    question = replace_word("slogen", "slogan", question)
    question = replace_word("athesim", "atheism", question)
    question = replace_word("penrive", "pendrive", question)
    question = replace_word("relliance", "reliance", question)
    question = replace_word("opportunuty", "opportunity", question)
    question = replace_word("marsmellow", "marshmallow", question)
    question = replace_word("wheather", "weather", question)
    question = replace_word("parfums", "perfume", question)
    question = replace_word("travelpune", "travel pune", question)  # pune is a city in India

    return question


def replace_word(old_word, new_word, text):
    """
    replaces a word in a given text, regardless of it being lower- or upper case.

    :param old_word: the word that is being replaced
    :param new_word: the word that is the replacy
    :param text: the text in which the replacement (might) take place
    :return: the text with possible replacement(s)
    """

    if old_word.lower() in text.lower():
        idx = 0
        while idx < len(text):
            index_l = text.lower().find(old_word.lower(), idx)
            if index_l == -1:
                return text
            text = text[:index_l] + new_word + text[index_l + len(old_word):]
            idx = index_l + len(old_word)

    return text
