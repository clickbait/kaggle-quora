from multiprocessing import Process


def run(tasks):
    processes = []
    for task in tasks:
        processes.append(Process(target=task['target'], args=task['args']))

    for process in processes:
        process.start()

    for process in processes:
        process.join()
