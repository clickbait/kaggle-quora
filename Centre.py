from sklearn.feature_extraction.text import TfidfVectorizer
from multiprocessing import Pool
from ConfigParser import SafeConfigParser
from itertools import izip

import re
import gensim
import pandas as pd
import numpy as np
import os

from nltk.corpus import stopwords

import logging

logging.getLogger('gensim').setLevel(logging.ERROR)
logger = logging.getLogger(__name__)

stop_words = stopwords.words('english')

logger = logging.getLogger(__name__)

def sent2weightedvec(s, model, tff_voc, tff_idx):
    words = [w for w in s if not w in stop_words]
    words = [w for w in words if w.isalpha()]
    wordcount = len(words)
    M = []
    Unknown = 0
    for w in words:
        try:
            try:
                M.append(tff_idx[tff_voc[w]] * model[w])
                #M.append(model[w])
            except:
                M.append(model[w])
                continue
        # number of words unkown to word2vec
        except:
            Unknown += 1
            continue
    M = np.array(M, dtype=np.float64)
    v = M.sum(axis=0)

    if len(M) != 0:
        return v / np.sqrt((v ** 2).sum()), Unknown

    else:
        return np.zeros((300)), Unknown


def questionStart(first_word):
    wcounts = {'how': 0, 'should': 0, 'what': 0, 'which': 0, 'can': 0, 'when': 0, 'after': 0, 'is': 0, 'why': 0,
               'math': 0, 'if': 0, 'other': 0}

    if first_word in wcounts:
        wcounts[first_word] += 1

    else:
        wcounts['other'] += 1

    return wcounts.values()

def decode_unigram(feature):
    """
    For a given unigram that is a list, but interpreted as a string, this makes it into a string

    :param feature: the string
    :return: the unigram
    """
    lst = feature[1:-1].split(',')
    lst = map(unicode, lst)
    lst = map(unicode.strip, lst)
    return lst

def extract_features(config, config_section, data_frame):

    data_path = config.get('data', 'data_path')
    model_base_path = config.get('data', 'model_path')
    model_file = config.get(config_section, 'model_file')
    model_path = os.path.join(data_path, model_base_path, model_file)

    num_qs = data_frame.unigram1.values.shape[0]

    logger.info("Retrieve unigrams")

    model = gensim.models.KeyedVectors.load_word2vec_format(model_path, binary=True)

    question1_vectors = np.zeros((num_qs, 300))
    question2_vectors = np.zeros((num_qs, 300))
    cv_char = TfidfVectorizer(analyzer='word', min_df=2)

    logger.info("Creating TFIDF vocabulary")

    cv_char.fit_transform(data_frame['unigram1'] + data_frame['unigram2'])
    cv_char.transform(data_frame['unigram1']+data_frame['unigram2'])

    tff_voc = cv_char.vocabulary_
    tff_idx = cv_char.idf_

    # number of words not known to word2vec
    Unknown_q1 = np.zeros((num_qs))
    Unknown_q2 = np.zeros((num_qs))
    # feature vector for the starting word of a vector
    QS_q1 = np.zeros((num_qs,12))
    QS_q2 = np.zeros((num_qs,12))
    logger.info("Retrieving weighted sentences from word2vec with TFIDF scaling")

    for i, q in enumerate(data_frame.unigram1.values):
        # get sentence in word2vec space, number of unknown words
        # use regex to strip sentences
        question1_vectors[i], un = sent2weightedvec(re.sub('[\[\]\?\,]', ' ', q).split(), model,tff_voc, tff_idx)
        try:
            QS_q1[i] = questionStart(re.sub('[\[\]\?\,]', ' ', q).split()[0])

        # assign 'other' if question is empty
        except IndexError:
            QS_q1[i][8] = 1

        Unknown_q1[i] = un

    for i, q in enumerate(data_frame.unigram2.values):
        # get sentence in word2vec space, number of unknown words, number of words
        question2_vectors[i], unb = sent2weightedvec(re.sub('[\[\]\?\,]', ' ', q).split(), model ,tff_voc, tff_idx)
        try:
            QS_q2[i] = questionStart(re.sub('[\[\]\?\,]', ' ', q).split()[0])

        # assign 'other' if question is empty
        except IndexError:
            QS_q2[i][8] = 1

        Unknown_q2[i] = un

    logger.info("Freeing memory")
    del model, cv_char

    feature_frame = pd.DataFrame(question1_vectors-question2_vectors)

    return feature_frame
    # pool = Pool(processes=config.getint('multiprocessing', 'cpu_cores'))
    #
    # if 'qid1' in data_frame.columns:
    #     data_frame = data_frame.drop(['qid1', 'qid2', 'question1', 'question2'], 1)
    # else:
    #     data_frame = data_frame.drop(['question1', 'question2'], 1)
    #     data_frame = data_frame.rename(columns={'test_id': 'id'})
    #
    # # Load unigram
    # logger.info("Retrieve unigrams")
    # data_frame['unigram1'] = pool.map(decode_unigram, data_frame['unigram1'])
    # data_frame['unigram2'] = pool.map(decode_unigram, data_frame['unigram2'])
    # pool.close()
    #
    # # Create vocab over CHARACTERS
    # logger.info("Create vocabolary for characters")
    # cv_char = CountVectorizer(ngram_range=(1, 3), analyzer='char', min_df=2)
    # char_freq = cv_char.fit_transform(
    #     map(lambda x: " ".join(x), data_frame['unigram1']) + map(lambda x: " ".join(x), data_frame['unigram2']))
    #
    # # Create character n-grams dictonaries (what is the gram, how many times), and a sorted list of indices
    # logger.info("Create uni-, bi- and tri-gram dictonaries")
    # unigrams = dict([(k, v) for (k, v) in cv_char.vocabulary_.items() if len(k) == 1])
    # idx_unigrams = np.sort(unigrams.values())
    #
    # bigrams = dict([(k, v) for (k, v) in cv_char.vocabulary_.items() if len(k) == 2])
    # idx_bigrams = np.sort(bigrams.values())
    #
    # trigrams = dict([(k, v) for (k, v) in cv_char.vocabulary_.items() if len(k) == 3])
    # idx_trigrams = np.sort(trigrams.values())
    #
    # # Parse the questions
    # logger.info("Parse questions using vocab")
    # gram_q1 = cv_char.transform(map(lambda x: " ".join(x), data_frame['unigram1']))
    # gram_q2 = cv_char.transform(map(lambda x: " ".join(x), data_frame['unigram2']))
    #
    # # Use the jaccard distance to obtain information on the questions
    # # Unigram
    # logger.info("Calculate the jaccard measures for unigrams")
    # data_frame['char_unigram_jaccard'] = jaccard(gram_q1, gram_q2, idx_unigrams)
    # data_frame['char_unigram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, idx_unigrams)
    # data_frame['char_unigram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, idx_unigrams)
    #
    # # Bigram
    # logger.info("Calculate the jaccard measures for bigrams")
    # data_frame['char_bigram_jaccard'] = jaccard(gram_q1, gram_q2, idx_bigrams)
    # data_frame['char_bigram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, idx_bigrams)
    # data_frame['char_bigram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, idx_bigrams)
    #
    # # Trigram
    # logger.info("Calculate the jaccard measures for trigrams")
    # data_frame['char_trigram_jaccard'] = jaccard(gram_q1, gram_q2, idx_trigrams)
    # data_frame['char_trigram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, idx_trigrams)
    # data_frame['char_trigram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, idx_trigrams)
    #
    # # Unigram + Bigram + Trigram
    # logger.info("Calculate the jaccard measures for uni + bi + tri grams")
    # data_frame['char_allgram_jaccard'] = jaccard(gram_q1, gram_q2,
    #                                              range(0, len(idx_unigrams) + len(idx_bigrams) + len(idx_trigrams)))
    # data_frame['char_allgram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, range(0, len(idx_unigrams) + len(
    #     idx_bigrams) + len(idx_trigrams)))
    # data_frame['char_allgram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, range(0, len(idx_unigrams) + len(
    #     idx_bigrams) + len(idx_trigrams)))
    #
    # # Get the tf-idf features
    # logger.info("Calculate the tf-idf features")
    # data_frame['char_tfidf_l2_cosine'] = tfidf_func(gram_q1, gram_q2, norm='l2', func='cosine')
    # data_frame['char_tfidf_l2_l1'] = tfidf_func(gram_q1, gram_q2, norm='l2', func='l1')
    # data_frame['char_tfidf_l2_l2'] = tfidf_func(gram_q1, gram_q2, norm='l2', func='l2')
    #
    # # Create vocab over WORDS
    # logger.info("Create the vocabulary for the words")
    # cv_word = CountVectorizer(ngram_range=(1, 3), analyzer='word', min_df=2)
    # char_word = cv_word.fit_transform(
    #     map(lambda x: " ".join(x), data_frame['unigram1']) + map(lambda x: " ".join(x), data_frame['unigram2']))
    #
    # # Create character n-grams dictonaries (what is the gram, how many times), and a sorted list of indices
    # logger.info("Create uni-, bi- and tri-gram dictonaries")
    # unigrams = dict([(k, v) for (k, v) in cv_word.vocabulary_.items() if len(k.split()) == 1])
    # idx_unigrams = np.sort(unigrams.values())
    #
    # bigrams = dict([(k, v) for (k, v) in cv_word.vocabulary_.items() if len(k.split()) == 2])
    # idx_bigrams = np.sort(bigrams.values())
    #
    # trigrams = dict([(k, v) for (k, v) in cv_word.vocabulary_.items() if len(k.split()) == 3])
    # idx_trigrams = np.sort(trigrams.values())
    #
    # # Parse the questions
    # logger.info("Parse questions using vocab")
    # word_q1 = cv_word.transform(map(lambda x: " ".join(x), data_frame['unigram1']))
    # word_q2 = cv_word.transform(map(lambda x: " ".join(x), data_frame['unigram2']))
    #
    # # Use the jaccard distance to obtain information on the questions
    # # Unigram
    # logger.info("Calculate the jaccard measures for unigrams")
    # data_frame['word_unigram_jaccard'] = jaccard(word_q1, word_q2, idx_unigrams)
    # data_frame['word_unigram_jaccard_all'] = jaccard_all(word_q1, word_q2, idx_unigrams)
    # data_frame['word_unigram_jaccard_max'] = jaccard_max(word_q1, word_q2, idx_unigrams)
    #
    # # Bigram
    # logger.info("Calculate the jaccard measures for bigrams")
    # data_frame['word_bigram_jaccard'] = jaccard(word_q1, word_q2, idx_bigrams)
    # data_frame['word_bigram_jaccard_all'] = jaccard_all(word_q1, word_q2, idx_bigrams)
    # data_frame['word_bigram_jaccard_max'] = jaccard_max(word_q1, word_q2, idx_bigrams)
    #
    # # Trigram
    # logger.info("Calculate the jaccard measures for trigrams")
    # data_frame['word_trigram_jaccard'] = jaccard(word_q1, word_q2, idx_trigrams)
    # data_frame['word_trigram_jaccard_all'] = jaccard_all(word_q1, word_q2, idx_trigrams)
    # data_frame['word_trigram_jaccard_max'] = jaccard_max(word_q1, word_q2, idx_trigrams)
    #
    # # Unigram + Bigram + Trigram
    # logger.info("Calculate the jaccard measures for uni + bi + tri grams")
    # data_frame['word_allgram_jaccard'] = jaccard(word_q1, word_q2,
    #                                              range(0, len(idx_unigrams) + len(idx_bigrams) + len(idx_trigrams)))
    # data_frame['word_allgram_jaccard_all'] = jaccard_all(word_q1, word_q2, range(0, len(idx_unigrams) + len(
    #     idx_bigrams) + len(idx_trigrams)))
    # data_frame['word_allgram_jaccard_max'] = jaccard_max(word_q1, word_q2, range(0, len(idx_unigrams) + len(
    #     idx_bigrams) + len(idx_trigrams)))
    #
    # # Get the tf-idf features
    # logger.info("Calculate the tf-idf features")
    # data_frame['word_tfidf_l2_cosine'] = tfidf_func(word_q1, word_q2, norm='l2', func='cosine')
    # data_frame['word_tfidf_l2_l1'] = tfidf_func(word_q1, word_q2, norm='l2', func='l1')
    # data_frame['word_tfidf_l2_l2'] = tfidf_func(word_q1, word_q2, norm='l2', func='l2')
    #
    # # Remove the unigram
    # data_frame = data_frame.drop(['unigram1', 'unigram2'], 1)
    #
    # return data_frame


if __name__ == "__main__":
    ''' Extract features gets the config file so this line is not required'''
    config_path = "../../config/"


    config = SafeConfigParser()
    print os.path.join(config_path, "roeland.ini")
    config.read(os.path.join(config_path, "roeland.ini"))

    ''' This will work fine with config_selection '''
    data_path = "/home/rooler/Documents/MLIP/kaggle-quora-data/train_preprocessed.csv"

    model_path = "/home/rooler/Documents/MLIP/kaggle-quora-data/models/word2vec/GoogleNews-vectors-negative300.bin"

    data_frame = pd.read_csv(data_path, encoding='utf-8')
    num_qs = 100000
    pool = Pool(processes=4)

    # Load unigram
    # data_frame['unigram1'] = pool.map(decode_unigram, data_frame['unigram1'][0:num_qs])
    # data_frame['unigram2'] = pool.map(decode_unigram, data_frame['unigram2'][0:num_qs])
    pool.close()

    print "Everything works?"

    model = gensim.models.KeyedVectors.load_word2vec_format(model_path, binary=True)
    print "Again?"
    logger.info("Sent2vec")
    question1_vectors = np.zeros((num_qs, 300))
    question2_vectors = np.zeros((num_qs, 300))
    cv_char = TfidfVectorizer(analyzer='word', min_df=2)
    char_freq = cv_char.fit_transform(data_frame['unigram1'][0:num_qs] + data_frame['unigram2'][0:num_qs])

    print "And again?"
    cv_char.transform(data_frame['unigram1'][0:num_qs])
    tff_voc = cv_char.vocabulary_
    tff_idx = cv_char.idf_

    print tff_idx[tff_voc['what']]
    print data_frame['unigram1'][33]

    print re.sub('[\[\]\?]', '', data_frame['unigram1'][33])
    # number of words not known to word2vec
    Unknown_q1 = np.zeros((num_qs))
    Unknown_q2 = np.zeros((num_qs))
    # numberof words
    Wc_q1 = np.zeros((num_qs))
    Wc_q2 = np.zeros((num_qs))
    # feature vector for the starting word of a vector
    QS_q1 = np.zeros((num_qs,12))
    QS_q2 = np.zeros((num_qs,12))


    for i, q in enumerate(data_frame.unigram1[0:num_qs].values):
        # get sentence in word2vec space, number of unknown words, number of words
        question1_vectors[i], un, wc = sent2weightedvec(re.sub('[\[\]\?\,]', ' ', q).split(), model,tff_voc, tff_idx)
        try:
            QS_q1[i] = questionStart(re.sub('[\[\]\?\,]', ' ', q).split()[0])

        # assign 'other' if question is empty
        except IndexError:
            QS_q1[i][8] = 1
            print "empty question"

        Unknown_q1[i] = un
        Wc_q1[i] = wc

    for i, q in enumerate(data_frame.unigram2[0:num_qs].values):
        # get sentence in word2vec space, number of unknown words, number of words
        question2_vectors[i], un, wc = sent2weightedvec(re.sub('[\[\]\?\,]', ' ', q).split(), model ,tff_voc, tff_idx)
        try:
            QS_q2[i] = questionStart(re.sub('[\[\]\?\,]', ' ', q).split()[0])

        # assign 'other' if question is empty
        except IndexError:
            QS_q2[i][8] = 1
            print "empty question"

        Unknown_q2[i] = un
        Wc_q2[i] = wc

    # # concatenate all the features
    # X = np.concatenate((question1_vectors, np.array((Unknown_q1, Wc_q1)).transpose(),
    #                     QS_q1, question2_vectors, np.array((Unknown_q2, Wc_q2)).transpose(), QS_q2), 1)[0:50000]
    # # import some data to play with
    # data_path = "/home/rooler/Documents/MLIP/kaggle-quora-data/train_labels.csv"
    # data_frame_labels = pd.read_csv(data_path, encoding='utf-8')



    # we create an instance of Neighbours Classifier and fit the data.
    # logreg.fit(X, Y)
    #
    # X_test = np.concatenate((question1_vectors, np.array((Unknown_q1, Wc_q1)).transpose(),
    #                     QS_q1, question2_vectors, np.array((Unknown_q2, Wc_q2)).transpose(), QS_q2), 1)[50001:100000]
    # Y_test = data_frame_labels.values[50001:100000][:,1]
    #
    # logreg.score(X_test,Y_test)
    #
    # ## Try a model with only the starting words
    # logreg = linear_model.LogisticRegression(C=1e5)


 # Logreg gets a score of 0.69665393307866152 with weighted vectors


    # pca = PCA(n_components=2)
    # pca.fit(question1_vectors)
    # P1 = pca.transform(question1_vectors)
    # pca = PCA(n_components=2)
    #
    # pca.fit(question2_vectors)
    # P2 = pca.transform(question2_vectors)
    #
    # plt.figure(4)
    # plt.plot(P1[:, 0], P1[:, 1],'r.')
    # plt.plot(P2[:, 0], P2[:, 1],'.b')
    # plt.show()


#TODO
    # run logisitc regression with features:
    # - weighted word2vec  (DONE)
    # - amount of unkowns (DONE)
    # - wordcount (DONE)
    # - start of question, (DONE)



    #
    # for i, q in enumerate(data_frame.unigram2.values):
    #     question2_vectors[i] = sent2vec(q, model)
    # model = []
    # print "And again?"

    # # Create vocab over CHARACTERS
    # cv_char = CountVectorizer(ngram_range=(1, 3), analyzer='char', min_df=2)
    # char_freq = cv_char.fit_transform(
    #     map(lambda x: " ".join(x), data_frame['unigram1']) + map(lambda x: " ".join(x), data_frame['unigram2']))
    #
    # # Create character n-grams dictonaries (what is the gram, how many times), and a sorted list of indices
    # print 'N-gram character classification over training set:'
    # unigrams = dict([(k, v) for (k, v) in cv_char.vocabulary_.items() if len(k) == 1])
    # idx_unigrams = np.sort(unigrams.values())
    # print 'Unigrams:', len(unigrams)
    # bigrams = dict([(k, v) for (k, v) in cv_char.vocabulary_.items() if len(k) == 2])
    # idx_bigrams = np.sort(bigrams.values())
    # print 'Bigrams: ', len(bigrams)
    # trigrams = dict([(k, v) for (k, v) in cv_char.vocabulary_.items() if len(k) == 3])
    # idx_trigrams = np.sort(trigrams.values())
    # print 'Trigrams:', len(trigrams)
    #
    # # Parse the questions
    # gram_q1 = cv_char.transform(map(lambda x: " ".join(x), data_frame['unigram1']))
    # gram_q2 = cv_char.transform(map(lambda x: " ".join(x), data_frame['unigram2']))
    #
    # # Use the jaccard distance to obtain information on the questions
    #
    # t0 = time.time()
    # # Unigram
    # data_frame['char_unigram_jaccard'] = jaccard(gram_q1, gram_q2, idx_unigrams)
    # data_frame['char_unigram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, idx_unigrams)
    # data_frame['char_unigram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, idx_unigrams)
    # print time.time() - t0
    #
    # t0 = time.time()
    # # Bigram
    # data_frame['char_bigram_jaccard'] = jaccard(gram_q1, gram_q2, idx_bigrams)
    # data_frame['char_bigram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, idx_bigrams)
    # data_frame['char_bigram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, idx_bigrams)
    # print time.time() - t0
    # t0 = time.time()
    # # Trigram
    # data_frame['char_trigram_jaccard'] = jaccard(gram_q1, gram_q2, idx_trigrams)
    # data_frame['char_trigram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, idx_trigrams)
    # data_frame['char_trigram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, idx_trigrams)
    # print time.time() - t0
    # t0 = time.time()
    # # Unigram + Bigram + Trigram
    # data_frame['char_allgram_jaccard'] = jaccard(gram_q1, gram_q2,
    #                                              range(0, len(idx_unigrams) + len(idx_bigrams) + len(idx_trigrams)))
    # data_frame['char_allgram_jaccard_all'] = jaccard_all(gram_q1, gram_q2, range(0, len(idx_unigrams) + len(
    #     idx_bigrams) + len(idx_trigrams)))
    # data_frame['char_allgram_jaccard_max'] = jaccard_max(gram_q1, gram_q2, range(0, len(idx_unigrams) + len(
    #     idx_bigrams) + len(idx_trigrams)))
    # print time.time() - t0
    #
    # # Get the tf-idf features
    # t0 = time.time()
    # data_frame['char_tfidf_l2_cosine'] = tfidf_func(gram_q1, gram_q2, norm='l2', func='cosine')
    # data_frame['char_tfidf_l2_l1'] = tfidf_func(gram_q1, gram_q2, norm='l2', func='l1')
    # data_frame['char_tfidf_l2_l2'] = tfidf_func(gram_q1, gram_q2, norm='l2', func='l2')
    # print time.time() - t0
    #
    # # Create vocab over WORDS
    # cv_word = CountVectorizer(ngram_range=(1, 3), analyzer='word', min_df=2)
    # char_word = cv_word.fit_transform(
    #     map(lambda x: " ".join(x), data_frame['unigram1']) + map(lambda x: " ".join(x), data_frame['unigram2']))
    #
    # # Create character n-grams dictonaries (what is the gram, how many times), and a sorted list of indices
    # print 'N-gram character classification over training set:'
    # unigrams = dict([(k, v) for (k, v) in cv_word.vocabulary_.items() if len(k.split()) == 1])
    # idx_unigrams = np.sort(unigrams.values())
    # print 'Unigrams:', len(unigrams)
    # bigrams = dict([(k, v) for (k, v) in cv_word.vocabulary_.items() if len(k.split()) == 2])
    # idx_bigrams = np.sort(bigrams.values())
    # print 'Bigrams: ', len(bigrams)
    # trigrams = dict([(k, v) for (k, v) in cv_word.vocabulary_.items() if len(k.split()) == 3])
    # idx_trigrams = np.sort(trigrams.values())
    # print 'Trigrams:', len(trigrams)
    #
    # # Parse the questions
    # word_q1 = cv_word.transform(map(lambda x: " ".join(x), data_frame['unigram1']))
    # word_q2 = cv_word.transform(map(lambda x: " ".join(x), data_frame['unigram2']))
    #
    # # Use the jaccard distance to obtain information on the questions
    # t0 = time.time()
    # # Unigram
    # data_frame['word_unigram_jaccard'] = jaccard(word_q1, word_q2, idx_unigrams)
    # data_frame['word_unigram_jaccard_all'] = jaccard_all(word_q1, word_q2, idx_unigrams)
    # data_frame['word_unigram_jaccard_max'] = jaccard_max(word_q1, word_q2, idx_unigrams)
    # print time.time() - t0
    #
    # t0 = time.time()
    # # Bigram
    # data_frame['word_bigram_jaccard'] = jaccard(word_q1, word_q2, idx_bigrams)
    # data_frame['word_bigram_jaccard_all'] = jaccard_all(word_q1, word_q2, idx_bigrams)
    # data_frame['word_bigram_jaccard_max'] = jaccard_max(word_q1, word_q2, idx_bigrams)
    # print time.time() - t0
    # t0 = time.time()
    # # Trigram
    # data_frame['word_trigram_jaccard'] = jaccard(word_q1, word_q2, idx_trigrams)
    # data_frame['word_trigram_jaccard_all'] = jaccard_all(word_q1, word_q2, idx_trigrams)
    # data_frame['word_trigram_jaccard_max'] = jaccard_max(word_q1, word_q2, idx_trigrams)
    # print time.time() - t0
    # t0 = time.time()
    # # Unigram + Bigram + Trigram
    # data_frame['word_allgram_jaccard'] = jaccard(word_q1, word_q2,
    #                                              range(0, len(idx_unigrams) + len(idx_bigrams) + len(idx_trigrams)))
    # data_frame['word_allgram_jaccard_all'] = jaccard_all(word_q1, word_q2, range(0, len(idx_unigrams) + len(
    #     idx_bigrams) + len(idx_trigrams)))
    # data_frame['word_allgram_jaccard_max'] = jaccard_max(word_q1, word_q2, range(0, len(idx_unigrams) + len(
    #     idx_bigrams) + len(idx_trigrams)))
    # print time.time() - t0
    #
    # # Get the tf-idf features
    # t0 = time.time()
    # data_frame['word_tfidf_l2_cosine'] = tfidf_func(word_q1, word_q2, norm='l2', func='cosine')
    # data_frame['word_tfidf_l2_l1'] = tfidf_func(word_q1, word_q2, norm='l2', func='l1')
    # data_frame['word_tfidf_l2_l2'] = tfidf_func(word_q1, word_q2, norm='l2', func='l2')
    # print time.time() - t0
    #
    # print data_frame.tail(10)